var clicks = 0;
var closeSelectOnClicksNumber = 0;
var closeClick = false;

$(window).ready(function() {
    initializePseudoSelect();

    $('body').live('click', function() {
        clicks++;
        if (closeClick && clicks === closeSelectOnClicksNumber) {
            $('.pseudo-select-list').hide();
            closeClick = false;
            closeSelectOnClicksNumber = 0;
        }
    });

    $('.pseudo-select-selected').live('click', function() {
        var list = $(this).parent().children('.pseudo-select-list');
        if (list.css('display') == 'none') {
            $('.pseudo-select-list').hide();
            closeClick = false;
            closeSelectOnClicksNumber = 0;
            list.show();
            closeClick = true;
            closeSelectOnClicksNumber = clicks + 2;
        } else {
            list.hide();
            closeClick = false;
            closeSelectOnClicksNumber = 0;
        }
    });

    $('.pseudo-select-arrow').live('click', function() {
        var list = $(this).parent().children('.pseudo-select-list');
        if (list.css('display') == 'none') {
            $('.pseudo-select-list').hide();
            closeClick = false;
            closeSelectOnClicksNumber = 0;
            list.show();
            closeClick = true;
            closeSelectOnClicksNumber = clicks + 2;
        } else {
            list.hide();
            closeClick = false;
            closeSelectOnClicksNumber = 0;
        }
    });

    $('.pseudo-select-list-item').live('click', function() {
        $(this).parent().parent().children('.pseudo-select-selected').html($(this).html());
        $(this).parent().parent().children('.pseudo-select-list').hide();
        $(this).parent().children('.pseudo-select-list-item').removeClass('pseudo-select-list-item-selected');
        $(this).addClass('pseudo-select-list-item-selected');
        $(this).parent().parent().children('.pseudo-select-input').val($(this).attr('name')).trigger('change');
        closeClick = false;
        closeSelectOnClicksNumber = 0;
    });
});

function initializePseudoSelect() {
    $('.pseudo-select-input').val('');
    $('.pseudo-select').each(function() {
        var not_selected = true;
        $(this).children('.pseudo-select-list').children('.pseudo-select-list-item').each(function() {
            if ($(this).hasClass('pseudo-select-list-item-selected')) {
                not_selected = false;
            }
        });
        if (not_selected) {
            var item = $(this).children('.pseudo-select-list').find(':nth-child(1)');
            $(this).children('.pseudo-select-selected').html(item.html());
            $(this).children('.pseudo-select-input').val(item.attr('name')).trigger('change');
            $(this).children('.pseudo-select-list').children('.pseudo-select-list-item').removeClass('pseudo-select-list-item-selected');
            item.addClass('pseudo-select-list-item-selected');
            closeClick = false;
            closeSelectOnClicksNumber = 0;
        } else {
            var item = $(this).children('.pseudo-select-list').children('.pseudo-select-list-item-selected');
            $(this).children('.pseudo-select-selected').html(item.html());
            $(this).children('.pseudo-select-input').val(item.attr('name')).trigger('change');
            closeClick = false;
            closeSelectOnClicksNumber = 0;
        }
    });
}
