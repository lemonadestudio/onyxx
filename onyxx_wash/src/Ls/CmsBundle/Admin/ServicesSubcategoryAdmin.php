<?php

// src/Ls/CmsBundle/Admin/PoradaCategoryAdmin.php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ServicesSubcategoryAdmin extends Admin {

    public function createQuery($context = 'list') {
        $query = parent::createQuery($context);
        $request = $this->getRequest();
        $catId = $request->get('catId',false);
        if($catId != false){
            $query->where('o.category = :catId')
            ->setParameter('catId', $catId);
        }
        $query->orderBy('o.arrangement', 'ASC');

        return $query;
    }

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('moveup', $this->getRouterIdParameter() . '/moveup');
        $collection->add('movedown', $this->getRouterIdParameter() . '/movedown');
        $collection->add('thumb', $this->getRouterIdParameter() . '/thumb/{type}');
        $collection->add('thumbSave', $this->getRouterIdParameter() . '/thumbSave');
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper->add('title', null, array('label' => 'Nazwa', 'required' => true))
        ->add('category', null, array('label' => 'Kategoria', 'required' => true));
        //$formMapper->add('slug', null, array('label' => 'Slug', 'required' => true));


        /*$formMapper->with('Zdjęcie');
        if (null !== $this->getRoot()->getSubject()->getPhoto()) {
            $formMapper
                ->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => false));
        } else {
            $formMapper
                ->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => true));
        }*/

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('title', null, array('label' => 'Nazwa'))
               // ->add('slug', null, array('label' => 'Slug'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('title', null, array('label' => 'Nazwa'))
                ->add('_action', 'actions', array(
                    'label' => 'Opcje',
                    'actions' => array(
                        'movedown' => array(),
                        'moveup' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    public function prePersist($entity) {

        /* @var $entity \Ls\CmsBundle\Entity\ServicesCategory */
       /* if (null !== $entity->getFile()) {
            $sFileName = uniqid('category-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $entity->upload();
        }*/


    }

    public function preUpdate($entity) {
       /* if (null !== $entity->getFile()) {

            $sFileName = uniqid('category-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $entity->upload();
        }*/


    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\ServicesCategory:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

}
