<?php

// src/Ls/CmsBundle/Admin/KarnetZamowienieAdmin.php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class KarnetZamowienieAdmin extends Admin
{

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $query->orderBy('o.created_at', 'DESC');

        return $query;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('show', 'list'));
    }

    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('code', null, array('label' => 'Kod'))
            ->add('price', null, array('label' => 'Cena'))
            ->add('created_at', null, array('label' => 'Data złożenia'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('created_at', null, array('label' => 'Data złożenia'))
            ->add('code', null, array('label' => 'Kod'))
            ->add('karnet', null, array('label' => 'Karnet', 'template' => 'LsCmsBundle:Admin\KarnetZamowienie:karnet.html.twig'))
            ->add('klient', null, array('label' => 'Klient', 'template' => 'LsCmsBundle:Admin\KarnetZamowienie:klient.html.twig'))
            ->add('price', null, array('label' => 'Cena'))
            ->add('_action', 'actions', array(
                'label' => 'Opcje',
                'actions' => array(
                    'show' => array(),
                )
            ));
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'show':
                return 'LsCmsBundle:Admin\KarnetZamowienie:show.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

}
