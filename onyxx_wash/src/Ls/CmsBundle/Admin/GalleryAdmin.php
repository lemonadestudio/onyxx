<?php

// src/Ls/CmsBundle/Admin/GalleryAdmin.php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GalleryAdmin extends Admin
{

    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'title'
    );

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('photos', $this->getRouterIdParameter() . '/photos');
        if ($this->hasParentFieldDescription()) {
            $collection->remove('create');
        }
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $helps = array();
        $formMapper
            ->add('title', null, array('label' => 'Tytuł', 'required' => true));
        if ($this->getRoot()->getSubject()->getId() > 0) {
            $formMapper->add('slug', null, array('label' => 'Końcówka adresu URL', 'required' => false));
            $helps['slug'] = 'Element linku URL identyfikujący galerię np. http://www.test.pl/galeria/<b>galeria</b>';
        }
        $formMapper
            ->add('content_short', 'textarea', array('label' => 'Krótka treść', 'required' => false, 'attr' => array('rows' => 5)))
            ->add('content', null, array('label' => 'Treść', 'attr' => array('class' => 'wysiwyg')))
            ->add('on_list', null, array('label' => 'Wyświetlaj na liście galerii', 'required' => false))
            ->add('attachable', null, array('label' => 'Dołączalna do podstron i aktualności', 'required' => false))
            ->add('seo_generate', null, array('label' => 'Generuj opcje SEO'))
            ->add('seo_title', null, array('label' => 'SEO Title', 'required' => false))
            ->add('seo_keywords', null, array('label' => 'SEO Keywords', 'required' => false))
            ->add('seo_description', null, array('label' => 'SEO Description', 'required' => false, 'attr' => array('rows' => 3)));
        $formMapper->setHelps($helps);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array('label' => 'Nazwa'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, array('label' => 'Nazwa'))
            ->add('on_list', null, array('label' => 'Widoczna na liście'))
            ->add('attachable', null, array('label' => 'Dołączalna'))
            ->add('_action', 'actions', array(
                'label' => 'Opcje',
                'actions' => array(
                    'photos' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }
}
