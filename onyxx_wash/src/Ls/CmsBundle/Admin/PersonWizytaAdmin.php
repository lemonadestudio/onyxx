<?php

// src/Ls/CmsBundle/Admin/PersonWizytaAdmin.php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PersonWizytaAdmin extends Admin
{

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $query->orderBy('o.termin', 'DESC');

        return $query;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('show', 'list'));
    }

    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('firstname', null, array('label' => 'Imię'))
            ->add('lastname', null, array('label' => 'Nazwisko'))
            ->add('phone', null, array('label' => 'Telefon'))
            ->add('email', null, array('label' => 'E-mail'))
            ->add('termin', null, array('label' => 'Preferowany termin'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('dietetyk', null, array('label' => 'Dietetyk', 'template' => 'LsCmsBundle:Admin\PersonWizyta:dietetyk.html.twig'))
            ->add('klient', null, array('label' => 'Klient', 'template' => 'LsCmsBundle:Admin\PersonWizyta:klient.html.twig'))
            ->add('termin', null, array('label' => 'Termin'))
            ->add('_action', 'actions', array(
                'label' => 'Opcje',
                'actions' => array(
                    'show' => array(),
                )
            ));
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'show':
                return 'LsCmsBundle:Admin\PersonWizyta:show.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

}
