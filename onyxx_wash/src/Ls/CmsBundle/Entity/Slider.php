<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Slider
 * @ORM\Table(name="slider")
 * @ORM\Entity
 */
class Slider
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @var string
     */
    private $label;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $photo_width;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $photo_height;

    /**
     * @ORM\OneToMany(
     *   targetEntity="SliderPhoto",
     *   mappedBy="slider",
     *   cascade={"all"}
     * )
     * @ORM\OrderBy({"arrangement" = "ASC"})
     * @var \Doctrine\Common\Collections\Collection
     */
    private $photos;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->photos = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Slider
     */
    public function setLabel($label)
    {
        $this->label = $label;
    
        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set photo_width
     *
     * @param integer $photoWidth
     * @return Slider
     */
    public function setPhotoWidth($photoWidth)
    {
        $this->photo_width = $photoWidth;
    
        return $this;
    }

    /**
     * Get photo_width
     *
     * @return integer 
     */
    public function getPhotoWidth()
    {
        return $this->photo_width;
    }

    /**
     * Set photo_height
     *
     * @param integer $photoHeight
     * @return Slider
     */
    public function setPhotoHeight($photoHeight)
    {
        $this->photo_height = $photoHeight;
    
        return $this;
    }

    /**
     * Get photo_height
     *
     * @return integer 
     */
    public function getPhotoHeight()
    {
        return $this->photo_height;
    }

    /**
     * Add photos
     *
     * @param \Ls\CmsBundle\Entity\SliderPhoto $photos
     * @return Slider
     */
    public function addPhoto(\Ls\CmsBundle\Entity\SliderPhoto $photos)
    {
        $this->photos[] = $photos;
    
        return $this;
    }

    /**
     * Remove photos
     *
     * @param \Ls\CmsBundle\Entity\SliderPhoto $photos
     */
    public function removePhoto(\Ls\CmsBundle\Entity\SliderPhoto $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhotos()
    {
        return $this->photos;
    }
    
    public function setPhotos(\Doctrine\Common\Collections\ArrayCollection $photos)
    {
        return $this->photos = $photos;
    }

    public function __toString() {
        if (is_null($this->getLabel())) {
            return 'NULL';
        }
        return $this->getLabel();
    }

}