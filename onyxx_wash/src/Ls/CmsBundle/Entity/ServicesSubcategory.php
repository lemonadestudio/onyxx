<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PhpThumb\ThumbFactory;
use Ls\CmsBundle\Utils\Tools;

/**
 * ServicesSubcategory
 *
 * @ORM\Table(name="services_subcategory")
 * @ORM\Entity
 */
class ServicesSubcategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @var string
     */
    private $title;


    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;


    /**
     * @ORM\ManyToOne(
     *     targetEntity="ServicesCategory",
     *     inversedBy="porady"
     * )
     * @ORM\JoinColumn(
     *     name="category_id",
     *     referencedColumnName="id",
     *     onDelete="SET NULL"
     * )
     * @var \Ls\CmsBundle\Entity\ServicesCategory
     */
    private $category;

    /*
     * /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer")
     */
   // private $category;
   //  */




    /**
     * Set title
     *
     * @param string $title
     * @return PoradaCategory
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set category
     *
     * @param \Ls\CmsBundle\Entity\ServicesCategory $category
     * @return ServicesSubcategory
     */
    public function setCategory(\Ls\CmsBundle\Entity\ServicesCategory $category = null) {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Ls\CmsBundle\Entity\ServicesCategory
     */
    public function getCategory() {
        return $this->category;
    }


    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return PoradaCategory
     */
    public function setArrangement($arrangement)
    {
        $this->arrangement = $arrangement;

        return $this;
    }

    public function __toString() {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }
        return $this->getTitle();
    }


    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement()
    {
        return $this->arrangement;
    }

}
