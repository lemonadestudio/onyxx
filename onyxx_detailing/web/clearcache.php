<?php
/**
 * Created by PhpStorm.
 * User: Lukasz
 * Date: 24.08.14
 * Time: 03:11
 */

function rrmdir($dir) {
    foreach(glob($dir . '/*') as $file) {
        if(is_dir($file)){
            chmod($file,0777);
            rrmdir($file);
        } else{
            chmod($file,0777);
            unlink($file);
        }
    } rmdir($dir);
}

$dir = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.'prod';

if(file_exists($dir)){
    $flushName = $dir.'_'.rand(100000,1000000);
    rename($dir,$flushName);


    rrmdir($flushName);
    unlink($flushName.DIRECTORY_SEPARATOR.'jms_diextra'.DIRECTORY_SEPARATOR.'metadata'.DIRECTORY_SEPARATOR.'.cache.php');
    rmdir($flushName.DIRECTORY_SEPARATOR.'jms_diextra'.DIRECTORY_SEPARATOR.'metadata');
    rmdir($flushName.DIRECTORY_SEPARATOR.'jms_diextra');
    rmdir($flushName);
}

echo 'cache clear';