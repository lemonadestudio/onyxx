$(document).ready(function() {

    $('#menuTop ul li.home a').html('&nbsp;');
    var loopIntervalID = false;
    $(".clickable").click(function(){
        var href = $(this).data('href');
        window.location.href = href;
    });

    $("a.fancybox").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600, 
		'speedOut'		:	200,
                'onCleanup': function(){}
	});
        
    var $p = $('#sliderTopWrapper');
    function initSliderMain() {
        var name = $p.find('.slide.position-1 > .name').html();
        $p.find('.leftSide > .bottomSide >.label').html(name);
    }

    var allowClick = true;
    $p.on('click', '.button', function() {
        var time = 500;
        if (allowClick) {
            allowClick = false;
            setTimeout(function() {
                allowClick = true;
            }, time);
            var $t = $(this);
            var $slides = $p.find('.slide');
            var pos;
            var minPosition = 1;
            var maxPosition = $slides.length;
            var direction = 'next';

            if ($t.hasClass('prev')) {
                direction = 'prev';
            }

            $slides.each(function() {
                pos = parseInt($(this).attr('class').split('-').pop());

                $(this).removeClass('position-' + pos);
                if (direction === 'next') {
                    pos--;
                    if (pos < minPosition) {
                        pos = maxPosition;
                    }
                } else {
                    pos++;
                    if (pos > maxPosition) {
                        pos = minPosition;
                    }
                }

                $(this).addClass('position-' + pos);
            });
            var $first = $p.find('.slide.position-1');

            var name = $first.find('.name').html();

            $p.find('.bottomSide > .label').html(name);
            clearTimeout(loopIntervalID);
            loopIntervalID = setTimeout(looping,6000);
        }
    });
    $p.on('click', '.slide', function(e) {
        e.preventDefault();
        var pos = parseInt($(this).attr('class').split('-').pop());
        if (pos === 2) {
            $p.find('.button.next').trigger('click');
        }
        else if (pos === 3) {

            $p.find('.button.prev').trigger('click');
        }
    });
    $('#references-side').hover(function() {
        $(this).css('right', '0');
    }, function() {
        $(this).css('right', '-188px');
    });

    initSliderMain();

    var looping = function(){
        $p.find('.button.next').trigger('click');
    }
    loopIntervalID = setTimeout(looping,6000);


    var newsDetail = $('#newsDetail');
    var projectsDetail = $('#projectsDetail');
    var servicesDetail = $('#servicesDetail');

    if (newsDetail.length > 0 || projectsDetail.length > 0 || servicesDetail.length > 0) {
        adjustTextAndItems();
    }
    function adjustTextAndItems() {
        var item = $('.galleryItem');
        var itemM = parseInt(item.css('margin-bottom'));
        var itemH = item.height() + itemM;
        var text = $('.pageContent');
        var textH = text.outerHeight();
        var rightCount = parseInt(textH / itemH) + 1;
        var newH = (rightCount * itemH) - itemM;
        
        text.css('height', newH + 'px');

        item.each(function() {
            if ($(this).position().left < 200) {
                $(this).addClass('noMargin');
            }
        });
    }
    
    adjustListingTitleLength();
    
    function adjustListingTitleLength() {
        var item = $('.adjustTitleLength');
        var triangleW = item.find('.triangle').outerWidth();
        item.each(function() {
            var itemW = $(this).outerWidth();
            var count = parseInt(itemW / triangleW);
            var newW = (count * triangleW) + (triangleW / 2);
            if(newW < itemW){
                count++;
                newW = (count * triangleW) + (triangleW / 2);
            }
            $(this).width(newW);
            for (i = 1; i <= count; i++) {
                $(this).append('<figure class="triangle triangle' + i + '"></figure');
            }
            if($(this).hasClass('adjustLeftOffset')){
                $(this).css('margin-left','-'+newW/2+'px');
            }

        });
    }
    $('.contactForm').on('focus','input, textarea',function(){
       $(this).parent('.item').addClass('focus');
    });
    $('.contactForm').on('blur','input, textarea',function(){
       $(this).parent('.item').removeClass('focus');
    });
    $('.datepicker').datepicker();
    $('.selectmenu').selectmenu({
        width: '100%',
        menuWidth: '223px'
    });
    $('.listing').on('click','.content',function(){
        var link = $(this).find('a').attr('href');
        window.location.href = link;
    });
    
});