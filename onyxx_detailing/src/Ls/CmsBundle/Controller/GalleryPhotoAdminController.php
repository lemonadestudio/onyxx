<?php

// src/Ls/CmsBundle/Controller/GalleryPhotoAdminController.php

namespace Ls\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use PhpThumb\ThumbFactory;
use Ls\CmsBundle\Entity\GalleryPhoto;
use Ls\CmsBundle\Utils\Tools;

class GalleryPhotoAdminController extends Controller {

    /**
     * return the Response object associated to the list action
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     *
     * @return Response
     */
    public function listAction() {
        if (false === $this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }

        $datagrid = $this->admin->getDatagrid();
        $formView = $datagrid->getForm()->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($formView, $this->admin->getFilterTheme());

        return $this->render($this->admin->getTemplate('list'), array(
                    'action' => 'list',
                    'form' => $formView,
                    'datagrid' => $datagrid,
                    'csrf_token' => $this->getCsrfToken('sonata.batch'),
                    'uploadfolder' => addslashes($this->getUploadRootDir()),
                    'addurl' => $this->admin->generateUrl('addmany'),
                    'redirecturl' => $this->admin->generateUrl('list'),
        ));
    }

    private function getMaxKolejnosc($gallery) {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                ->select('COUNT(c.id)')
                ->from('LsCmsBundle:GalleryPhoto', 'c')
                ->where('c.gallery = :gallery')
                ->setParameter('gallery', $gallery)
                ->getQuery();

        $total = $query->getSingleScalarResult();
        return $total + 1;
    }

    public function movedownAction() {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();

        $target = $em->getRepository('LsCmsBundle:GalleryPhoto')->findOneById($request->attributes->get('childId'));

        $max = $this->getMaxKolejnosc($target->getGallery());
        $old_kolejnosc = $target->getArrangement();
        $new_kolejnosc = $old_kolejnosc + 1;
        if ($new_kolejnosc < $max) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                    ->select('c.id')
                    ->from('LsCmsBundle:GalleryPhoto', 'c')
                    ->where('c.arrangement = :arrangement')
                    ->andWhere('c.gallery = :gallery')
                    ->setParameters(
                            array(
                                'arrangement' => $new_kolejnosc,
                                'gallery' => $target->getGallery()
                    ))
                    ->getQuery();

            $photo_id = $query->getSingleScalarResult();
            $entity = $em->getRepository('LsCmsBundle:GalleryPhoto')->findOneById($photo_id);
            $entity->setArrangement(0);
            $em->persist($entity);
            $em->flush();
            $target->setArrangement($new_kolejnosc);
            $em->persist($target);
            $em->flush();
            $entity->setArrangement($old_kolejnosc);
            $em->persist($entity);
            $em->flush();
        }

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function moveupAction() {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();

        $target = $em->getRepository('LsCmsBundle:GalleryPhoto')->findOneById($request->attributes->get('childId'));

        $old_kolejnosc = $target->getArrangement();
        $new_kolejnosc = $old_kolejnosc - 1;
        if ($new_kolejnosc > 0) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                    ->select('c.id')
                    ->from('LsCmsBundle:GalleryPhoto', 'c')
                    ->where('c.arrangement = :arrangement')
                    ->andWhere('c.gallery = :gallery')
                    ->setParameters(
                            array(
                                'arrangement' => $new_kolejnosc,
                                'gallery' => $target->getGallery()
                    ))
                    ->getQuery();

            $photo_id = $query->getSingleScalarResult();
            $entity = $em->getRepository('LsCmsBundle:GalleryPhoto')->findOneById($photo_id);
            $entity->setArrangement(0);
            $em->persist($entity);
            $em->flush();
            $target->setArrangement($new_kolejnosc);
            $em->persist($target);
            $em->flush();
            $entity->setArrangement($old_kolejnosc);
            $em->persist($entity);
            $em->flush();
        }

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function thumbAction() {
        $type = $this->get('request')->get('type');
        $id = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (null === $object->getPhotoAbsolutePath()) {
            return new RedirectResponse($this->admin->generateUrl('list'));
        } else {
            $size = $object->getThumbSize($type);
            $photo = $object->getPhotoSize();
            $thumb_ratio = $size['width'] / $size['height'];
            $photo_ratio = $photo['width'] / $photo['height'];

            $thumb_conf = array();
            $thumb_conf['photo_width'] = $photo['width'];
            $thumb_conf['photo_height'] = $photo['height'];
            if ($thumb_ratio < $photo_ratio) {
                $thumb_conf['width'] = round($photo['height'] * $thumb_ratio);
                $thumb_conf['height'] = $photo['height'];
                $thumb_conf['x'] = ceil(($photo['width'] - $thumb_conf['width']) / 2);
                $thumb_conf['y'] = 0;
            } else {
                $thumb_conf['width'] = $photo['width'];
                $thumb_conf['height'] = round($photo['width'] / $thumb_ratio);
                $thumb_conf['x'] = 0;
                $thumb_conf['y'] = ceil(($photo['height'] - $thumb_conf['height']) / 2);
            }

            $preview = array();
            $preview['width'] = 150;
            $preview['height'] = round(150 / $thumb_ratio);
            return $this->render('LsCmsBundle:Admin/GalleryPhoto:kadruj.html.twig', array(
                        'object' => $object,
                        'preview' => $preview,
                        'thumb_conf' => $thumb_conf,
                        'size' => $size,
                        'aspect' => $thumb_ratio,
                        'type' => $type,
            ));
        }
    }

    public function thumbSaveAction() {
        $type = $this->get('request')->get('type');
        $x = $this->get('request')->get('x');
        $y = $this->get('request')->get('y');
        $x2 = $this->get('request')->get('x2');
        $y2 = $this->get('request')->get('y2');
        $id = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        $object->Thumb($x, $y, $x2, $y2, $type);

        return new RedirectResponse($this->admin->generateObjectUrl('edit', $object));
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/temp';
    }

    public function addmanyAction() {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();

        $gallery = $em->getRepository('LsCmsBundle:Gallery')->findOneById($request->attributes->get('id'));
        $arrangement = $this->getMaxKolejnosc($gallery);

        $files_string = substr($request->request->get('files'), 1);
        $files = explode(';', $files_string);

        foreach ($files as $filename) {
            $filename_array = explode('.', $filename);
            $ext = end($filename_array);
            $source = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $filename;

            $sFileName = uniqid('gallery-image-') . '.' . $ext;

            $entity = new GalleryPhoto();
            $entity->setGallery($gallery);
            $entity->setArrangement($arrangement);
            $entity->setPhoto($sFileName);
            $sSourceName = $entity->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

            if (!is_dir($entity->getUploadRootDir())) {
                $old_umask = umask(0);
                mkdir($entity->getUploadRootDir(), 0777);
                umask($old_umask);
            }

            copy($source, $sSourceName);

            $thumb = ThumbFactory::create($sSourceName);
            $dimensions = $thumb->getCurrentDimensions();

            //zmniejszenie zdjecia oryginalnego jesli jest za duze
            if ($dimensions['width'] > 1024 || $dimensions['height'] > 768) {
                $thumb->resize(1024, 768);
                $thumb->save($sSourceName);
            }

            //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
            $thumb = ThumbFactory::create($sSourceName);
            $sThumbNameL = Tools::thumbName($sSourceName, '_l');
            $aThumbSizeL = $entity->getThumbSize('list');
            $thumb->adaptiveResize($aThumbSizeL['width'], $aThumbSizeL['height']);
            $thumb->save($sThumbNameL);

            $thumb = ThumbFactory::create($sSourceName);
            $sThumbNameD = Tools::thumbName($sSourceName, '_d');
            $aThumbSizeD = $entity->getThumbSize('detail');
            $thumb->adaptiveResize($aThumbSizeD['width'], $aThumbSizeD['height']);
            $thumb->save($sThumbNameD);

            $thumb = ThumbFactory::create($sSourceName);
            $sThumbNameV = Tools::thumbName($sSourceName, '_v');
            $aThumbSizeV = $entity->getThumbSize('vertical');
            $thumb->adaptiveResize($aThumbSizeV['width'], $aThumbSizeV['height']);
            $thumb->save($sThumbNameV);

            $em->persist($entity);
            if (file_exists($source)) {
                @unlink($source);
            }
            $arrangement++;
        }
        $em->flush();
        return new Response('ok');
    }

}
