<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\CmsBundle\Entity\Gallery;
use Ls\CmsBundle\Utils\Tools;

/**
 * Gallery controller.
 *
 */
class GalleryController extends Controller
{

    /**
     * Lists all Gallery entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()->select('a')
            ->from('LsCmsBundle:Gallery', 'a')
            ->where('a.on_list = 1')
            ->getQuery()
            ->getResult();

        $first_column = array();
        $second_column = array();

        foreach($entities as $key => $entity) {
            if($key % 2 == 0) {
                $first_column[] = $entity;
            } else {
                $second_column[] = $entity;
            }
        }

        $qb = $em->createQueryBuilder();
        $news = $qb->select('n')
            ->from('LsCmsBundle:News', 'n')
            ->where($qb->expr()->isNotNull('n.published_at'))
            ->orderBy('n.published_at', 'DESC')
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();

        foreach ($news as $item) {
            $item->setContentShort(Tools::truncateWord($item->getContentShort(), 250, '...'));
        }

        return $this->render('LsCmsBundle:Gallery:index.html.twig', array(
            'first_column' => $first_column,
            'second_column' => $second_column,
            'news' => $news,
        ));
    }

    /**
     * Finds and displays a Gallery entity.
     *
     */
    public function showAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsCmsBundle:Gallery')->findOneBySlug($slug);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $qb = $em->createQueryBuilder();
        $news = $qb->select('n')
            ->from('LsCmsBundle:News', 'n')
            ->where($qb->expr()->isNotNull('n.published_at'))
            ->orderBy('n.published_at', 'DESC')
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();

        foreach ($news as $item) {
            $item->setContentShort(Tools::truncateWord($item->getContentShort(), 250, '...'));
        }

        return $this->render('LsCmsBundle:Gallery:show.html.twig', array(
            'entity' => $entity,
            'news' => $news,
        ));
    }

}
