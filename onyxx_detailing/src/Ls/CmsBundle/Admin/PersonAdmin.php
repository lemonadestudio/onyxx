<?php

// src/Ls/CmsBundle/Admin/PersonAdmin.php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PersonAdmin extends Admin
{

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $query->orderBy('o.arrangement', 'ASC');

        return $query;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('moveup', $this->getRouterIdParameter() . '/moveup');
        $collection->add('movedown', $this->getRouterIdParameter() . '/movedown');
        $collection->add('thumb', $this->getRouterIdParameter() . '/thumb/{type}');
        $collection->add('thumbSave', $this->getRouterIdParameter() . '/thumbSave');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne')
            ->add('firstname', null, array('label' => 'Imię', 'required' => true))
            ->add('lastname', null, array('label' => 'Nazwisko', 'required' => true))
            ->add('dietetyk', null, array('label' => 'Dietetyk'))
            ->add('content', null, array('label' => 'Opis', 'required' => false, 'attr' => array('class' => 'wysiwyg-basic')))
            ->add('content_main', 'textarea', array('label' => 'Opis na stronę głowną', 'required' => false, 'attr' => array('class' => 'wysiwyg-basic')))
            ->with('Zdjęcie')
        ;
        if (null !== $this->getRoot()->getSubject()->getPhotoList()) {
            $formMapper
                ->add('file_list', 'file', array('label' => 'Zdjęcie na listę', 'required' => false));
        } else {
            $formMapper
                ->add('file_list', 'file', array('label' => 'Zdjęcie na listę', 'required' => true));
        }
            $formMapper
                ->add('file_main', 'file', array('label' => 'Zdjęcie na stronę główną', 'required' => false));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('firstname', null, array('label' => 'Imię'))
            ->add('lastname', null, array('label' => 'Nazwisko'))
            ->add('dietetyk', null, array('label' => 'Dietetyk'))
            ->add('_action', 'actions', array(
                'label' => 'Opcje',
                'actions' => array(
                    'movedown' => array(),
                    'moveup' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\Person:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function prePersist($entity) {
        if (null !== $entity->getFileList()) {
            $sFileName = uniqid('person-') . '.' . $entity->getFileList()->guessExtension();
            $entity->setPhotoList($sFileName);
            $entity->upload('list');
        }
        if (null !== $entity->getFileMain()) {
            $sFileName = uniqid('person-') . '.' . $entity->getFileMain()->guessExtension();
            $entity->setPhotoMain($sFileName);
            $entity->upload('main');
        }
    }

    public function preUpdate($entity) {
        if (null !== $entity->getFileList()) {
            $sFileName = uniqid('person-') . '.' . $entity->getFileList()->guessExtension();
            $entity->setPhotoList($sFileName);
            $entity->upload('list');
        }
        if (null !== $entity->getFileMain()) {
            $sFileName = uniqid('person-') . '.' . $entity->getFileMain()->guessExtension();
            $entity->setPhotoMain($sFileName);
            $entity->upload('main');
        }
    }
}
