<?php

// src/Ls/CmsBundle/Admin/PoradaCategoryAdmin.php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PoradaCategoryAdmin extends Admin {

    public function createQuery($context = 'list') {
        $query = parent::createQuery($context);

        $query->orderBy('o.arrangement', 'ASC');

        return $query;
    }

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('moveup', $this->getRouterIdParameter() . '/moveup');
        $collection->add('movedown', $this->getRouterIdParameter() . '/movedown');
        $collection->add('thumb', $this->getRouterIdParameter() . '/thumb/{type}');
        $collection->add('thumbSave', $this->getRouterIdParameter() . '/thumbSave');
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper->add('name', null, array('label' => 'Nazwa', 'required' => true));
        $formMapper->add('color', null, array('label' => 'Kolor', 'required' => true));
        $formMapper->add('description', null, array('label' => 'Opis', 'attr' => array('class' => 'wysiwyg')));

        $formMapper->with('Zdjęcie');
        if (null !== $this->getRoot()->getSubject()->getPhoto()) {
            $formMapper
                ->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => false));
        } else {
            $formMapper
                ->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => true));
        }
        $formMapper->with('Miniatura menu');

        if (null !== $this->getRoot()->getSubject()->getMenuBgImg()) {

            $formMapper->add('thumb', 'file', array('label' => 'Tło menu', 'required' => false));
        } else {

            $formMapper->add('thumb', 'file', array('label' => 'Tło menu', 'required' => true));
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name', null, array('label' => 'Nazwa'))
                ->add('color', null, array('label' => 'Kolor'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('name', null, array('label' => 'Nazwa'))
                ->add('_action', 'actions', array(
                    'label' => 'Opcje',
                    'actions' => array(
                        'movedown' => array(),
                        'moveup' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    public function prePersist($entity) {

        /* @var $entity \Ls\CmsBundle\Entity\PoradaCategory */
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('category-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $entity->upload();
        }

        if (null !== $entity->getThumb()) {
            $sFileName = uniqid('category-thumb-') . '.' . $entity->getThumb()->guessExtension();
            $entity->setMenuBgImg($sFileName);
            $entity->uploadThumb();
        }
    }

    public function preUpdate($entity) {
        if (null !== $entity->getFile()) {

            $sFileName = uniqid('category-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $entity->upload();
        }

        if (null !== $entity->getThumb()) {
            $sFileName = uniqid('category-thumb-') . '.' . $entity->getThumb()->guessExtension();
            $entity->setMenuBgImg($sFileName);
            $entity->uploadThumb();
        }
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\Category:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

}
