<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PhpThumb\ThumbFactory;
use Ls\CmsBundle\Utils\Tools;

/**
 * SliderPhoto
 * @ORM\Table(name="slider_photo")
 * @ORM\Entity
 */
class SliderPhoto {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $link_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $route;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $routeParameters;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $onclick;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @Assert\File(maxSize="4194304")
     */
    protected $file;

    protected $uri;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Slider",
     *     inversedBy="photos"
     * )
     * @ORM\JoinColumn(
     *     name="slider_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @var \Ls\CmsBundle\Entity\Slider
     */
    private $slider;

    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return SliderPhoto
     */
    public function setPhoto($photo) {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string 
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return SliderPhoto
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set link_name
     *
     * @param string $link_name
     * @return SliderPhoto
     */
    public function setLinkName($link_name) {
        $this->link_name = $link_name;

        return $this;
    }

    /**
     * Get link_name
     *
     * @return string 
     */
    public function getLinkName() {
        return $this->link_name;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return SliderPhoto
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set route
     *
     * @param string $route
     * @return SliderPhoto
     */
    public function setRoute($route)
    {
        $this->route = $route;
    
        return $this;
    }

    /**
     * Get route
     *
     * @return string 
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set routeParameters
     *
     * @param string $routeParameters
     * @return SliderPhoto
     */
    public function setRouteParameters($routeParameters)
    {
        $this->routeParameters = $routeParameters;
    
        return $this;
    }

    /**
     * Get routeParameters
     *
     * @return string 
     */
    public function getRouteParameters()
    {
        return $this->routeParameters;
    }

    public function getRouteParametersArray() {
    	return (array)json_decode($this->getRouteParameters());
    }

    /**
     * Set url
     *
     * @param string $url
     * @return SliderPhoto
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set onclick
     *
     * @param string $onclick
     * @return MenuItem
     */
    public function setOnclick($onclick) {
        $this->onclick = $onclick;

        return $this;
    }

    /**
     * Get onclick
     *
     * @return string 
     */
    public function getOnclick() {
        return $this->onclick;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return SliderPhoto
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer 
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Set uri
     *
     * @param string $uri
     * @return SliderPhoto
     */
    public function setUri($uri) {
        $this->uri = $uri;
    }

    /**
     * Get uri
     *
     * @return string 
     */
    public function getUri() {
        return $this->uri;
    }

    /**
     * Set slider
     *
     * @param \Ls\CmsBundle\Entity\Slider $slider
     * @return SliderPhoto
     */
    public function setSlider(\Ls\CmsBundle\Entity\Slider $slider = null) {
        $this->slider = $slider;

        return $this;
    }

    /**
     * Get slider
     *
     * @return \Ls\CmsBundle\Entity\Slider 
     */
    public function getSlider() {
        return $this->slider;
    }

    public function __toString() {
        if (is_null($this->getPhoto())) {
            return 'NULL';
        }
        return $this->getPhoto();
    }

    public function getThumbSize($type) {
        $size = array();
        switch ($type) {
            case 'full':
                $size['width'] = $this->getSlider()->getPhotoWidth();
                $size['height'] = $this->getSlider()->getPhotoHeight();
                break;
        }
        return $size;
    }

    public function getThumbWebPath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            switch ($type) {
                case 'full':
                    $sThumbName = Tools::thumbName($this->photo, '_f');
                    break;
            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            switch ($type) {
                case 'full':
                    $sThumbName = Tools::thumbName($this->photo, '_f');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFile(UploadedFile $file = null) {
        $this->deletePhoto();
        $this->file = $file;
        if (empty($this->photo)) {
            $this->setPhoto('empty');
        } else {
            $this->setPhoto('');
        }
    }

    public function getFile() {
        return $this->file;
    }

    public function deletePhoto() {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_f = Tools::thumbName($filename, '_f');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_f)) {
                @unlink($filename_f);
            }
        }
    }

    public function getPhotoAbsolutePath() {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath() {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        if ($this->getSlider()) {
            return 'upload/slider/' . $this->getSlider()->getId();
        } else {
            return 'upload/slider';
        }
    }

    public function upload() {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();
        $ext = $this->getFile()->guessExtension();
        $this->file->move($this->getUploadRootDir(), $sFileName);

        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;
        
        $options = array('jpegQuality' => 100);
        
        $thumb = ThumbFactory::create($sSourceName, $options);
        $dimensions = $thumb->getCurrentDimensions();

        //zmniejszenie zdjecia oryginalnego jesli jest za duze
        if ($dimensions['width'] > 1920 || $dimensions['height'] > 1080) {
            $thumb->resize(1920, 1080);
            $thumb->save($sSourceName);
        }

        $options = array('jpegQuality' => 100);
        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = ThumbFactory::create($sSourceName, $options);
        $sThumbNameF = Tools::thumbName($sSourceName, '_f');
        $aThumbSizeF = $this->getThumbSize('full');
        $thumb->adaptiveResize($aThumbSizeF['width'], $aThumbSizeF['height']);
        $thumb->save($sThumbNameF);
        
        unset($this->file);
    }

    public function Thumb($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $options = array('jpegQuality' => 100);

        $thumb = ThumbFactory::create($sSourceName, $options);
        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }
    
}