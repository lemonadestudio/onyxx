<?php

// src/Ls/CmsBundle/Controller/PricingItemAdminController.php

namespace Ls\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\AdminBundle\Controller\CRUDController as Controller;

class PricingItemAdminController extends Controller
{
    private function getMaxKolejnosc()
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsCmsBundle:PricingItem', 'c')
            ->getQuery();

        $total = $query->getSingleScalarResult();
        return $total + 1;
    }

    public function movedownAction()
    {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();

        $target = $em->getRepository('LsCmsBundle:PricingItem')->findOneById($request->attributes->get('id'));

        $max = $this->getMaxKolejnosc();
        $old_kolejnosc = $target->getArrangement();
        $new_kolejnosc = $old_kolejnosc + 1;
        if ($new_kolejnosc < $max) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsCmsBundle:PricingItem', 'c')
                ->where('c.arrangement = :arrangement')
                ->setParameter('arrangement', $new_kolejnosc)
                ->getQuery();

            $photo_id = $query->getSingleScalarResult();
            $entity = $em->getRepository('LsCmsBundle:PricingItem')->findOneById($photo_id);
            $entity->setArrangement(0);
            $em->persist($entity);
            $em->flush();
            $target->setArrangement($new_kolejnosc);
            $em->persist($target);
            $em->flush();
            $entity->setArrangement($old_kolejnosc);
            $em->persist($entity);
            $em->flush();
        }

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function moveupAction()
    {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();

        $target = $em->getRepository('LsCmsBundle:PricingItem')->findOneById($request->attributes->get('id'));

        $old_kolejnosc = $target->getArrangement();
        $new_kolejnosc = $old_kolejnosc - 1;
        if ($new_kolejnosc > 0) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsCmsBundle:PricingItem', 'c')
                ->where('c.arrangement = :arrangement')
                ->setParameter('arrangement', $new_kolejnosc)
                ->getQuery();

            $photo_id = $query->getSingleScalarResult();
            $entity = $em->getRepository('LsCmsBundle:PricingItem')->findOneById($photo_id);
            $entity->setArrangement(0);
            $em->persist($entity);
            $em->flush();
            $target->setArrangement($new_kolejnosc);
            $em->persist($target);
            $em->flush();
            $entity->setArrangement($old_kolejnosc);
            $em->persist($entity);
            $em->flush();
        }

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

}
