<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\CmsBundle\Utils\Tools;

/**
 * Pages controller.
 *
 */
class PageController extends Controller
{

    public function getCategories(){
        $em = $this->getDoctrine()->getManager();

        $categories = $em->createQueryBuilder()
            ->select('n')
            ->from('LsCmsBundle:PoradaCategory', 'n')
            ->orderBy('n.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        return $categories;
    }

    /**
     * Finds and displays a Pages entity.
     *
     */
    public function showAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsCmsBundle:Page')->findOneBySlug($slug);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $qb = $em->createQueryBuilder();
        $news = $qb->select('n')
            ->from('LsCmsBundle:News', 'n')
            ->where($qb->expr()->isNotNull('n.published_at'))
            ->orderBy('n.published_at', 'DESC')
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();

        foreach ($news as $item) {
            $item->setContentShort(Tools::truncateWord($item->getContentShort(), 250, '...'));
        }

        return $this->render('LsCmsBundle:Page:show.html.twig', array(
            'entity' => $entity,
            'news' => $news,
            'categories' => $this->getCategories()
        ));
    }

    public function rescaleAction(){


        die('dupa');
    }

}
