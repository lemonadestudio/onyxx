<?php

// src/LS/CMSBundle/Admin/SliderPhotoAdmin.php

namespace LS\CMSBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class SliderPhotoAdmin extends Admin {

    protected $parentAssociationMapping = 'slider';

    public function createQuery($context = 'list') {
        $query = parent::createQuery($context);

        $query->orderBy('o.slider', 'ASC');
        $query->addOrderBy('o.arrangement', 'ASC');

        return $query;
    }

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('routeparameters');
        $collection->add('moveup', $this->getRouterIdParameter() . '/moveup');
        $collection->add('movedown', $this->getRouterIdParameter() . '/movedown');
        $collection->add('thumb', $this->getRouterIdParameter() . '/thumb/{type}');
        $collection->add('thumbSave', $this->getRouterIdParameter() . '/thumbSave');
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('Tekst')
                ->add('title', null, array('label' => 'Tytuł'))
                ->add('content', null, array('label' => 'Treść', 'attr' => array('class' => 'wysiwyg-slider')))
                ->with('Link')
                ->add('link_name', null, array('label' => 'Tekst linku'))
                ->add('type', 'choice', array('choices' => $this->getTypeChoices(), 'label' => 'Typ linku'))
                ->add('url', null, array('label' => 'URL'))
                ->add('route', 'choice', array('required' => false, 'choices' => $this->getModuleChoices(), 'label' => 'Moduł'))
                ->add('routeParameters', 'hidden', array('required' => false))
                ->add('onclick', 'choice', array('required' => false, 'choices' => $this->getOnClickChoices(), 'label' => 'Otwierany obiekt'))
                ->add('searchServices', 'hidden', array('data' => $this->getSearchServicesJson(), 'mapped' => false), array('style' => ''))
                ->with('Zdjęcie');
        if (null !== $this->getRoot()->getSubject()->getPhoto()) {
            $formMapper->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => false));
        } else {
            $formMapper->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => true));
        }
        $formMapper->setHelps(array('file' => 'Zdjęcie nie mniejsze niż 1290x832 px.'));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('slider', null, array('label' => 'Slider'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('slider', null, array('label' => 'Slider'))
                ->add('image', null, array('label' => 'Zdjęcie', 'template' => 'LsCmsBundle:Admin\SliderPhoto:list_photo.html.twig'))
                ->add('_action', 'actions', array(
                    'label' => 'Opcje',
                    'actions' => array(
                        'movedown' => array(),
                        'moveup' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    public function getTypeChoices() {
        $types = array(
            'none' => 'Bez linkowania',
            'url' => 'Adres URL',
            'route' => 'Moduł',
            'button' => 'Przycisk JavaScript'
        );

        return $types;
    }

    public function getLocationChoices() {

        $config_menu = $this->getConfigurationPool()->getContainer()->getParameter('ls_cms.menu');

        $locations = array();

        foreach ($config_menu['locations'] as $location) {

            $locations[$location['label']] = $location['name'];
        }

        return $locations;
    }

    public function getModuleChoices() {

        $config_menu = $this->getConfigurationPool()->getContainer()->getParameter('ls_cms.menu');

        $modules = array();

        foreach ($config_menu['modules'] as $module) {

            $modules[$module['route']] = $module['label'];
        }

        return $modules;
    }

    public function getOnClickChoices() {
        $config_menu = $this->getConfigurationPool()->getContainer()->getParameter('ls_cms.menu');

        $functions = array();

        foreach ($config_menu['onclick'] as $function) {
            $functions[$function['name']] = $function['label'];
        }

        return $functions;
    }

    public function getSearchServicesJson() {
        return json_encode($this->getSearchServices());
    }

    public function getSearchServices() {
        $config_menu = $this->getConfigurationPool()->getContainer()->getParameter('ls_cms.menu');

        $services = array();
        foreach ($config_menu['modules'] as $module) {
            $services[$module['route']] = $module['get_elements_service'];
        }

        return $services;
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\SliderPhoto:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function prePersist($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('slider-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function preUpdate($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('slider-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function saveFile($entity) {
        $entity->upload();
    }

}
