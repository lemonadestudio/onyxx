<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PhpThumb\ThumbFactory;
use Ls\CmsBundle\Utils\Tools;

/**
 * Page
 * @ORM\Table(name="page")
 * @ORM\Entity
 */
class Page {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $content_short_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $content_short;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seo_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_description;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="Gallery", inversedBy="pages")
     * @ORM\JoinColumn(name="gallery_id", referencedColumnName="id", onDelete="SET NULL")
     * @var \Ls\CmsBundle\Entity\Gallery
     *
     */
    private $gallery;

    /**
     * @Assert\File(maxSize="2097152")
     */
    protected $file;

    protected $listWidth = 156;
    protected $listHeight = 101;
    protected $detailWidth = 355;
    protected $detailHeight = 236;

    /**
     * Constructor
     */
    public function __construct() {
        $this->created_at = new \DateTime();
        $this->content_short_generate = true;
        $this->seo_generate = true;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Page
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set content_short_generate
     *
     * @param boolean $contentShortGenerate
     * @return Page
     */
    public function setContentShortGenerate($contentShortGenerate) {
        $this->content_short_generate = $contentShortGenerate;

        return $this;
    }

    /**
     * Get content_short_generate
     *
     * @return boolean 
     */
    public function getContentShortGenerate() {
        return $this->content_short_generate;
    }

    /**
     * Set content_short
     *
     * @param string $contentShort
     * @return Page
     */
    public function setContentShort($contentShort) {
        $this->content_short = $contentShort;

        return $this;
    }

    /**
     * Get content_short
     *
     * @return string 
     */
    public function getContentShort() {
        return $this->content_short;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Page
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return News
     */
    public function setPhoto($photo) {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * Set seo_generate
     *
     * @param boolean $seoGenerate
     * @return Page
     */
    public function setSeoGenerate($seoGenerate) {
        $this->seo_generate = $seoGenerate;

        return $this;
    }

    /**
     * Get seo_generate
     *
     * @return boolean 
     */
    public function getSeoGenerate() {
        return $this->seo_generate;
    }

    /**
     * Set seo_title
     *
     * @param string $seoTitle
     * @return Page
     */
    public function setSeoTitle($seoTitle) {
        $this->seo_title = $seoTitle;

        return $this;
    }

    /**
     * Get seo_title
     *
     * @return string 
     */
    public function getSeoTitle() {
        return $this->seo_title;
    }

    /**
     * Set seo_keywords
     *
     * @param string $seoKeywords
     * @return Page
     */
    public function setSeoKeywords($seoKeywords) {
        $this->seo_keywords = $seoKeywords;

        return $this;
    }

    /**
     * Get seo_keywords
     *
     * @return string 
     */
    public function getSeoKeywords() {
        return $this->seo_keywords;
    }

    /**
     * Set seo_description
     *
     * @param string $seoDescription
     * @return Page
     */
    public function setSeoDescription($seoDescription) {
        $this->seo_description = $seoDescription;

        return $this;
    }

    /**
     * Get seo_description
     *
     * @return string 
     */
    public function getSeoDescription() {
        return $this->seo_description;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Page
     */
    public function setCreatedAt($createdAt) {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Page
     */
    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    /**
     * Set gallery
     *
     * @param \Ls\CmsBundle\Entity\Gallery $gallery
     * @return Page
     */
    public function setGallery(\Ls\CmsBundle\Entity\Gallery $gallery = null) {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \Ls\CmsBundle\Entity\Gallery 
     */
    public function getGallery() {
        return $this->gallery;
    }

    public function __toString() {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }
        return $this->getTitle();
    }

    public function getThumbSize($type) {
        $size = array();
        switch ($type) {
            case 'list':
                $size['width'] = $this->listWidth;
                $size['height'] = $this->listHeight;
                break;
            case 'detail':
                $size['width'] = $this->detailWidth;
                $size['height'] = $this->detailHeight;
                break;
        }
        return $size;
    }

    public function getThumbWebPath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            switch ($type) {
                case 'list':
                    $sThumbName = Tools::thumbName($this->photo, '_l');
                    break;
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            switch ($type) {
                case 'list':
                    $sThumbName = Tools::thumbName($this->photo, '_l');
                    break;
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFile(UploadedFile $file = null) {
        $this->deletePhoto();
        $this->file = $file;
        if (empty($this->photo)) {
            $this->setPhoto('empty');
        } else {
            $this->setPhoto('');
        }
    }

    public function getFile() {
        return $this->file;
    }

    public function deletePhoto() {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_l = Tools::thumbName($filename, '_l');
            $filename_d = Tools::thumbName($filename, '_d');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_l)) {
                @unlink($filename_l);
            }
            if (file_exists($filename_d)) {
                @unlink($filename_d);
            }
        }
    }

    public function getPhotoAbsolutePath() {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath() {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/page';
    }

    public function upload() {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);
        $options = array('jpegQuality' => 100);
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;
        $thumb = ThumbFactory::create($sSourceName);
        $dimensions = $thumb->getCurrentDimensions();

        //zmniejszenie zdjecia oryginalnego jesli jest za duze
        if ($dimensions['width'] > 1024 || $dimensions['height'] > 768) {
            $thumb->resize(1024, 768);
            $thumb->save($sSourceName);
        }

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = ThumbFactory::create($sSourceName,$options);
        $sThumbNameL = Tools::thumbName($sSourceName, '_l');
        $aThumbSizeL = $this->getThumbSize('list');
        $thumb->adaptiveResize($aThumbSizeL['width'], $aThumbSizeL['height']);
        $thumb->save($sThumbNameL);

        $thumb = ThumbFactory::create($sSourceName,$options);
        $sThumbNameD = Tools::thumbName($sSourceName, '_d');
        $aThumbSizeD = $this->getThumbSize('detail');
        $thumb->adaptiveResize($aThumbSizeD['width'], $aThumbSizeD['height']);
        $thumb->save($sThumbNameD);

        unset($this->file);
    }

    public function Thumb($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = ThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }


}
