<?php

namespace Ls\CmsBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * PersonWizyta
 * @ORM\Table(name="person_wizyta")
 * @ORM\Entity
 */
class PersonWizyta
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Wypełnij pole")
     * @var string
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Wypełnij pole")
     * @var string
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Wypełnij pole")
     * @Assert\Email(message="Podaj poprawny adres e-mail")
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\NotBlank(message="Wypełnij pole")
     * @var \DateTime
     */
    private $termin;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Person",
     *     inversedBy="wizyty"
     * )
     * @ORM\JoinColumn(
     *     name="person_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @var \Ls\CmsBundle\Entity\Person
     */
    private $person;

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return PersonWizyta
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return PersonWizyta
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return PersonWizyta
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return PersonWizyta
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set termin
     *
     * @param \DateTime $termin
     * @return PersonWizyta
     */
    public function setTermin($termin)
    {
        $this->termin = $termin;

        return $this;
    }

    /**
     * Get termin
     *
     * @return \DateTime
     */
    public function getTermin()
    {
        return $this->termin;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return PersonWizyta
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set person
     *
     * @param \Ls\CmsBundle\Entity\Person $person
     * @return PersonWizyta
     */
    public function setPerson(\Ls\CmsBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get slider
     *
     * @return \Ls\CmsBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    public function __toString()
    {
        if (is_null($this->getFirstname()) || is_null($this->getLastname())) {
            return 'NULL';
        }
        return $this->getFirstname() . ' ' . $this->getLastname();
    }

}