<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Gallery
 * @ORM\Table(name="gallery")
 * @ORM\Entity
 */
class Gallery
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $content_short;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     * @var boolean
     */
    private $on_list;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     * @var boolean
     */
    private $attachable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seo_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_description;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(
     *   targetEntity="News",
     *   mappedBy="gallery"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $news;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Page",
     *   mappedBy="gallery"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $pages;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Porada",
     *   mappedBy="gallery"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $porady;

    /**
     * @ORM\OneToMany(
     *   targetEntity="GalleryPhoto",
     *   mappedBy="gallery",
     *   cascade={"all"}
     * )
     * @ORM\OrderBy({"arrangement" = "ASC"})
     * @var \Doctrine\Common\Collections\Collection
     */
    private $photos;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->on_list = true;
        $this->attachable = true;
        $this->news = new ArrayCollection();
        $this->pages = new ArrayCollection();
        $this->porady = new ArrayCollection();
        $this->photos = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Gallery
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Gallery
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set content_short
     *
     * @param string $contentShort
     * @return Gallery
     */
    public function setContentShort($contentShort) {
        $this->content_short = $contentShort;

        return $this;
    }

    /**
     * Get content_short
     *
     * @return string
     */
    public function getContentShort() {
        return $this->content_short;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Gallery
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set on_list
     *
     * @param boolean $onList
     * @return Gallery
     */
    public function setOnList($onList)
    {
        $this->on_list = $onList;
    
        return $this;
    }

    /**
     * Get on_list
     *
     * @return boolean 
     */
    public function getOnList()
    {
        return $this->on_list;
    }

    /**
     * Set attachable
     *
     * @param boolean $attachable
     * @return Gallery
     */
    public function setAttachable($attachable)
    {
        $this->attachable = $attachable;
    
        return $this;
    }

    /**
     * Get attachable
     *
     * @return boolean 
     */
    public function getAttachable()
    {
        return $this->attachable;
    }

    /**
     * Set seo_generate
     *
     * @param boolean $seoGenerate
     * @return Pages
     */
    public function setSeoGenerate($seoGenerate) {
        $this->seo_generate = $seoGenerate;

        return $this;
    }

    /**
     * Get seo_generate
     *
     * @return boolean 
     */
    public function getSeoGenerate() {
        return $this->seo_generate;
    }

    /**
     * Set seo_title
     *
     * @param string $seoTitle
     * @return Pages
     */
    public function setSeoTitle($seoTitle) {
        $this->seo_title = $seoTitle;

        return $this;
    }

    /**
     * Get seo_title
     *
     * @return string 
     */
    public function getSeoTitle() {
        return $this->seo_title;
    }

    /**
     * Set seo_keywords
     *
     * @param string $seoKeywords
     * @return Pages
     */
    public function setSeoKeywords($seoKeywords) {
        $this->seo_keywords = $seoKeywords;

        return $this;
    }

    /**
     * Get seo_keywords
     *
     * @return string 
     */
    public function getSeoKeywords() {
        return $this->seo_keywords;
    }

    /**
     * Set seo_description
     *
     * @param string $seoDescription
     * @return Pages
     */
    public function setSeoDescription($seoDescription) {
        $this->seo_description = $seoDescription;

        return $this;
    }

    /**
     * Get seo_description
     *
     * @return string 
     */
    public function getSeoDescription() {
        return $this->seo_description;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Gallery
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Gallery
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add news
     *
     * @param \Ls\CmsBundle\Entity\News $news
     * @return Gallery
     */
    public function addNews(\Ls\CmsBundle\Entity\News $news)
    {
        $this->news[] = $news;
    
        return $this;
    }

    /**
     * Remove news
     *
     * @param \Ls\CmsBundle\Entity\News $news
     */
    public function removeNews(\Ls\CmsBundle\Entity\News $news)
    {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Add pages
     *
     * @param \Ls\CmsBundle\Entity\Page $pages
     * @return Gallery
     */
    public function addPages(\Ls\CmsBundle\Entity\Page $pages)
    {
        $this->pages[] = $pages;
    
        return $this;
    }

    /**
     * Remove pages
     *
     * @param \Ls\CmsBundle\Entity\Page $pages
     */
    public function removePages(\Ls\CmsBundle\Entity\Page $pages)
    {
        $this->pages->removeElement($pages);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Add porady
     *
     * @param \Ls\CmsBundle\Entity\Porada $porada
     * @return Gallery
     */
    public function addPorady(\Ls\CmsBundle\Entity\Porada $porada)
    {
        $this->porady[] = $porada;

        return $this;
    }

    /**
     * Remove porady
     *
     * @param \Ls\CmsBundle\Entity\Porada $porada
     */
    public function removePorady(\Ls\CmsBundle\Entity\Porada $porada)
    {
        $this->porady->removeElement($porada);
    }

    /**
     * Get porady
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPorady()
    {
        return $this->porady;
    }

    /**
     * Add photos
     *
     * @param \Ls\CmsBundle\Entity\GalleryPhoto $photos
     * @return Gallery
     */
    public function addPhoto(\Ls\CmsBundle\Entity\GalleryPhoto $photos)
    {
        $this->photos[] = $photos;
    
        return $this;
    }

    /**
     * Remove photos
     *
     * @param \Ls\CmsBundle\Entity\GalleryPhoto $photos
     */
    public function removePhoto(\Ls\CmsBundle\Entity\GalleryPhoto $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhotos()
    {
        return $this->photos;
    }
    
    public function __toString() {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }
        return $this->getTitle();
    }
}