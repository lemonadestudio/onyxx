<?php

namespace LS\CMSBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CmsBundle\Entity\Person;

class PersonUpdater implements EventSubscriber
{

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'preUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Person) {
            if (null === $entity->getArrangement()) {
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsCmsBundle:Person', 'c')
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Person) {
            if (null === $entity->getArrangement()) {
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsCmsBundle:Person', 'c')
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Person) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();

                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsCmsBundle:Person', 'c')
                    ->where('c.arrangement > :arrangement')
                    ->setParameter('arrangement', $arrangement)
                    ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['type'] = 'Person';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
            $entity->deletePhoto('list');
            $entity->deletePhoto('main');
        }
    }

}