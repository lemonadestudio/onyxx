<?php

namespace LS\CMSBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Routing\Router;
use Ls\CmsBundle\Entity\PoradaCategory;
use Ls\CmsBundle\Entity\Porada;
use Ls\CmsBundle\Utils\Tools;

class PoradaUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'preUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof PoradaCategory) {
            if (null === $entity->getArrangement()) {
                $query = $em->createQueryBuilder()
                        ->select('COUNT(c.id)')
                        ->from('LsCmsBundle:PoradaCategory', 'c')
                        ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }

        if ($entity instanceof Porada) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
            if ($entity->getSeoGenerate()) {
                // description
                $description = strip_tags($entity->getContent());
                $description = Tools::truncateWord(html_entity_decode($description), 255, '');

                // usunięcie nowych linii
                $description = preg_replace('@\v@', ' ', $description);
                // podwójnych białych znaków
                $description = preg_replace('@\h{2,}@', ' ', $description);

                // usunięcie ostatniego, niedokończonego zdania
                $description = preg_replace('@(.*)\..*@', '\1.', $description);

                // trim
                $description = trim($description);

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('@\.,;\'\"@', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoKeywords(implode(', ', $keywords));
                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoGenerate(false);
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof PoradaCategory) {
            if (null === $entity->getArrangement()) {
                $query = $em->createQueryBuilder()
                        ->select('COUNT(c.id)')
                        ->from('LsCmsBundle:PoradaCategory', 'c')
                        ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }

        if ($entity instanceof Porada) {
            $entity->setUpdatedAt(new \DateTime());
            if ($entity->getSeoGenerate()) {
                // description
                $description = strip_tags($entity->getContent());
                $description = Tools::truncateWord(html_entity_decode($description), 255, '');

                // usunięcie nowych linii
                $description = preg_replace('@\v@', ' ', $description);
                // podwójnych białych znaków
                $description = preg_replace('@\h{2,}@', ' ', $description);

                // usunięcie ostatniego, niedokończonego zdania
                $description = preg_replace('@(.*)\..*@', '\1.', $description);

                // trim
                $description = trim($description);

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('@\.,;\'\"@', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $keywords = implode(', ', $keywords);
                $args->setNewValue('seo_title', strip_tags($entity->getTitle()));
                $args->setNewValue('seo_keywords', $keywords);
                $args->setNewValue('seo_description', $description);
                $args->setNewValue('seo_generate', false);
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof PoradaCategory) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();

                $query = $em->createQueryBuilder()
                        ->select('c')
                        ->from('LsCmsBundle:PoradaCategory', 'c')
                        ->where('c.arrangement > :arrangement')
                        ->setParameter('arrangement', $arrangement)
                        ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['type'] = 'PoradaCategory';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
        }
    }

}
