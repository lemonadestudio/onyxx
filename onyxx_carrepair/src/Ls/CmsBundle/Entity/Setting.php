<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use PhpThumb\ThumbFactory;
use Ls\CmsBundle\Utils\Tools;

/**
 * Setting
 *
 * @ORM\Entity
 * @ORM\Table(name="setting")
 */
class Setting {

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $label;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $value;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @Assert\File(maxSize="2097152")
     */
    protected $file;

    public function setFile(UploadedFile $file = null) {

        $this->file = $file;
        if (empty($this->value)) {
            $this->setValue('empty');
        } else {
            $this->setValue('');
        }
    }

    public function getFile() {
        return $this->file;
    }


    /**
     * Set label
     *
     * @param string $label
     * @return Setting
     */
    public function setLabel($label) {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel() {
        return $this->label;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Setting
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Setting
     */
    public function setValue($value) {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue() {
        return $this->value;
    }

    public function __toString() {
        if (is_null($this->getDescription())) {
            return 'NULL';
        }
        return $this->getDescription();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/settings';
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    public function upload(){

        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getValue();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        /*$options = array('jpegQuality' => 100);*/
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;
        $thumb = ThumbFactory::create($sSourceName);
        $dimensions = $thumb->getCurrentDimensions();

        $description = $this->getDescription();
        $pattern = '*\[(\d+)x(\d+)\]*';
        preg_match($pattern, $description, $matches);

        //zmniejszenie zdjecia oryginalnego jesli jest za duze
        if (count($matches) > 1 && $dimensions['width'] > $matches[1] || $dimensions['height'] > $matches[2]) {
            $thumb->resize($matches[1], $matches[2]);
            $thumb->save($sSourceName);
        }


    }

}