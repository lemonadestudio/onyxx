<?php

// src/Ls/CmsBundle/Admin/PricingItemAdmin.php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PricingItemAdmin extends Admin {

    public function createQuery($context = 'list') {
        $query = parent::createQuery($context);

        $query->orderBy('o.arrangement', 'ASC');

        return $query;
    }

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('moveup', $this->getRouterIdParameter() . '/moveup');
        $collection->add('movedown', $this->getRouterIdParameter() . '/movedown');
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('Ogólne')
                ->add('karnet', null, array('label' => 'Karnet'))
                ->add('name', null, array('label' => 'Nazwa', 'required' => true))
                ->add('description', null, array('label' => 'Opis'))
                ->add('price', null, array('label' => 'Cena'))
                ->with('Promocja')
                ->add('promo_price', null, array('label' => 'Cena'))
                ->add('promo_description', null, array('label' => 'Uwagi'));
        $helps['promo_description'] = 'Wyświetlane pod cennikiem';
        $formMapper->setHelps($helps);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name', null, array('label' => 'Nazwa'));
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('karnet', null, array('label' => 'Karnet'))
                ->add('name', null, array('label' => 'Nazwa'))
                ->add('description', null, array('label' => 'Opis'))
                ->add('price', null, array('label' => 'Cena'))
                ->add('promo_price', null, array('label' => 'Cena promocyjna'))
                ->add('_action', 'actions', array(
                    'label' => 'Opcje',
                    'actions' => array(
                        'movedown' => array(),
                        'moveup' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
        ));
    }

}
