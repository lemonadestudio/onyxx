<?php

// src/Ls/CmsBundle/Admin/SettingAdmin.php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SettingAdmin extends Admin
{

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $qb = $this->modelManager->getEntityManager('Ls\CmsBundle\Entity\Setting')->createQueryBuilder();

        $query->where($qb->expr()->notLike('o.label', $qb->expr()->literal('added_texts_%')));
        $query->orderBy('o.label', 'ASC');

        return $query;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {


        $formMapper
            ->with('Ogólne')
            ->add('label', null, array('label' => 'Etykieta'))
            ->add('description', null, array('label' => 'Opis', 'required' => false));
        if(strpos($this->getRoot()->getSubject()->getLabel(),'file') === false){
            $formMapper->add('value', null, array('label' => 'Wartość', 'required' => false));
        }else{
            $formMapper->add('file', 'file', array('label' => 'Plik', 'required' => false));
            //$formMapper->add('value', null, array('label' => 'Wartość', 'required' => false));
        }

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('label', null, array('label' => 'Etykieta'))
            ->add('value', null, array('label' => 'Wartość'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('description', null, array('label' => 'Opis'))
            ->add('value', null, array('label' => 'Wartość', 'template' => 'LsCmsBundle:Admin\Setting:list_value.html.twig'))
            ->add('_action', 'actions', array(
                'label' => 'Opcje',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    public function preUpdate($entity) {

        if (null !== $entity->getFile()) {
            $sFileName = uniqid('settings-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setValue($sFileName);
            $entity->upload();
        }

    }

}
