<?php

namespace Ls\CmsBundle\Controller;

use Ls\CmsBundle\Entity\PersonWizyta;
use Ls\CmsBundle\Entity\KarnetZamowienie;
use Ls\CmsBundle\Form\PersonWizytaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ls\CmsBundle\Utils\Tools;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotEqualTo;
use Symfony\Component\Validator\Constraints\NotBlank;

class ServicesController extends Controller {

    public function indexAction(){

        $em = $this->getDoctrine()->getManager();

        $categories = $em->createQueryBuilder()
            ->select('n')
            ->from('LsCmsBundle:ServicesCategory', 'n')
            ->orderBy('n.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('LsCmsBundle:Services:index.html.twig', array(
            'entities' => $categories
        ));

    }

    public function listAction($slug){

        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('LsCmsBundle:ServicesCategory')->findOneBySlug($slug);

        $qb = $em->createQueryBuilder();
        $items = $qb
            ->select('n')
            ->from('LsCmsBundle:Services', 'n')
            ->where($qb->expr()->isNotNull('n.published_at'))
            ->where('n.category = :catId')
            ->setParameter('catId',$category->getId())
            ->addOrderBy('n.subcategory', 'DESC')
            ->addOrderBy('n.published_at', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('LsCmsBundle:Services:list.html.twig', array(
            'entities' => $items,
            'category' => $category
        ));

    }

    public function detailAction($catslug,$slug){

        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('LsCmsBundle:ServicesCategory')->findOneBySlug($catslug);
        $qb = $em->createQueryBuilder();
             $qb
            ->select('n')
            ->from('LsCmsBundle:Services', 'n')
            ->where($qb->expr()->isNotNull('n.published_at'));

            $qb->andWhere('n.category = :catId')
            ->setParameter('catId',$category->getId());
            $qb->andWhere('n.slug = :slug')
            ->setParameter('slug',$slug);

        $entity = $qb->getQuery()->getSingleResult();

        return $this->render('LsCmsBundle:Services:detail.html.twig', array(
            'entity' => $entity
        ));

    }

}