<?php

namespace Ls\CmsBundle\Controller;

use Ls\CmsBundle\Entity\PersonWizyta;
use Ls\CmsBundle\Entity\KarnetZamowienie;
use Ls\CmsBundle\Form\PersonWizytaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ls\CmsBundle\Utils\Tools;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotEqualTo;
use Symfony\Component\Validator\Constraints\NotBlank;

class DefaultController extends Controller {

    public function indexAction() {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        $projects = $qb->select(array('n'))
                ->from('LsCmsBundle:Project', 'n')
                ->where($qb->expr()->isNotNull('n.published_at'))
                ->addOrderBy('n.published_at', 'DESC')
                ->setMaxResults(3)
                ->getQuery()
                ->getResult();

        $qb = $em->createQueryBuilder();

        $services = $qb->select(array('n'))
            ->from('LsCmsBundle:ServicesCategory', 'n')
            ->addOrderBy('n.arrangement', 'ASC')
            ->setMaxResults(6)
            ->getQuery()
            ->getResult();


        $qb = $em->createQueryBuilder();
        $news = $qb->select('n')
                ->from('LsCmsBundle:News', 'n')
                ->where($qb->expr()->isNotNull('n.published_at'))
                ->orderBy('n.published_at', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

        if ($news != null) {
            $news->setContentShort(Tools::truncateWord($news->getContentShort(), 250, '...'));
        }


        $qb = $em->createQueryBuilder();
        $promo = $qb->select('n')
            ->from('LsCmsBundle:Promo', 'n')
            ->orderBy('n.arrangement', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();



        return $this->render('LsCmsBundle:Default:index.html.twig', array(
                    'projects' => $projects,
                    'services' => $services,
                    'news' => $news,
                    'promo' => $promo
        ));
    }

    public function getCategories(){
    $em = $this->getDoctrine()->getManager();

    $categories = $em->createQueryBuilder()
        ->select('n')
        ->from('LsCmsBundle:PoradaCategory', 'n')
        ->orderBy('n.arrangement', 'ASC')
        ->getQuery()
        ->getResult();



    return $categories;

    }

    public function getCategory($slug){
        $em = $this->getDoctrine()->getManager();

       /* $category = $em->createQueryBuilder()
            ->select('n')
            ->from('LsCmsBundle:PoradaCategory', 'n')
            ->where('n.id = '.$id)
            ->getQuery()
            ->getResult();*/

        $category = $em->getRepository('LsCmsBundle:PoradaCategory')->findOneBySlug($slug);


        return $category;

    }


    public function kontaktAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        $services = $qb->select(array('n'))
            ->from('LsCmsBundle:ServicesCategory', 'n')
            ->addOrderBy('n.arrangement', 'DESC')
            ->getQuery()
            ->getResult();

        // $services
        $choices = array('' => 'Wybierz usługę');
        foreach($services as $service){
            $choices[(string)$service] = (string)$service;
        }



        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('lscms_kontakt'))
                ->setMethod('POST')
                ->add('content', 'textarea', array(
                    'label' => 'Treść Twojej wiadomości...',
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Wypełnij pole'))
                    )
                        )
                )
                ->add('subject', 'choice', array(
                        'label' => 'Temat',
                        'required' => true,
                        'choices' => $choices
                    )
                )
                ->add('phone', null, array(
                        'label' => 'Phone',
                        'required' => true,
                        'constraints' => array(
                            new NotBlank(array('message' => 'Wypełnij pole'))
                        )
                    )
                )
                ->add('name', null, array(
                    'label' => 'Twoje imię...',
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Wypełnij pole'))
                    )
                        )
                )
                ->add('email', null, array(
                    'label' => 'E-mail zwrotny...',
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Wypełnij pole')),
                        new Email(array('message' => 'Podaj poprawny adres e-mail'))
                    )
                        )
                )
                ->getForm();

        $form->handleRequest($request);

        $success = false;
        if ($form->isValid()) {

            $email = $form->get('email')->getData();

            $message_txt = '<h3>Wiadomość z formularza kontaktowego: '.$form->get('subject')->getData().'</h3>';
            $message_txt .= nl2br($form->get('content')->getData()) . ' <hr />';
            $message_txt .= '<h3>Pozostałe dane:</h3>';
            $message_txt .= 'Imię: ' . $form->get('name')->getData() . '<br />';
            $message_txt .= 'E-mail: ' . $form->get('email')->getData() . '<br />';
            $message_txt .= 'Telefon: ' . $form->get('phone')->getData() . '<br />';

            $email_to = $em->getRepository('LsCmsBundle:Setting')->findOneByLabel('email_to_kontakt')->getValue();

            $message = \Swift_Message::newInstance()
                    ->setSubject('Wiadomość z formularza kontaktowego:'.$form->get('subject')->getData())
                    ->setFrom(array($this->container->getParameter('mailer_user') => 'Onyxx / Mailer'))
                    ->setTo($email_to)
                    ->setBody($message_txt, 'text/html')
                    ->addPart(strip_tags($message_txt), 'text/plain');

            if (!empty($email)) {
                $message->setReplyTo($email);
            }

            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->container->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);
            $success = true;
        }

        return $this->render('LsCmsBundle:Default:kontakt.html.twig', array(
                    'form' => $form->createView(),
                    'success' => $success

        ));
    }

    public function referencesAction(){

        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()
            ->select('n')
            ->from('LsCmsBundle:References', 'n')
            ->orderBy('n.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('LsCmsBundle:Default:references.html.twig', array(
            'entities' => $entities
        ));

    }

    public function partnersAction(){

        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()
            ->select('n')
            ->from('LsCmsBundle:Partners', 'n')
            ->orderBy('n.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('LsCmsBundle:Default:partners.html.twig', array(
            'entities' => $entities
        ));

    }

    public function promoAction(){

        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()
            ->select('n')
            ->from('LsCmsBundle:Promo', 'n')
            ->orderBy('n.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('LsCmsBundle:Default:promo.html.twig', array(
            'entities' => $entities
        ));

    }

    public function activityAction($slug = null,Request $request) {

        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $category = 0;
        if ($session->has('porada_category')) {
            $category = $session->get('porada_category');
        } else {
            $session->set('porada_category', $category);
        }
        $show = $request->query->get('show',null);

        $categories = $this->getCategories();

        if(!is_null($slug)){

            $categoryEntity = $this->getCategory($slug);

        }else{
            $categoryEntity = false;
        }



        $qb = $em->createQueryBuilder();
        $query = $qb->select('n')
                ->from('LsCmsBundle:Porada', 'n')
                ->where($qb->expr()->isNotNull('n.published_at'))
                ->orderBy('n.published_at', 'DESC');


        if(is_object($categoryEntity)){
            $query->where('n.category = :catId');
            $query->setParameter('catId',$categoryEntity->getId());
        }

        if(!is_null($show)){
            switch($show){
                case 'projekty':
                    $query->andWhere('n.page_type = 1');
                    break;
                case 'wydarzenia':
                    $query->andWhere('n.page_type = 0');
                    break;
                default:
            }
        }


        $entities = $query->getQuery()->getResult();

        // ->where('n.category_id = ?',$catId)
        foreach ($entities as $item) {
            $item->setContentShort(Tools::truncateWord($item->getContentShort(), 250, '...'));
        }

        $page = $this->get('request')->query->get('page', 1);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities, $page, 9
        );

        return $this->render('LsCmsBundle:Default:activity.html.twig', array(
                    'categories' => $categories,
                    'category' => $category,
                    'categoryEntity' => $categoryEntity,
                    'entities' => $pagination,
                    'show' => $show
        ));
    }



}
