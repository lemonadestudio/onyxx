-- phpMyAdmin SQL Dump
-- version 4.6.1
-- http://www.phpmyadmin.net
--
-- Host: lemonade.nazwa.pl:3306
-- Czas generowania: 28 Gru 2016, 09:14
-- Wersja serwera: 5.5.53-MariaDB
-- Wersja PHP: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `lemonade_84`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_classes`
--

CREATE TABLE `acl_classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_entries`
--

CREATE TABLE `acl_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `object_identity_id` int(10) UNSIGNED DEFAULT NULL,
  `security_identity_id` int(10) UNSIGNED NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) UNSIGNED NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_object_identities`
--

CREATE TABLE `acl_object_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_object_identity_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_object_identity_ancestors`
--

CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) UNSIGNED NOT NULL,
  `ancestor_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_security_identities`
--

CREATE TABLE `acl_security_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_group`
--

CREATE TABLE `fos_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `date_of_birth`, `firstname`, `lastname`, `website`, `biography`, `gender`, `locale`, `timezone`, `phone`, `facebook_uid`, `facebook_name`, `facebook_data`, `twitter_uid`, `twitter_name`, `twitter_data`, `gplus_uid`, `gplus_name`, `gplus_data`, `token`, `two_step_code`) VALUES
(1, 'admin', 'admin', 'test@test.pl', 'test@test.pl', 1, 'aqn5nzng0dck00kk8gogwkkc8c4wooc', '5I3l3ug/NzJzzLui1jzlIooHH1Cy4jqhDaukjHC/VuOoS8Vfk8kY8RHmAgEr4ra+Wpb/sOjIJO+28U/o+7envA==', '2015-09-17 14:05:35', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2014-04-01 15:18:49', '2015-09-17 14:05:35', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(2, 'rafalglazar', 'rafalglazar', 'rafalglazar@gmail.com', 'rafalglazar@gmail.com', 1, 't0uk2tmpg40cog80kkg04088sc48wo4', 'viAvhoyV8qazSD+crS7R5zspKj3+Rv4aeqCH9gitAU6RKnkFIdgH19RjtXPJtj23wdrxYVczR4rKGt9uI4ILLQ==', '2014-06-27 17:50:17', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2014-04-01 15:19:14', '2014-06-27 17:50:17', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(3, 'marcin', 'marcin', 'biuro@iammusic.pl', 'biuro@iammusic.pl', 1, 's9h29nkgj2ss04g0scgc40488w40s00', 'LNwX93etNOmeKxqqGOoTpDQ0PbcXUhCeiHcfByhlMMOUvkupmddlAqKcmiwCgZmL8zwGKSPuH6bAbFgulsevyw==', NULL, 0, 0, NULL, NULL, NULL, 'a:2:{i:0;s:10:"ROLE_ADMIN";i:1;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2014-06-16 19:27:49', '2014-06-16 19:28:01', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(4, 'superuser', 'superuser', 'luke1990@niepodam.pl', 'luke1990@niepodam.pl', 1, 'aqn5nzng0dck00kk8gogwkkc8c4wooc', 'y1k4ZkPg+VzgX6hqiyYz9nJjsLW3FTwyS6emayNxpEL/Xf+R8dtSJ15CctpQcO08Wv6EFiFwcgHk1TDbcHJoVA==', '2015-03-18 00:18:02', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2014-08-13 00:20:50', '2015-03-18 00:18:02', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(5, 'piotrek', 'piotrek', 'piotr@piotrjanczarek.pl', 'piotr@piotrjanczarek.pl', 0, '5v9gjge0bf4scw4oo4ooc8k4s00w88g', 'y91LRJ1sTWT3cIZSGXIKoCfqm/gM2g3RCozdhxR2qZV/xFpwUDJad1rWIaE1I6Lubdog9r9ZMq/ZzbyTmt0FGw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2014-08-26 01:01:07', '2014-08-26 01:01:07', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(6, 'Sylwia', 'sylwia', 'sylwia13s@gmail.com', 'sylwia13s@gmail.com', 1, 'qahocfow7ms0oow8sgwgwsc0k0okwgg', 'b4lfNKmfeUjT2bqmkq4Zo2NlwnhjYEeY7mDjaIaGnqyRa3crj+oYpH92tcqJLJ9lFDxapPlzU1ZnF1vZzACOMA==', '2014-09-04 12:20:15', 0, 0, NULL, NULL, NULL, 'a:3:{i:0;s:10:"ROLE_ADMIN";i:1;s:16:"ROLE_SUPER_ADMIN";i:2;s:22:"ROLE_ALLOWED_TO_SWITCH";}', 0, NULL, '2014-09-03 13:18:56', '2014-09-04 12:20:15', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(7, 'yogi', 'yogi', 'biuro@onyxx.pl', 'biuro@onyxx.pl', 1, '9ayipyqt0g84gk48cgo8g0k8ko0ckw4', 'wbJxaILAtYyZ9Aboff2tO56h44hOW5g7zZBjV1gXF7ZjIeNzOP35NoY6qmShOKZYS4VgRCJwzkrBrI/UgcOsFg==', '2015-10-17 11:49:53', 0, 0, NULL, NULL, NULL, 'a:2:{i:0;s:10:"ROLE_ADMIN";i:1;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2014-10-06 21:45:41', '2015-10-17 11:49:53', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user_user_group`
--

CREATE TABLE `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `on_list` tinyint(1) NOT NULL DEFAULT '0',
  `attachable` tinyint(1) NOT NULL DEFAULT '0',
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `gallery`
--

INSERT INTO `gallery` (`id`, `title`, `slug`, `on_list`, `attachable`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `content_short`, `content`) VALUES
(4, 'O nas', 'o-nas', 1, 1, 0, NULL, NULL, NULL, '2014-09-17 00:11:44', NULL, NULL, NULL),
(7, 'Porsche Panamera', 'porsche-panamera', 1, 1, 0, NULL, NULL, NULL, '2014-10-09 20:23:27', NULL, 'Nowiutkie Porsche Panamera przeszło u nas zbiegi pielęgnacji, korekty lakieru i zabezpieczenia powłoką kwarcową...', NULL),
(8, 'Dodge RAM', 'dodge-ram', 1, 1, 0, NULL, NULL, NULL, '2014-10-09 20:52:12', NULL, NULL, NULL),
(9, 'Lexus LS600H', 'lexus-ls600h', 1, 1, 0, NULL, NULL, NULL, '2014-10-19 21:57:52', NULL, NULL, NULL),
(10, 'Chevrolet Corvette C1', 'chevrolet-corvette-c1', 1, 1, 0, NULL, NULL, NULL, '2014-11-02 10:18:06', NULL, NULL, NULL),
(11, 'VW Touareg', 'vw-touareg', 1, 1, 0, NULL, NULL, NULL, '2014-11-03 20:58:53', NULL, NULL, NULL),
(12, 'Nowa firma', 'nowa-firma', 1, 1, 0, NULL, NULL, NULL, '2014-11-04 20:51:03', NULL, NULL, NULL),
(13, 'Hyundai IX35', 'hyundai-ix35', 1, 1, 0, NULL, NULL, NULL, '2014-11-06 19:57:45', NULL, NULL, NULL),
(14, 'Modesta', 'modesta', 1, 1, 0, NULL, NULL, NULL, '2014-11-06 22:13:09', NULL, NULL, NULL),
(15, 'Toyota Avensis III', 'toyota-avensis-iii', 1, 1, 0, NULL, NULL, NULL, '2014-11-07 18:56:42', NULL, NULL, NULL),
(16, 'Honda Accord S-type', 'honda-accord-s-type', 1, 1, 0, NULL, NULL, NULL, '2014-11-07 19:41:34', NULL, NULL, NULL),
(17, 'BMW 635csi', 'bmw-635csi', 1, 1, 0, NULL, NULL, NULL, '2014-11-07 20:37:19', NULL, NULL, NULL),
(18, 'Mercedes-Benz E-klasse W211', 'mercedes-benz-e-klasse-w211', 1, 1, 0, NULL, NULL, NULL, '2014-11-07 21:32:05', NULL, NULL, NULL),
(19, 'Renault Scenic', 'renault-scenic', 1, 1, 0, NULL, NULL, NULL, '2014-11-08 07:51:17', NULL, NULL, NULL),
(20, 'Skoda Octavia Scout', 'skoda-octavia-scout', 1, 1, 0, NULL, NULL, NULL, '2014-11-08 07:59:21', NULL, NULL, NULL),
(21, 'Audi A4 B7', 'audi-a4-b7', 1, 1, 0, NULL, NULL, NULL, '2014-11-08 08:12:06', NULL, NULL, NULL),
(22, 'Honda Accord', 'honda-accord', 1, 1, 0, NULL, NULL, NULL, '2014-11-09 10:27:30', NULL, NULL, NULL),
(23, 'Alfa Romeo 147', 'alfa-romeo-147', 1, 1, 0, NULL, NULL, NULL, '2014-11-09 10:41:36', NULL, NULL, NULL),
(24, 'Toyota Avensis II', 'toyota-avensis-ii', 1, 1, 0, NULL, NULL, NULL, '2014-11-09 11:06:47', NULL, NULL, NULL),
(25, 'VW Passat B5 FL', 'vw-passat-b5-fl', 1, 1, 0, NULL, NULL, NULL, '2014-11-09 11:21:29', NULL, NULL, NULL),
(26, 'BMW X6', 'bmw-x6', 1, 1, 0, NULL, NULL, NULL, '2014-11-09 11:49:23', NULL, NULL, NULL),
(27, 'BMW 645 & Mercedes SL65', 'bmw-645-mercedes-sl65', 1, 1, 0, NULL, NULL, NULL, '2014-11-09 12:07:19', NULL, NULL, NULL),
(28, 'Chevrolet Bel Air', 'chevrolet-bel-air', 1, 1, 0, NULL, NULL, NULL, '2014-11-09 12:53:45', NULL, NULL, NULL),
(29, 'Volvo S60', 'volvo-s60', 1, 1, 0, NULL, NULL, NULL, '2014-11-09 13:10:41', NULL, NULL, NULL),
(30, 'Alfa Romeo 159', 'alfa-romeo-159', 1, 1, 0, NULL, NULL, NULL, '2014-11-09 23:44:22', NULL, NULL, NULL),
(31, 'Mercedes-Benz S-klasse W220', 'mercedes-benz-s-klasse-w220', 1, 1, 0, NULL, NULL, NULL, '2014-11-10 00:04:14', NULL, NULL, NULL),
(32, 'Podstawowe mycie SPA', 'podstawowe-mycie-spa', 1, 1, 0, NULL, NULL, NULL, '2014-11-10 21:48:56', NULL, NULL, NULL),
(33, 'Pełne mycie SPA z dekontaminacją', 'pelne-mycie-spa-z-dekontaminacja', 1, 1, 0, NULL, NULL, NULL, '2014-11-10 22:14:22', NULL, NULL, NULL),
(34, 'Protect NEW Car', 'protect-new-car', 1, 1, 0, NULL, NULL, NULL, '2014-11-11 23:37:56', NULL, NULL, NULL),
(35, 'Reflektory', 'reflektory', 1, 1, 0, NULL, NULL, NULL, '2014-11-22 09:25:31', NULL, NULL, NULL),
(36, 'Korekta lakieru', 'korekta-lakieru', 1, 1, 0, NULL, NULL, NULL, '2014-11-23 11:04:09', NULL, NULL, NULL),
(37, 'Czyszczenie skóry', 'czyszczenie-skory', 1, 1, 0, NULL, NULL, NULL, '2014-11-23 12:39:14', NULL, NULL, NULL),
(38, 'Naprawa skóry', 'naprawa-skory', 1, 1, 0, NULL, NULL, NULL, '2014-11-23 22:13:33', NULL, NULL, NULL),
(39, 'Detailing wnętrza', 'detailing-wnetrza', 1, 1, 0, NULL, NULL, NULL, '2014-11-24 20:20:07', NULL, NULL, NULL),
(40, 'Zabezpieczenie lakieru powłoką', 'zabezpieczenie-lakieru-powloka', 1, 1, 0, NULL, NULL, NULL, '2014-12-04 18:16:22', NULL, NULL, NULL),
(41, 'Zabezpieczenie felg powłoką', 'zabezpieczenie-felg-powloka', 1, 1, 0, NULL, NULL, NULL, '2014-12-06 08:17:40', NULL, 'Zabieg przeznaczony dla osób, które chcą aby ich felgi jak najdłużej pozostały piękne a ich mycie to była czysta przyjemność. Koniec z usmarowanymi felgami na które trzeba wylać litry chemii aby doprowadzić je do ładu.', NULL),
(42, 'Ford Focus', 'ford-focus', 1, 1, 0, NULL, NULL, NULL, '2014-12-07 07:42:36', NULL, NULL, NULL),
(43, 'Sesje', 'sesje', 1, 1, 0, NULL, NULL, NULL, '2014-12-07 11:04:41', NULL, NULL, NULL),
(44, 'Infiniti FX50', 'infiniti-fx50', 1, 1, 0, NULL, NULL, NULL, '2014-12-21 10:49:28', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery_photo`
--

CREATE TABLE `gallery_photo` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `gallery_photo`
--

INSERT INTO `gallery_photo` (`id`, `gallery_id`, `photo`, `arrangement`) VALUES
(109, 7, 'gallery-image-5436d4fc78670.jpg', 1),
(110, 7, 'gallery-image-5436d4fd02516.jpg', 2),
(111, 7, 'gallery-image-5436d4fd5d4f3.jpg', 4),
(112, 7, 'gallery-image-5436d4fdb63f4.jpg', 5),
(113, 7, 'gallery-image-5436d4fe2c0f6.jpg', 3),
(114, 7, 'gallery-image-5436d4fe93bb1.jpg', 6),
(115, 7, 'gallery-image-5436d4ff03034.jpg', 7),
(116, 7, 'gallery-image-5436d4ff56ec5.jpg', 8),
(117, 7, 'gallery-image-5436d4ffa9be8.jpg', 9),
(118, 7, 'gallery-image-5436d50008957.jpg', 10),
(119, 7, 'gallery-image-5436d5005bd8c.jpg', 11),
(120, 7, 'gallery-image-5436d500af665.jpg', 12),
(121, 7, 'gallery-image-5436d5010c2c9.jpg', 13),
(122, 7, 'gallery-image-5436d5015e815.jpg', 14),
(123, 7, 'gallery-image-5436d501ca16c.jpg', 15),
(124, 7, 'gallery-image-5436d502281d6.jpg', 16),
(125, 7, 'gallery-image-5436d5027eb21.jpg', 17),
(126, 8, 'gallery-image-5436dfe17ae84.jpg', 1),
(127, 8, 'gallery-image-5436dfe1cca02.jpg', 2),
(128, 8, 'gallery-image-5436dfe224c10.jpg', 3),
(129, 8, 'gallery-image-5436dfe272f1d.jpg', 4),
(130, 8, 'gallery-image-5436dfe2c5893.jpg', 5),
(131, 8, 'gallery-image-5436dfe3208e6.jpg', 6),
(132, 8, 'gallery-image-5436dfe368e91.jpg', 7),
(133, 8, 'gallery-image-5436dfe3b4e95.jpg', 8),
(134, 8, 'gallery-image-5436dfe40ed63.jpg', 9),
(135, 8, 'gallery-image-5436dfe4583e6.jpg', 10),
(136, 8, 'gallery-image-5436dfe4a26af.jpg', 11),
(137, 8, 'gallery-image-5436dfe4ea123.jpg', 12),
(138, 8, 'gallery-image-5436dfe53fa81.jpg', 13),
(139, 8, 'gallery-image-5436dfe58d231.jpg', 14),
(140, 8, 'gallery-image-5436dfe5d75dd.jpg', 15),
(141, 8, 'gallery-image-5436dfe62feee.jpg', 16),
(142, 8, 'gallery-image-5436dfe67ad33.jpg', 17),
(143, 8, 'gallery-image-5436dfe6d9b32.jpg', 18),
(144, 8, 'gallery-image-5436dfe74d5cf.jpg', 19),
(145, 8, 'gallery-image-5436dfe79648b.jpg', 20),
(146, 8, 'gallery-image-5436dfe7dd78e.jpg', 21),
(147, 8, 'gallery-image-5436dfe831a6c.jpg', 22),
(148, 8, 'gallery-image-5436dfe87c310.jpg', 23),
(149, 8, 'gallery-image-5436dfe8c6e6b.jpg', 24),
(150, 8, 'gallery-image-5436dfe91d5cd.jpg', 25),
(151, 8, 'gallery-image-5436dfe9683d5.jpg', 26),
(152, 8, 'gallery-image-5436dfe9b1bff.jpg', 27),
(153, 8, 'gallery-image-5436dfea071f0.jpg', 28),
(154, 8, 'gallery-image-5436dfea50738.jpg', 29),
(155, 8, 'gallery-image-5436dfea97967.jpg', 30),
(156, 8, 'gallery-image-5436dfeae064f.jpg', 31),
(157, 8, 'gallery-image-5436dfeb3849f.jpg', 32),
(158, 8, 'gallery-image-5436dfeb80585.jpg', 33),
(159, 8, 'gallery-image-5436dfebe6bbd.jpg', 34),
(160, 8, 'gallery-image-5436dfec52ff5.jpg', 35),
(161, 8, 'gallery-image-5436dfecbc1a5.jpg', 36),
(162, 8, 'gallery-image-5436dfed2d6f5.jpg', 37),
(163, 8, 'gallery-image-5436dfede8f26.jpg', 38),
(164, 8, 'gallery-image-5436dfee6cf50.jpg', 39),
(165, 8, 'gallery-image-5436dfeebd9d4.jpg', 40),
(166, 8, 'gallery-image-5436dfef47028.jpg', 41),
(167, 8, 'gallery-image-5436dfefa2b7a.jpg', 42),
(168, 8, 'gallery-image-5436dff024eb6.jpg', 43),
(169, 8, 'gallery-image-5436dff09cbef.jpg', 44),
(170, 8, 'gallery-image-5436dff0e5fa3.jpg', 45),
(171, 8, 'gallery-image-5436dff13e56b.jpg', 46),
(172, 8, 'gallery-image-5436dff1a00ed.jpg', 47),
(173, 8, 'gallery-image-5436dff1e458a.jpg', 48),
(174, 8, 'gallery-image-5436dff23613a.jpg', 49),
(175, 8, 'gallery-image-5436dff279ff9.jpg', 50),
(176, 8, 'gallery-image-5436dff2c3f45.jpg', 51),
(177, 8, 'gallery-image-5436dff316b5b.jpg', 52),
(178, 8, 'gallery-image-5436dff35bd87.jpg', 53),
(179, 8, 'gallery-image-5436dff3a3238.jpg', 54),
(180, 8, 'gallery-image-5436dff3e9835.jpg', 55),
(181, 8, 'gallery-image-5436dff43abb8.jpg', 56),
(182, 8, 'gallery-image-5436dff47cc32.jpg', 57),
(183, 8, 'gallery-image-5436dff4c2624.jpg', 58),
(184, 8, 'gallery-image-5436dff5137a8.jpg', 59),
(185, 8, 'gallery-image-5436dff559597.jpg', 60),
(186, 8, 'gallery-image-5436dff5a060e.jpg', 61),
(187, 8, 'gallery-image-5436dff5ec6c1.jpg', 62),
(188, 8, 'gallery-image-5436dff63d185.jpg', 63),
(189, 8, 'gallery-image-5436dff6993b6.jpg', 64),
(190, 8, 'gallery-image-5436dff6dfdd4.jpg', 65),
(191, 8, 'gallery-image-5436dff72f420.jpg', 66),
(192, 8, 'gallery-image-5436dff7794e9.jpg', 67),
(193, 8, 'gallery-image-5436dff7ba4db.jpg', 68),
(194, 8, 'gallery-image-5436dff80d359.jpg', 69),
(195, 8, 'gallery-image-5436dff85b68c.jpg', 70),
(196, 8, 'gallery-image-5436dff8ba5ac.jpg', 71),
(197, 8, 'gallery-image-5436dff90e602.jpg', 72),
(198, 8, 'gallery-image-5436dff9754e0.jpg', 73),
(199, 8, 'gallery-image-5436dff9c00bd.jpg', 74),
(200, 8, 'gallery-image-5436dffa33590.jpg', 75),
(201, 8, 'gallery-image-5436dffa7dc95.jpg', 76),
(202, 8, 'gallery-image-5436dffac8420.jpg', 77),
(203, 8, 'gallery-image-5436dffb1d1e4.jpg', 78),
(204, 8, 'gallery-image-5436dffb66079.jpg', 79),
(205, 8, 'gallery-image-5436dffbb2c54.jpg', 80),
(206, 8, 'gallery-image-5436dffc0bbca.jpg', 81),
(207, 8, 'gallery-image-5436dffc59f5e.jpg', 82),
(208, 8, 'gallery-image-5436dffca660e.jpg', 83),
(209, 8, 'gallery-image-5436dffcf2b3f.jpg', 84),
(210, 8, 'gallery-image-5436dffd4a0d8.jpg', 85),
(211, 8, 'gallery-image-5436dffd96add.jpg', 86),
(212, 8, 'gallery-image-5436dffde7298.jpg', 87),
(213, 8, 'gallery-image-5436dffe3d659.jpg', 88),
(214, 8, 'gallery-image-5436dffe85bfe.jpg', 89),
(215, 8, 'gallery-image-5436dffed3d38.jpg', 90),
(216, 8, 'gallery-image-5436dfff2cd8a.jpg', 91),
(217, 8, 'gallery-image-5436dfff7b1a7.jpg', 92),
(218, 8, 'gallery-image-5436dfffc4ff2.jpg', 93),
(219, 8, 'gallery-image-5436e0001bcdc.jpg', 94),
(220, 8, 'gallery-image-5436e00068c18.jpg', 95),
(221, 8, 'gallery-image-5436e000b376f.jpg', 96),
(222, 8, 'gallery-image-5436e0010845d.jpg', 97),
(223, 8, 'gallery-image-5436e00150046.jpg', 98),
(224, 8, 'gallery-image-5436e0019b186.jpg', 99),
(225, 8, 'gallery-image-5436e001e84e4.jpg', 100),
(226, 8, 'gallery-image-5436e0024082f.jpg', 101),
(227, 8, 'gallery-image-5436e0029400f.jpg', 102),
(228, 8, 'gallery-image-5436e002dbdfc.jpg', 103),
(229, 8, 'gallery-image-5436e00336def.jpg', 104),
(230, 8, 'gallery-image-5436e0038517f.jpg', 105),
(231, 8, 'gallery-image-5436e003d4b51.jpg', 106),
(232, 8, 'gallery-image-5436e00434101.jpg', 107),
(233, 8, 'gallery-image-5436e0047ef92.jpg', 108),
(234, 8, 'gallery-image-5436e004c9d87.jpg', 109),
(235, 8, 'gallery-image-5436e0051ecb5.jpg', 110),
(236, 8, 'gallery-image-5436e0056a747.jpg', 111),
(237, 8, 'gallery-image-5436e005b72e7.jpg', 112),
(238, 8, 'gallery-image-5436e00617e0c.jpg', 113),
(239, 8, 'gallery-image-5436e0065d16f.jpg', 114),
(240, 8, 'gallery-image-5436e006a8b4e.jpg', 115),
(241, 8, 'gallery-image-5436e007023b7.jpg', 116),
(242, 8, 'gallery-image-5436e0074d2d7.jpg', 117),
(243, 8, 'gallery-image-5436e00796b52.jpg', 118),
(244, 8, 'gallery-image-5436e007df720.jpg', 119),
(245, 8, 'gallery-image-5436e0083a268.jpg', 120),
(246, 8, 'gallery-image-5436e00887b75.jpg', 121),
(247, 8, 'gallery-image-5436e008d082d.jpg', 122),
(248, 8, 'gallery-image-5436e00926269.jpg', 123),
(249, 8, 'gallery-image-5436e0096fa54.jpg', 124),
(250, 8, 'gallery-image-5436e009bb482.jpg', 125),
(251, 8, 'gallery-image-5436e00a14280.jpg', 126),
(252, 8, 'gallery-image-5436e00a5e33b.jpg', 127),
(253, 8, 'gallery-image-5436e00aa7451.jpg', 128),
(254, 8, 'gallery-image-5436e00af2a1c.jpg', 129),
(255, 8, 'gallery-image-5436e00b4c89c.jpg', 130),
(256, 8, 'gallery-image-5436e00b9930b.jpg', 131),
(257, 8, 'gallery-image-5436e00be3940.jpg', 132),
(258, 9, 'gallery-image-5444188314f19.jpg', 1),
(259, 9, 'gallery-image-54441883c6acc.jpg', 2),
(260, 9, 'gallery-image-544418846498d.jpg', 3),
(261, 9, 'gallery-image-54441884f37b9.jpg', 4),
(262, 9, 'gallery-image-544418858cddc.jpg', 5),
(263, 9, 'gallery-image-5444188629190.jpg', 6),
(264, 9, 'gallery-image-54441886ba74a.jpg', 7),
(265, 9, 'gallery-image-5444188757fd5.jpg', 8),
(266, 9, 'gallery-image-5444188809b0a.jpg', 9),
(267, 9, 'gallery-image-544418889b2b2.jpg', 10),
(268, 9, 'gallery-image-5444188936fa2.jpg', 11),
(269, 9, 'gallery-image-54441889c6be7.jpg', 12),
(270, 9, 'gallery-image-5444188a6074c.jpg', 13),
(271, 9, 'gallery-image-5444188b05481.jpg', 14),
(272, 9, 'gallery-image-5444188b96ea5.jpg', 15),
(273, 9, 'gallery-image-5444188c32650.jpg', 16),
(274, 9, 'gallery-image-5444188cd7259.jpg', 17),
(275, 9, 'gallery-image-5444188d73693.jpg', 18),
(276, 9, 'gallery-image-5444188e12bc2.jpg', 19),
(277, 9, 'gallery-image-5444188e9fca9.jpg', 20),
(278, 9, 'gallery-image-5444188f3d8de.jpg', 21),
(279, 9, 'gallery-image-5444188fcb657.jpg', 22),
(280, 9, 'gallery-image-544418905df7d.jpg', 23),
(281, 9, 'gallery-image-54441890e55d5.jpg', 24),
(282, 9, 'gallery-image-54441891809ae.jpg', 25),
(283, 9, 'gallery-image-54441892598b4.jpg', 26),
(284, 9, 'gallery-image-544418934ae82.jpg', 27),
(285, 9, 'gallery-image-54441893d610f.jpg', 28),
(286, 9, 'gallery-image-54441894c1b70.jpg', 29),
(287, 9, 'gallery-image-5444189599e74.jpg', 30),
(288, 9, 'gallery-image-544418968fde0.jpg', 31),
(289, 9, 'gallery-image-54441897a6e5b.jpg', 32),
(290, 9, 'gallery-image-54441898c6224.jpg', 33),
(291, 9, 'gallery-image-54441899afd63.jpg', 34),
(292, 9, 'gallery-image-5444189a5b426.jpg', 35),
(293, 9, 'gallery-image-5444189af2c1a.jpg', 36),
(294, 9, 'gallery-image-5444189b8f9d2.jpg', 37),
(295, 9, 'gallery-image-5444189c2addd.jpg', 38),
(296, 9, 'gallery-image-5444189cb7686.jpg', 39),
(297, 9, 'gallery-image-5444189d54a7c.jpg', 40),
(298, 9, 'gallery-image-5444189de68b5.jpg', 41),
(299, 9, 'gallery-image-5444189e829b8.jpg', 42),
(300, 9, 'gallery-image-5444189f264d2.jpg', 43),
(301, 9, 'gallery-image-5444189fb4fd8.jpg', 44),
(302, 9, 'gallery-image-544418a0530dc.jpg', 45),
(303, 9, 'gallery-image-544418a1031eb.jpg', 46),
(304, 9, 'gallery-image-544418a194d61.jpg', 47),
(305, 9, 'gallery-image-544418a23796e.jpg', 48),
(306, 10, 'gallery-image-5455f77978bc0.jpg', 1),
(307, 10, 'gallery-image-5455f779ef362.jpg', 2),
(308, 10, 'gallery-image-5455f77a4ee1f.jpg', 3),
(309, 10, 'gallery-image-5455f77aaa7bd.jpg', 4),
(310, 10, 'gallery-image-5455f77b1a809.jpg', 5),
(311, 10, 'gallery-image-5455f77b6946e.jpg', 6),
(312, 10, 'gallery-image-5455f77bb8d70.jpg', 7),
(313, 10, 'gallery-image-5455f77c14422.jpg', 8),
(314, 10, 'gallery-image-5455f77c62b9b.jpg', 9),
(315, 10, 'gallery-image-5455f77cb4572.jpg', 10),
(316, 10, 'gallery-image-5455f77d0d27f.jpg', 11),
(317, 10, 'gallery-image-5455f77d5d1e2.jpg', 12),
(318, 10, 'gallery-image-5455f77dacf87.jpg', 13),
(319, 10, 'gallery-image-5455f77e2fa86.jpg', 14),
(320, 10, 'gallery-image-5455f77e9199d.jpg', 15),
(321, 10, 'gallery-image-5455f77ee1d66.jpg', 16),
(322, 10, 'gallery-image-5455f77f3d071.jpg', 17),
(323, 10, 'gallery-image-5455f77f8cb88.jpg', 18),
(324, 10, 'gallery-image-5455f77fdc826.jpg', 19),
(325, 10, 'gallery-image-5455f78036533.jpg', 20),
(326, 10, 'gallery-image-5455f78087c21.jpg', 21),
(327, 10, 'gallery-image-5455f780d494a.jpg', 22),
(328, 10, 'gallery-image-5455f7812cdb0.jpg', 23),
(329, 10, 'gallery-image-5455f78187353.jpg', 24),
(330, 10, 'gallery-image-5455f781d72c2.jpg', 25),
(331, 10, 'gallery-image-5455f7823ad93.jpg', 26),
(332, 10, 'gallery-image-5455f7828a372.jpg', 27),
(333, 10, 'gallery-image-5455f782d9dea.jpg', 28),
(334, 10, 'gallery-image-5455f78333d78.jpg', 29),
(335, 10, 'gallery-image-5455f783814fe.jpg', 30),
(336, 10, 'gallery-image-5455f783cea15.jpg', 31),
(337, 10, 'gallery-image-5455f784272ca.jpg', 32),
(338, 10, 'gallery-image-5455f784727d3.jpg', 33),
(339, 10, 'gallery-image-5455f784c134a.jpg', 34),
(340, 10, 'gallery-image-5455f785174cd.jpg', 35),
(341, 10, 'gallery-image-5455f785625e8.jpg', 36),
(342, 10, 'gallery-image-5455f785ad974.jpg', 37),
(343, 10, 'gallery-image-5455f7861cd52.jpg', 38),
(344, 10, 'gallery-image-5455f78669488.jpg', 39),
(345, 10, 'gallery-image-5455f786b1840.jpg', 40),
(346, 10, 'gallery-image-5455f78705a86.jpg', 41),
(347, 10, 'gallery-image-5455f7874eaed.jpg', 42),
(348, 10, 'gallery-image-5455f787b4169.jpg', 43),
(349, 10, 'gallery-image-5455f7880cb67.jpg', 44),
(350, 10, 'gallery-image-5455f7885432c.jpg', 45),
(351, 10, 'gallery-image-5455f7889e8d7.jpg', 46),
(352, 10, 'gallery-image-5455f788ed513.jpg', 47),
(353, 10, 'gallery-image-5455f78940ee4.jpg', 48),
(354, 10, 'gallery-image-5455f78987e27.jpg', 49),
(355, 10, 'gallery-image-5455f78a025a1.jpg', 50),
(356, 10, 'gallery-image-5455f78a52326.jpg', 51),
(357, 10, 'gallery-image-5455f78a9da32.jpg', 52),
(358, 10, 'gallery-image-5455f78ae9b9a.jpg', 53),
(359, 10, 'gallery-image-5455f78b492ea.jpg', 54),
(360, 10, 'gallery-image-5455f78b91a45.jpg', 55),
(361, 10, 'gallery-image-5455f78bdab89.jpg', 56),
(362, 10, 'gallery-image-5455f78c2f1cf.jpg', 57),
(363, 10, 'gallery-image-5455f78c917ff.jpg', 58),
(364, 10, 'gallery-image-5455f78cd805b.jpg', 59),
(365, 10, 'gallery-image-5455f78d28dae.jpg', 60),
(366, 10, 'gallery-image-5455f78d8aecd.jpg', 61),
(367, 10, 'gallery-image-5455f78dd55c6.jpg', 62),
(368, 10, 'gallery-image-5455f78e4f574.jpg', 63),
(369, 10, 'gallery-image-5455f78ea1cf0.jpg', 64),
(370, 10, 'gallery-image-5455f78f20c1b.jpg', 65),
(371, 10, 'gallery-image-5455f78f624ae.jpg', 66),
(372, 10, 'gallery-image-5455f78fa551a.jpg', 67),
(373, 10, 'gallery-image-5455f78fec517.jpg', 68),
(374, 10, 'gallery-image-5455f7903e7d0.jpg', 69),
(375, 10, 'gallery-image-5455f790886e1.jpg', 70),
(376, 10, 'gallery-image-5455f790d59f3.jpg', 71),
(377, 10, 'gallery-image-5455f7912c0d5.jpg', 72),
(378, 10, 'gallery-image-5455f7917afc2.jpg', 73),
(379, 10, 'gallery-image-5455f791f2275.jpg', 74),
(380, 10, 'gallery-image-5455f79275035.jpg', 75),
(381, 10, 'gallery-image-5455f79318c12.jpg', 76),
(382, 10, 'gallery-image-5455f7938dd0b.jpg', 77),
(385, 11, 'gallery-image-5457e399c715d.jpg', 1),
(386, 11, 'gallery-image-5457e39af2c92.jpg', 2),
(387, 11, 'gallery-image-5457e39ba747f.jpg', 3),
(388, 11, 'gallery-image-5457e39cb0761.jpg', 4),
(389, 11, 'gallery-image-5457e39d1ff2a.jpg', 5),
(390, 11, 'gallery-image-5457e39d84363.jpg', 6),
(391, 11, 'gallery-image-5457e39edfbb1.jpg', 7),
(392, 11, 'gallery-image-5457e3d14c3d0.jpg', 8),
(393, 11, 'gallery-image-5457e3d243de7.jpg', 9),
(394, 11, 'gallery-image-5457e3d3340e0.jpg', 10),
(395, 11, 'gallery-image-5457e3d49a63b.jpg', 11),
(396, 11, 'gallery-image-5457e3d5bd987.jpg', 12),
(397, 11, 'gallery-image-5457e3d68aefc.jpg', 13),
(398, 11, 'gallery-image-5457e3d7e02cf.jpg', 14),
(399, 11, 'gallery-image-5457e3d891f16.jpg', 15),
(400, 11, 'gallery-image-5457e3d9abf1a.jpg', 16),
(401, 11, 'gallery-image-5457e3dc2d54c.jpg', 17),
(402, 11, 'gallery-image-5457e3dd4c8b0.jpg', 18),
(403, 11, 'gallery-image-5457e3df6bf06.jpg', 19),
(404, 11, 'gallery-image-5457e434c94bb.jpg', 20),
(405, 11, 'gallery-image-5457e437533ce.jpg', 21),
(406, 11, 'gallery-image-5457e4393db64.jpg', 22),
(407, 11, 'gallery-image-5457e43babc41.jpg', 23),
(408, 11, 'gallery-image-5457e43d24492.jpg', 24),
(409, 11, 'gallery-image-5457e43fb2e69.jpg', 25),
(410, 11, 'gallery-image-5457e44260708.jpg', 26),
(411, 11, 'gallery-image-5457e4445714f.jpg', 27),
(412, 11, 'gallery-image-5457e44561e33.jpg', 28),
(413, 11, 'gallery-image-5457e446b4e6b.jpg', 29),
(414, 11, 'gallery-image-5457e4481c546.jpg', 30),
(415, 11, 'gallery-image-5457e449696aa.jpg', 31),
(416, 11, 'gallery-image-5457e44b3a4c4.jpg', 32),
(417, 11, 'gallery-image-5457e44daeb28.jpg', 33),
(418, 11, 'gallery-image-5457e44f9fb63.jpg', 34),
(419, 11, 'gallery-image-5457e450a91eb.jpg', 35),
(420, 11, 'gallery-image-5457e4513eea3.jpg', 36),
(421, 11, 'gallery-image-5457e4527f864.jpg', 37),
(422, 11, 'gallery-image-5457e4543cb3c.jpg', 38),
(423, 11, 'gallery-image-5457e454df4c6.jpg', 39),
(424, 11, 'gallery-image-5457e455cba89.jpg', 40),
(425, 11, 'gallery-image-5457e4573002e.jpg', 41),
(426, 11, 'gallery-image-5457e45802e82.jpg', 42),
(427, 11, 'gallery-image-5457e45a81974.jpg', 43),
(428, 11, 'gallery-image-5457e45d82e46.jpg', 44),
(429, 11, 'gallery-image-5457e45f3f3b6.jpg', 45),
(430, 11, 'gallery-image-5457e46044981.jpg', 46),
(431, 11, 'gallery-image-5457e46238abb.jpg', 47),
(432, 12, 'gallery-image-54592e5bd1a4c.jpg', 1),
(433, 12, 'gallery-image-54592e734fedb.jpg', 2),
(434, 12, 'gallery-image-54592e74c34ec.jpg', 3),
(435, 12, 'gallery-image-54592e769d25c.jpg', 4),
(436, 13, 'gallery-image-545bc4d84c648.jpg', 1),
(437, 13, 'gallery-image-545bc4d8da315.jpg', 2),
(438, 13, 'gallery-image-545bc4d975de2.jpg', 3),
(439, 13, 'gallery-image-545bc4da0d626.jpg', 4),
(440, 13, 'gallery-image-545bc4daa86f4.jpg', 5),
(441, 13, 'gallery-image-545bc4db45ec2.jpg', 6),
(442, 13, 'gallery-image-545bc4dbdb286.jpg', 7),
(443, 13, 'gallery-image-545bc4dc767d5.jpg', 8),
(444, 13, 'gallery-image-545bc4dd2af29.jpg', 9),
(445, 13, 'gallery-image-545bc4ddd56f7.jpg', 10),
(446, 4, 'gallery-image-545bdfa7b8320.jpg', 1),
(447, 4, 'gallery-image-545bdfda5396c.jpg', 2),
(448, 4, 'gallery-image-545bdfe5a4e28.jpg', 3),
(449, 4, 'gallery-image-545bdfeedcca7.jpg', 4),
(451, 14, 'gallery-image-545be48848f41.png', 1),
(452, 14, 'gallery-image-545be488a3090.jpg', 2),
(453, 14, 'gallery-image-545be4890ae01.png', 3),
(454, 15, 'gallery-image-545d083ea8d48.jpg', 1),
(455, 15, 'gallery-image-545d083f5b1ba.jpg', 2),
(456, 15, 'gallery-image-545d083fdff49.jpg', 3),
(457, 15, 'gallery-image-545d084078f50.jpg', 4),
(458, 15, 'gallery-image-545d084116fe1.jpg', 5),
(459, 15, 'gallery-image-545d0841a4f0c.jpg', 6),
(460, 15, 'gallery-image-545d08423d430.jpg', 7),
(461, 15, 'gallery-image-545d0842cd65c.jpg', 8),
(462, 15, 'gallery-image-545d0843636e7.jpg', 9),
(463, 15, 'gallery-image-545d0843f2189.jpg', 10),
(464, 15, 'gallery-image-545d08448cfdc.jpg', 11),
(465, 15, 'gallery-image-545d084504ed2.jpg', 12),
(466, 15, 'gallery-image-545d08456fa5f.jpg', 13),
(467, 15, 'gallery-image-545d08460e5fa.jpg', 14),
(468, 15, 'gallery-image-545d08469f0b3.jpg', 15),
(469, 15, 'gallery-image-545d08473d681.jpg', 16),
(470, 15, 'gallery-image-545d0847d4e45.jpg', 17),
(471, 15, 'gallery-image-545d08487e2f6.jpg', 18),
(472, 15, 'gallery-image-545d08491ceaa.jpg', 19),
(473, 15, 'gallery-image-545d0849a76fd.jpg', 20),
(474, 15, 'gallery-image-545d084a42efd.jpg', 21),
(475, 15, 'gallery-image-545d084ad34c1.jpg', 22),
(476, 15, 'gallery-image-545d084b69bce.jpg', 23),
(477, 15, 'gallery-image-545d084c06c23.jpg', 24),
(478, 16, 'gallery-image-545d12a6a11d2.jpg', 1),
(479, 16, 'gallery-image-545d12a76b1b8.jpg', 2),
(480, 16, 'gallery-image-545d12a8149ad.jpg', 3),
(481, 16, 'gallery-image-545d12a918a30.jpg', 4),
(482, 16, 'gallery-image-545d12a9b386b.jpg', 5),
(483, 16, 'gallery-image-545d12aa4ee51.jpg', 6),
(484, 16, 'gallery-image-545d12ab18785.jpg', 7),
(485, 16, 'gallery-image-545d12abf17a1.jpg', 8),
(486, 16, 'gallery-image-545d12ac922d5.jpg', 9),
(487, 16, 'gallery-image-545d12ad295c5.jpg', 10),
(488, 16, 'gallery-image-545d12adb87a4.jpg', 11),
(489, 16, 'gallery-image-545d12ae5bda7.jpg', 12),
(490, 16, 'gallery-image-545d12af3344d.jpg', 13),
(491, 16, 'gallery-image-545d12b07dbcc.jpg', 14),
(492, 16, 'gallery-image-545d12b13a623.jpg', 15),
(493, 17, 'gallery-image-545d1fc90e8c6.jpg', 1),
(494, 17, 'gallery-image-545d1fc9a55be.jpg', 2),
(495, 17, 'gallery-image-545d1fca453bf.jpg', 3),
(496, 17, 'gallery-image-545d1fcadb3cf.jpg', 4),
(497, 17, 'gallery-image-545d1fcb79d16.jpg', 5),
(498, 17, 'gallery-image-545d1fcc184b9.jpg', 6),
(499, 17, 'gallery-image-545d1fcca4fe3.jpg', 7),
(500, 17, 'gallery-image-545d1fcd467a3.jpg', 8),
(501, 17, 'gallery-image-545d1fcdd796b.jpg', 9),
(502, 17, 'gallery-image-545d1fce72ff2.jpg', 10),
(503, 17, 'gallery-image-545d1fcf10af2.jpg', 11),
(504, 17, 'gallery-image-545d1fcfa6422.jpg', 12),
(505, 17, 'gallery-image-545d1fd040973.jpg', 13),
(506, 17, 'gallery-image-545d1fd0ea670.jpg', 14),
(507, 17, 'gallery-image-545d1fd1900d2.jpg', 15),
(508, 17, 'gallery-image-545d1fd22f4cf.jpg', 16),
(509, 17, 'gallery-image-545d1fd2c4ca9.jpg', 17),
(510, 17, 'gallery-image-545d1fd37ba7a.jpg', 18),
(511, 17, 'gallery-image-545d1fd41e8cc.jpg', 19),
(512, 17, 'gallery-image-545d1fd4b3242.jpg', 20),
(513, 17, 'gallery-image-545d1fd550c48.jpg', 21),
(514, 17, 'gallery-image-545d1fd5e28ae.jpg', 22),
(515, 17, 'gallery-image-545d1fd683c59.jpg', 23),
(516, 17, 'gallery-image-545d208b4f3e4.jpg', 24),
(517, 17, 'gallery-image-545d208c19804.jpg', 25),
(518, 17, 'gallery-image-545d208ca87a0.jpg', 26),
(519, 17, 'gallery-image-545d208d59024.jpg', 27),
(520, 17, 'gallery-image-545d208e0c066.jpg', 28),
(521, 17, 'gallery-image-545d208e9c837.jpg', 29),
(522, 17, 'gallery-image-545d208f452c8.jpg', 30),
(523, 17, 'gallery-image-545d208fdf39f.jpg', 31),
(524, 17, 'gallery-image-545d20907551c.jpg', 32),
(525, 17, 'gallery-image-545d20911a63e.jpg', 33),
(526, 17, 'gallery-image-545d2091af951.jpg', 34),
(527, 17, 'gallery-image-545d209243eae.jpg', 35),
(528, 17, 'gallery-image-545d2092d2916.jpg', 36),
(529, 17, 'gallery-image-545d209371b82.jpg', 37),
(530, 17, 'gallery-image-545d20940dfcb.jpg', 38),
(531, 17, 'gallery-image-545d2094a6cbf.jpg', 39),
(532, 17, 'gallery-image-545d209545164.jpg', 40),
(533, 17, 'gallery-image-545d2095dd783.jpg', 41),
(534, 17, 'gallery-image-545d2096827cd.jpg', 42),
(535, 17, 'gallery-image-545d20971c4ab.jpg', 43),
(536, 17, 'gallery-image-545d2097a5419.jpg', 44),
(537, 17, 'gallery-image-545d2097e9542.jpg', 45),
(538, 17, 'gallery-image-545d2098407cb.jpg', 46),
(539, 18, 'gallery-image-545d2ca32714c.jpg', 1),
(540, 18, 'gallery-image-545d2ca3cb917.jpg', 2),
(541, 18, 'gallery-image-545d2ca46f0ea.jpg', 3),
(542, 18, 'gallery-image-545d2ca5181dc.jpg', 4),
(543, 18, 'gallery-image-545d2ca5ae61f.jpg', 5),
(544, 18, 'gallery-image-545d2ca64d1d9.jpg', 6),
(545, 18, 'gallery-image-545d2ca6de651.jpg', 7),
(546, 18, 'gallery-image-545d2ca798753.jpg', 8),
(547, 18, 'gallery-image-545d2ca85500c.jpg', 9),
(548, 18, 'gallery-image-545d2ca939060.jpg', 10),
(549, 18, 'gallery-image-545d2ca9d1528.jpg', 11),
(550, 18, 'gallery-image-545d2caa83ae2.jpg', 12),
(551, 18, 'gallery-image-545d2cab258d8.jpg', 13),
(552, 18, 'gallery-image-545d2cabcad60.jpg', 14),
(553, 19, 'gallery-image-545dbd9ca7a59.jpg', 1),
(554, 19, 'gallery-image-545dbd9d0d3ca.jpg', 2),
(555, 19, 'gallery-image-545dbd9d7a872.jpg', 3),
(556, 19, 'gallery-image-545dbd9e1938f.jpg', 4),
(557, 19, 'gallery-image-545dbd9ed65c7.jpg', 5),
(558, 19, 'gallery-image-545dbd9f8e980.jpg', 6),
(559, 19, 'gallery-image-545dbda048eb6.jpg', 7),
(560, 19, 'gallery-image-545dbda0ba387.jpg', 8),
(561, 20, 'gallery-image-545dbfb60a9d2.jpg', 1),
(562, 20, 'gallery-image-545dbfb69aee0.jpg', 2),
(563, 20, 'gallery-image-545dbfb733c57.jpg', 3),
(564, 20, 'gallery-image-545dbfb7c3e0a.jpg', 4),
(565, 20, 'gallery-image-545dbfb879e55.jpg', 5),
(566, 20, 'gallery-image-545dbfb960803.jpg', 6),
(567, 20, 'gallery-image-545dbfba0e732.jpg', 7),
(568, 20, 'gallery-image-545dbfbaa56ee.jpg', 8),
(569, 20, 'gallery-image-545dbfbb3d6fd.jpg', 9),
(570, 20, 'gallery-image-545dbfbbcc67b.jpg', 10),
(571, 20, 'gallery-image-545dbfbc9a685.jpg', 11),
(572, 20, 'gallery-image-545dbfbd6de45.jpg', 12),
(573, 20, 'gallery-image-545dbfbe0c1d7.jpg', 13),
(574, 20, 'gallery-image-545dbfbe9b3cd.jpg', 14),
(575, 20, 'gallery-image-545dbfbf33c85.jpg', 15),
(576, 20, 'gallery-image-545dbfbfe1f0c.jpg', 16),
(577, 20, 'gallery-image-545dbfc0d3528.jpg', 17),
(578, 20, 'gallery-image-545dbfc1712ea.jpg', 18),
(579, 21, 'gallery-image-545dc298e7a9a.jpg', 1),
(580, 21, 'gallery-image-545dc29988cbc.jpg', 2),
(581, 21, 'gallery-image-545dc29a26d60.jpg', 3),
(582, 21, 'gallery-image-545dc29ab96ec.jpg', 4),
(583, 21, 'gallery-image-545dc29b5906f.jpg', 5),
(584, 21, 'gallery-image-545dc29bf0a48.jpg', 6),
(585, 21, 'gallery-image-545dc29c87ef4.jpg', 7),
(586, 21, 'gallery-image-545dc29d245cc.jpg', 8),
(587, 21, 'gallery-image-545dc29db7735.jpg', 9),
(588, 21, 'gallery-image-545dc29e51970.jpg', 10),
(589, 21, 'gallery-image-545dc29ee3794.jpg', 11),
(590, 21, 'gallery-image-545dc29f810a8.jpg', 12),
(591, 21, 'gallery-image-545dc2a01d7ca.jpg', 13),
(592, 21, 'gallery-image-545dc2a0affce.jpg', 14),
(593, 21, 'gallery-image-545dc2a14ce18.jpg', 15),
(595, 22, 'gallery-image-545f33f54f118.jpg', 1),
(596, 22, 'gallery-image-545f33f64b6d6.jpg', 2),
(597, 22, 'gallery-image-545f33f730df9.jpg', 3),
(598, 22, 'gallery-image-545f33f81be70.jpg', 4),
(599, 22, 'gallery-image-545f33f8aacaf.jpg', 5),
(600, 22, 'gallery-image-545f33f94d25c.jpg', 6),
(601, 22, 'gallery-image-545f33fa2fea7.jpg', 7),
(602, 22, 'gallery-image-545f33fadd036.jpg', 8),
(603, 22, 'gallery-image-545f33fbb60a2.jpg', 9),
(604, 22, 'gallery-image-545f33fc7f353.jpg', 10),
(605, 22, 'gallery-image-545f33fceea4c.jpg', 11),
(606, 22, 'gallery-image-545f33fd8b67a.jpg', 12),
(607, 22, 'gallery-image-545f33fe279fc.jpg', 13),
(608, 22, 'gallery-image-545f33febb624.jpg', 14),
(609, 22, 'gallery-image-545f33ff8913e.jpg', 15),
(610, 22, 'gallery-image-545f340024fed.jpg', 16),
(611, 22, 'gallery-image-545f3400ae534.jpg', 17),
(612, 22, 'gallery-image-545f340152ce8.jpg', 18),
(613, 22, 'gallery-image-545f340235e87.jpg', 19),
(614, 22, 'gallery-image-545f3402c74b9.jpg', 20),
(615, 22, 'gallery-image-545f34036c5e0.jpg', 21),
(616, 22, 'gallery-image-545f340438472.jpg', 22),
(617, 22, 'gallery-image-545f3404ddb8c.jpg', 23),
(618, 22, 'gallery-image-545f340590115.jpg', 24),
(619, 22, 'gallery-image-545f34062cf42.jpg', 25),
(620, 23, 'gallery-image-545f370a1ead8.jpg', 3),
(621, 23, 'gallery-image-545f370a618a5.jpg', 4),
(622, 23, 'gallery-image-545f370a9e28f.jpg', 5),
(625, 23, 'gallery-image-545f370b6a9be.jpg', 6),
(626, 23, 'gallery-image-545f370bacbb9.jpg', 7),
(627, 23, 'gallery-image-545f370bef688.jpg', 8),
(628, 23, 'gallery-image-545f370c3dd13.jpg', 9),
(629, 23, 'gallery-image-545f370c8175d.jpg', 10),
(630, 23, 'gallery-image-545f370cc1896.jpg', 1),
(631, 23, 'gallery-image-545f370d0f29f.jpg', 11),
(632, 23, 'gallery-image-545f370d4e039.jpg', 12),
(633, 23, 'gallery-image-545f370d8f0b7.jpg', 13),
(634, 23, 'gallery-image-545f370dcf482.jpg', 14),
(635, 23, 'gallery-image-545f370e1bc76.jpg', 16),
(636, 23, 'gallery-image-545f370e5a31f.jpg', 15),
(637, 23, 'gallery-image-545f370e99e6d.jpg', 17),
(638, 23, 'gallery-image-545f370edb574.jpg', 18),
(639, 23, 'gallery-image-545f370f2a640.jpg', 2),
(640, 23, 'gallery-image-545f370f7a3eb.jpg', 19),
(641, 23, 'gallery-image-545f370fe17da.jpg', 20),
(642, 24, 'gallery-image-545f3cf340492.jpg', 1),
(643, 24, 'gallery-image-545f3cf3ce875.jpg', 2),
(644, 24, 'gallery-image-545f3cf46a65c.jpg', 3),
(645, 24, 'gallery-image-545f3cf506ab9.jpg', 4),
(646, 24, 'gallery-image-545f3cf59a0f6.jpg', 5),
(647, 24, 'gallery-image-545f3cf6379c9.jpg', 6),
(648, 24, 'gallery-image-545f3cf6cbc73.jpg', 7),
(649, 24, 'gallery-image-545f3cf76ab8b.jpg', 8),
(650, 24, 'gallery-image-545f3cf80745b.jpg', 9),
(651, 24, 'gallery-image-545f3cf898edd.jpg', 10),
(652, 24, 'gallery-image-545f3cf9358b4.jpg', 11),
(653, 24, 'gallery-image-545f3cf9ca508.jpg', 12),
(654, 24, 'gallery-image-545f3cfa64348.jpg', 13),
(655, 25, 'gallery-image-545f40768aee8.jpg', 1),
(656, 25, 'gallery-image-545f407723d23.jpg', 2),
(657, 25, 'gallery-image-545f4077b1f10.jpg', 3),
(658, 25, 'gallery-image-545f407858873.jpg', 4),
(659, 25, 'gallery-image-545f4078e64b5.jpg', 5),
(660, 25, 'gallery-image-545f407985474.jpg', 6),
(661, 25, 'gallery-image-545f407a27168.jpg', 7),
(662, 25, 'gallery-image-545f407ab142f.jpg', 8),
(663, 25, 'gallery-image-545f407b4f889.jpg', 9),
(664, 25, 'gallery-image-545f407be0e89.jpg', 10),
(665, 25, 'gallery-image-545f407c7c8d8.jpg', 11),
(666, 26, 'gallery-image-545f46fb89893.jpg', 1),
(667, 26, 'gallery-image-545f46fc2a406.jpg', 2),
(668, 26, 'gallery-image-545f46fcb7d81.jpg', 3),
(669, 26, 'gallery-image-545f46fd50fd1.jpg', 4),
(670, 26, 'gallery-image-545f46fddf7b7.jpg', 5),
(671, 26, 'gallery-image-545f46fe7e4c4.jpg', 6),
(672, 26, 'gallery-image-545f46ff24617.jpg', 7),
(673, 26, 'gallery-image-545f46ffb4c46.jpg', 8),
(674, 26, 'gallery-image-545f4700536a7.jpg', 9),
(675, 26, 'gallery-image-545f4700e51a8.jpg', 10),
(676, 26, 'gallery-image-545f470182191.jpg', 11),
(677, 26, 'gallery-image-545f47021872e.jpg', 12),
(678, 26, 'gallery-image-545f4702a710f.jpg', 13),
(679, 26, 'gallery-image-545f47033f82a.jpg', 14),
(680, 26, 'gallery-image-545f4703d32f1.jpg', 15),
(681, 27, 'gallery-image-545f4b2d50b40.jpg', 1),
(682, 27, 'gallery-image-545f4b2dec9c6.jpg', 2),
(683, 27, 'gallery-image-545f4b2e959f0.jpg', 3),
(684, 27, 'gallery-image-545f4b2f3c9c3.jpg', 4),
(685, 27, 'gallery-image-545f4b2fd31e1.jpg', 5),
(686, 27, 'gallery-image-545f4b3073125.jpg', 6),
(687, 27, 'gallery-image-545f4b310f609.jpg', 7),
(688, 27, 'gallery-image-545f4b31b3cc2.jpg', 8),
(689, 27, 'gallery-image-545f4b326fa7f.jpg', 9),
(690, 27, 'gallery-image-545f4b330c689.jpg', 10),
(691, 27, 'gallery-image-545f4b33c0e3b.jpg', 11),
(692, 27, 'gallery-image-545f4b345dd9b.jpg', 12),
(693, 27, 'gallery-image-545f4b34ec15c.jpg', 13),
(694, 27, 'gallery-image-545f4b358c60a.jpg', 14),
(695, 27, 'gallery-image-545f4b362b0ad.jpg', 15),
(696, 27, 'gallery-image-545f4b36c1e4f.jpg', 16),
(697, 27, 'gallery-image-545f4b376e9e6.jpg', 17),
(698, 28, 'gallery-image-545f56671892c.jpg', 1),
(699, 28, 'gallery-image-545f56681f476.jpg', 2),
(700, 28, 'gallery-image-545f5668b169f.jpg', 3),
(701, 28, 'gallery-image-545f56695634d.jpg', 4),
(702, 28, 'gallery-image-545f5669eb423.jpg', 5),
(703, 28, 'gallery-image-545f566a8c117.jpg', 6),
(704, 28, 'gallery-image-545f566b27093.jpg', 7),
(705, 28, 'gallery-image-545f566bb9a97.jpg', 8),
(706, 28, 'gallery-image-545f566c5979e.jpg', 9),
(707, 28, 'gallery-image-545f566ce7bdb.jpg', 10),
(708, 28, 'gallery-image-545f566d8269f.jpg', 11),
(709, 28, 'gallery-image-545f566e216d1.jpg', 12),
(710, 28, 'gallery-image-545f566eafd57.jpg', 13),
(711, 28, 'gallery-image-545f566f59281.jpg', 14),
(712, 28, 'gallery-image-545f566ff224b.jpg', 15),
(713, 28, 'gallery-image-545f567092d04.jpg', 16),
(714, 28, 'gallery-image-545f5671541ab.jpg', 17),
(715, 28, 'gallery-image-545f56721015e.jpg', 18),
(716, 28, 'gallery-image-545f5672a7e94.jpg', 19),
(717, 28, 'gallery-image-545f56733de1f.jpg', 20),
(718, 28, 'gallery-image-545f5673cc466.jpg', 21),
(719, 28, 'gallery-image-545f5674645db.jpg', 22),
(720, 28, 'gallery-image-545f56750a115.jpg', 23),
(721, 28, 'gallery-image-545f5675940c8.jpg', 24),
(722, 28, 'gallery-image-545f5676364ca.jpg', 25),
(723, 28, 'gallery-image-545f5676d0bf7.jpg', 26),
(724, 28, 'gallery-image-545f567789edc.jpg', 27),
(725, 28, 'gallery-image-545f56783e349.jpg', 28),
(726, 28, 'gallery-image-545f5678ddd59.jpg', 29),
(727, 28, 'gallery-image-545f5679800c5.jpg', 30),
(728, 28, 'gallery-image-545f567a21315.jpg', 31),
(729, 28, 'gallery-image-545f567ab5977.jpg', 32),
(730, 28, 'gallery-image-545f567b53440.jpg', 33),
(731, 28, 'gallery-image-545f567bf0cd3.jpg', 34),
(732, 28, 'gallery-image-545f567c8ad09.jpg', 35),
(733, 28, 'gallery-image-545f567d28afa.jpg', 36),
(734, 29, 'gallery-image-545f5a200e562.jpg', 1),
(735, 29, 'gallery-image-545f5a20ba7e9.jpg', 2),
(736, 29, 'gallery-image-545f5a21529d5.jpg', 3),
(737, 29, 'gallery-image-545f5a21f1028.jpg', 4),
(738, 29, 'gallery-image-545f5a22b1f5f.jpg', 5),
(739, 29, 'gallery-image-545f5a23962ae.jpg', 6),
(740, 29, 'gallery-image-545f5a2433dc8.jpg', 7),
(741, 29, 'gallery-image-545f5a24be988.jpg', 8),
(742, 29, 'gallery-image-545f5a25569a6.jpg', 9),
(743, 29, 'gallery-image-545f5a26006dd.jpg', 10),
(744, 29, 'gallery-image-545f5a2688894.jpg', 11),
(745, 29, 'gallery-image-545f5a272327c.jpg', 12),
(746, 29, 'gallery-image-545f5a27aeeb2.jpg', 13),
(747, 29, 'gallery-image-545f5a2844892.jpg', 14),
(748, 29, 'gallery-image-545f5a28d6674.jpg', 15),
(749, 29, 'gallery-image-545f5a29734a1.jpg', 16),
(750, 29, 'gallery-image-545f5a2a4308b.jpg', 17),
(751, 29, 'gallery-image-545f5a2b63f1e.jpg', 18),
(752, 29, 'gallery-image-545f5a2c92547.jpg', 19),
(753, 29, 'gallery-image-545f5a2dafe49.jpg', 20),
(754, 29, 'gallery-image-545f5a2e77036.jpg', 21),
(755, 29, 'gallery-image-545f5a2f12706.jpg', 22),
(756, 29, 'gallery-image-545f5a2f9bd83.jpg', 23),
(757, 29, 'gallery-image-545f5a303d554.jpg', 24),
(758, 29, 'gallery-image-545f5a30e08f7.jpg', 25),
(759, 29, 'gallery-image-545f5a3196408.jpg', 26),
(760, 29, 'gallery-image-545f5a323cd72.jpg', 27),
(761, 29, 'gallery-image-545f5a32d4e30.jpg', 28),
(762, 29, 'gallery-image-545f5a336d7f6.jpg', 29),
(763, 29, 'gallery-image-545f5a340274d.jpg', 30),
(764, 30, 'gallery-image-545feeee14d15.jpg', 1),
(765, 30, 'gallery-image-545feeeeb869f.jpg', 2),
(766, 30, 'gallery-image-545feeef5539a.jpg', 3),
(767, 30, 'gallery-image-545feeefe291a.jpg', 4),
(768, 30, 'gallery-image-545feef087eac.jpg', 5),
(769, 30, 'gallery-image-545feef12974f.jpg', 6),
(770, 30, 'gallery-image-545feef1b8f5e.jpg', 7),
(771, 30, 'gallery-image-545feef2536af.jpg', 8),
(772, 30, 'gallery-image-545feef2e00cd.jpg', 9),
(773, 30, 'gallery-image-545feef378785.jpg', 10),
(774, 30, 'gallery-image-545feef3ded0c.jpg', 11),
(775, 30, 'gallery-image-545feef476fcb.jpg', 12),
(776, 30, 'gallery-image-545feef5191f3.jpg', 13),
(777, 30, 'gallery-image-545feef5a797e.jpg', 14),
(778, 30, 'gallery-image-545feef63f243.jpg', 15),
(779, 30, 'gallery-image-545feef6d2546.jpg', 16),
(780, 30, 'gallery-image-545feef769e42.jpg', 17),
(781, 30, 'gallery-image-545feef8047d1.jpg', 18),
(782, 30, 'gallery-image-545feef8a8c44.jpg', 19),
(783, 30, 'gallery-image-545feef93d755.jpg', 20),
(784, 30, 'gallery-image-545feef9cae82.jpg', 21),
(785, 30, 'gallery-image-545feefa641ce.jpg', 22),
(786, 30, 'gallery-image-545feefb1b26b.jpg', 23),
(787, 30, 'gallery-image-545feefbe173f.jpg', 24),
(788, 30, 'gallery-image-545feefcb8c41.jpg', 25),
(789, 30, 'gallery-image-545feefd5405c.jpg', 26),
(790, 30, 'gallery-image-545feefde8266.jpg', 27),
(791, 30, 'gallery-image-545feefe7f0b1.jpg', 28),
(792, 30, 'gallery-image-545feeff36266.jpg', 29),
(793, 30, 'gallery-image-545feeffd0aa8.jpg', 30),
(794, 30, 'gallery-image-545fef006d7ce.jpg', 31),
(795, 30, 'gallery-image-545fef010b6a2.jpg', 32),
(796, 30, 'gallery-image-545fef019bbcc.jpg', 33),
(797, 30, 'gallery-image-545fef0239662.jpg', 34),
(798, 30, 'gallery-image-545fef02d0412.jpg', 35),
(799, 30, 'gallery-image-545fef036f3f8.jpg', 36),
(800, 30, 'gallery-image-545fef0417d63.jpg', 37),
(801, 30, 'gallery-image-545fef04b3e06.jpg', 38),
(802, 30, 'gallery-image-545fef0554c19.jpg', 39),
(803, 30, 'gallery-image-545fef05e9843.jpg', 40),
(804, 30, 'gallery-image-545fef0680456.jpg', 41),
(805, 31, 'gallery-image-545ff3a53e454.jpg', 1),
(806, 31, 'gallery-image-545ff3a5da123.jpg', 2),
(807, 31, 'gallery-image-545ff3a675542.jpg', 3),
(808, 31, 'gallery-image-545ff3a70c46f.jpg', 4),
(809, 31, 'gallery-image-545ff3a7a0da4.jpg', 5),
(810, 31, 'gallery-image-545ff3a816ee5.jpg', 6),
(811, 31, 'gallery-image-545ff3a8846fd.jpg', 7),
(812, 31, 'gallery-image-545ff3a932fbe.jpg', 8),
(813, 31, 'gallery-image-545ff3a9c7069.jpg', 9),
(814, 31, 'gallery-image-545ff3aa65ef4.jpg', 10),
(815, 31, 'gallery-image-545ff3ab00d04.jpg', 11),
(816, 31, 'gallery-image-545ff3ab8e46f.jpg', 12),
(817, 31, 'gallery-image-545ff3ac2b79c.jpg', 13),
(818, 31, 'gallery-image-545ff3acb9f1c.jpg', 14),
(819, 31, 'gallery-image-545ff3ad550cc.jpg', 15),
(820, 31, 'gallery-image-545ff3ade5748.jpg', 16),
(821, 31, 'gallery-image-545ff3ae84ae0.jpg', 17),
(822, 31, 'gallery-image-545ff3af1f7b4.jpg', 18),
(823, 31, 'gallery-image-545ff3afcf480.jpg', 19),
(824, 31, 'gallery-image-545ff3b070326.jpg', 20),
(825, 31, 'gallery-image-545ff3b115273.jpg', 21),
(826, 31, 'gallery-image-545ff3b1a54f6.jpg', 22),
(830, 32, 'gallery-image-546125848e5b2.jpg', 1),
(833, 32, 'gallery-image-54612a7808636.jpg', 2),
(834, 33, 'gallery-image-54612ad55940c.jpg', 1),
(835, 33, 'gallery-image-54612b064de16.jpg', 2),
(836, 34, 'gallery-image-54628fefd6618.jpg', 1),
(837, 34, 'gallery-image-54628ff094bc4.jpg', 2),
(838, 34, 'gallery-image-54629010e40c2.jpg', 3),
(839, 34, 'gallery-image-54629036e9a5d.jpg', 4),
(840, 35, 'gallery-image-5470489c81485.jpg', 1),
(841, 35, 'gallery-image-5470489d1d603.jpg', 2),
(842, 35, 'gallery-image-5470489da6c9c.jpg', 3),
(843, 35, 'gallery-image-54704e4672409.jpg', 4),
(844, 35, 'gallery-image-54704e47252a0.jpg', 5),
(845, 35, 'gallery-image-54704e47c3524.jpg', 6),
(846, 35, 'gallery-image-54704e486e53b.jpg', 7),
(847, 35, 'gallery-image-54704e4916107.jpg', 8),
(848, 35, 'gallery-image-54704e49af3bc.jpg', 9),
(849, 36, 'gallery-image-5471b170d7f4b.jpg', 1),
(850, 36, 'gallery-image-5471b1b60649b.jpg', 2),
(851, 36, 'gallery-image-5471b1b663156.jpg', 3),
(852, 36, 'gallery-image-5471b28ceee38.jpg', 4),
(853, 36, 'gallery-image-5471b28d9c68b.jpg', 5),
(854, 36, 'gallery-image-5471b28e50895.jpg', 6),
(855, 36, 'gallery-image-5471b291c2046.jpg', 7),
(856, 36, 'gallery-image-5471b292b2b26.jpg', 8),
(857, 36, 'gallery-image-5471c423c7ccf.jpg', 9),
(858, 36, 'gallery-image-5471c42465b83.jpg', 10),
(859, 36, 'gallery-image-5471c424f0d7e.jpg', 11),
(860, 37, 'gallery-image-5471ca314e861.jpg', 1),
(861, 37, 'gallery-image-5471ca31eba42.jpg', 2),
(862, 37, 'gallery-image-5471caa2569b2.jpg', 3),
(863, 37, 'gallery-image-5471caa2cab36.jpg', 4),
(864, 37, 'gallery-image-5471fc72a52cb.jpg', 5),
(865, 38, 'gallery-image-54724e4ec38da.jpg', 1),
(866, 38, 'gallery-image-54724e4f6c688.jpg', 2),
(867, 38, 'gallery-image-54724ee8b62ca.jpg', 3),
(868, 38, 'gallery-image-54724ee954375.jpg', 4),
(870, 38, 'gallery-image-54724f8d24a2c.jpg', 5),
(871, 38, 'gallery-image-54724f8db8d97.jpg', 6),
(872, 38, 'gallery-image-5472504357a6c.jpg', 7),
(873, 38, 'gallery-image-54725043f1ade.jpg', 8),
(875, 39, 'gallery-image-5474b976618b6.jpg', 1),
(876, 39, 'gallery-image-5474b976ac76d.jpg', 2),
(877, 39, 'gallery-image-5474b97700f04.jpg', 3),
(878, 39, 'gallery-image-5474b9774c09a.jpg', 4),
(879, 39, 'gallery-image-5474b9cfef548.jpg', 5),
(880, 39, 'gallery-image-5474b9d08fecc.jpg', 7),
(881, 39, 'gallery-image-5474b9d1353c0.jpg', 6),
(882, 39, 'gallery-image-5474b9d1d54d9.jpg', 8),
(883, 39, 'gallery-image-5474ba10caa93.jpg', 9),
(884, 39, 'gallery-image-5474ba6612a4b.jpg', 10),
(885, 39, 'gallery-image-5474ba66af512.jpg', 11),
(886, 39, 'gallery-image-5474ba674d6cf.jpg', 12),
(887, 39, 'gallery-image-5474ba682d25b.jpg', 13),
(888, 39, 'gallery-image-5474ba6917a4b.jpg', 14),
(889, 39, 'gallery-image-5474ba69a93f0.jpg', 15),
(890, 40, 'gallery-image-54809773df9ef.jpg', 1),
(891, 40, 'gallery-image-548097bc8c806.jpg', 2),
(892, 40, 'gallery-image-5480980985eaa.jpg', 3),
(893, 40, 'gallery-image-5480983eb8f81.jpg', 4),
(894, 41, 'gallery-image-5482ae00bda50.jpg', 1),
(895, 41, 'gallery-image-5482ae31d96ff.jpg', 2),
(896, 41, 'gallery-image-5482ae32804c5.jpg', 3),
(897, 41, 'gallery-image-5482ae8b48170.jpg', 4),
(898, 41, 'gallery-image-5482aeada328b.jpg', 5),
(899, 42, 'gallery-image-5483f725860db.jpg', 1),
(900, 42, 'gallery-image-5483f7262f033.jpg', 2),
(901, 42, 'gallery-image-5483f726bf6fc.jpg', 3),
(902, 42, 'gallery-image-5483f7275c181.jpg', 4),
(903, 42, 'gallery-image-5483f727ed300.jpg', 5),
(904, 42, 'gallery-image-5483f72889590.jpg', 6),
(905, 42, 'gallery-image-5483f7292517d.jpg', 7),
(906, 42, 'gallery-image-5483f729b6ca0.jpg', 8),
(907, 42, 'gallery-image-5483f72a533e7.jpg', 9),
(908, 42, 'gallery-image-5483f72ae1308.jpg', 10),
(909, 42, 'gallery-image-5483f72b7e37f.jpg', 11),
(910, 43, 'gallery-image-5484267bc1408.jpg', 1),
(911, 43, 'gallery-image-5484267c67fc1.jpg', 4),
(912, 43, 'gallery-image-5484267d17136.jpg', 5),
(913, 43, 'gallery-image-548426d23f544.jpg', 3),
(914, 43, 'gallery-image-54842779a6d87.jpg', 2),
(915, 43, 'gallery-image-548427a37deb9.jpg', 6),
(916, 43, 'gallery-image-548427f3aaf7a.jpg', 7),
(917, 43, 'gallery-image-54842819a61ee.jpg', 8),
(918, 43, 'gallery-image-548428334e3f5.jpg', 9),
(919, 43, 'gallery-image-54842846b8226.jpg', 10),
(920, 43, 'gallery-image-548428904a6d9.jpg', 11),
(921, 43, 'gallery-image-548428c76ebc7.jpg', 12),
(922, 43, 'gallery-image-548428d1e1182.jpg', 13),
(923, 44, 'gallery-image-549698c0b0f8c.jpg', 1),
(924, 44, 'gallery-image-549698c16c9d7.jpg', 2),
(925, 44, 'gallery-image-549698c209a20.jpg', 3),
(926, 44, 'gallery-image-549698c299952.jpg', 4),
(927, 44, 'gallery-image-549698c331ef9.jpg', 5),
(928, 44, 'gallery-image-549698c3bf4e1.jpg', 6),
(929, 44, 'gallery-image-549698c459683.jpg', 7),
(930, 44, 'gallery-image-549698c4e7f7d.jpg', 8),
(931, 44, 'gallery-image-549698c57cf99.jpg', 9),
(932, 44, 'gallery-image-549698c61296a.jpg', 10),
(933, 44, 'gallery-image-549698c69b25c.jpg', 11),
(934, 44, 'gallery-image-549698c7360ea.jpg', 12),
(935, 44, 'gallery-image-549698c7bf493.jpg', 13),
(936, 44, 'gallery-image-549698c850097.jpg', 14),
(937, 44, 'gallery-image-549698c8d7881.jpg', 15),
(938, 44, 'gallery-image-549698c96e644.jpg', 16),
(939, 44, 'gallery-image-549698ca03344.jpg', 17);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `karnet_zamowienie`
--

CREATE TABLE `karnet_zamowienie` (
  `id` int(11) NOT NULL,
  `karnet_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ankieta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_item`
--

CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `routeParameters` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `onclick` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `menu_item`
--

INSERT INTO `menu_item` (`id`, `parent_id`, `location`, `type`, `title`, `route`, `routeParameters`, `url`, `onclick`, `arrangement`) VALUES
(2, NULL, 'menu_top', 'route', 'Firma', 'lscms_pages_show', '{"slug":"firma"}', NULL, NULL, 2),
(5, NULL, 'menu_bottom', 'route', 'Strona główna', 'lscms_homepage', NULL, NULL, NULL, 1),
(6, NULL, 'menu_bottom', 'route', 'Aktualności', 'lscms_news', NULL, NULL, NULL, 2),
(7, NULL, 'menu_bottom', 'route', 'O mnie', 'lscms_pages_show', '{"slug":"o-nas"}', NULL, NULL, 3),
(10, NULL, 'menu_top', 'route', 'Projekty', 'lscms_projekty', NULL, 'tesst', NULL, 5),
(11, NULL, 'menu_bottom', 'route', 'Moja działalność', 'lscms_dietetyk', NULL, NULL, NULL, 4),
(12, NULL, 'menu_top', 'route', 'Kontakt', 'lscms_kontakt', NULL, NULL, NULL, 9),
(13, NULL, 'menu_bottom', 'route', 'Kontakt', 'lscms_kontakt', NULL, NULL, NULL, 5),
(16, NULL, 'menu_top', 'route', 'Ceny', 'lscms_pages_show', '{"slug":"ceny"}', NULL, NULL, 4),
(17, NULL, 'menu_top', 'route', 'Strona Główna', 'lscms_homepage', NULL, NULL, NULL, 1),
(19, NULL, 'menu_top', 'route', 'Szkolenia', 'lscms_trainings', '{"slug":"o-mnie"}', NULL, NULL, 7),
(23, NULL, 'menu_top', 'route', 'Usługi', 'lscms_services', NULL, NULL, NULL, 3),
(24, NULL, 'menu_top', 'route', 'Promocje', 'lscms_promo', NULL, NULL, NULL, 8),
(25, NULL, 'menu_top', 'route', 'Współpraca', 'lscms_partners', '{"slug":"wspolpraca"}', NULL, NULL, 6);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `news`
--

INSERT INTO `news` (`id`, `gallery_id`, `title`, `slug`, `content_short_generate`, `content_short`, `content`, `photo`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`) VALUES
(14, 12, 'Nowa siedziba firmy', 'nowa-siedziba-firmy', 0, 'Od dnia 01 września 2014 roku nasza firma zmieniła siedzibę z ulicy Hoffmanowej 19 na ul. Lwowsk', '<p>\r\n	Od dnia 01 września 2014 roku nasza firma zmieniła siedzibę z ulicy Hoffmanowej 19 na ul. Lwowską 112.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nowe miejsce to przede wszystkim o wiele wyższy standard, to r&oacute;wnież większa powierzchnia i teren całkowicie ogrodzony. Monitoring i system alarmowy pozwolą Państwu czuć komfort po zostawieniu u nas swojego pojazdu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nowe miejsce to także większe możliwości rozwoju naszej firmy. Dwa stanowiska przygotowania samochod&oacute;w do lakierowania, wydzielone pomieszczenie do lakierowania oraz dwa stanowiska do pielęgnacji samoch&oacute;d pozwolą nam szybko i sprawnie zająć się Państwa samochodami. Zapraszamy serdecznie do naszej firmy w nowej lokalizacji przy ul. Lwowskiej 112 (p&oacute;ł kilometra przed Auchan po prawej stronie)</p>', 'news-image-5459277f1ef82.jpeg', 0, 'Nowa siedziba firmy', 'Nowa, siedziba, firmy, dnia, września, 2014, roku, nasza, firma, zmieniła', 'Od dnia 01 września 2014 roku nasza firma zmieniła siedzibę z ulicy Hoffmanowej 19 na ul. Lwowsk', '2014-08-21 21:45:18', '2014-11-04 20:53:15', '2014-09-01 00:00:00'),
(15, 14, 'ONYXX wśród światowej elity', 'onyxx-wsrod-swiatowej-elity', 0, 'ONYXX dołączył do najlepszych na świecie firm oferujących jako zabezpieczenie lakieru, japońskie powłoki ochronne marki Modesta.', '<p>\r\n	<span style="font-size:12px;">Modesta to w tym momencie światowy TOP jeśli chodzi o ceramiczne powłoki do zabezpieczenia lakieru. Produkt&oacute;w Modesta używają najlepsi detailerzy i salony na świecie.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	Miło nam poinformować, iż nasza firma dołączyła do tego grona. W naszym Auto SPA możecie Państwo zabezpieczyć sw&oacute;j lakier za pomocą całej gamy produkt&oacute;w Modesta. Więcej informacji w dziale Usługi.</p>\r\n<p class="wyroznienie">\r\n	<a href="http://www.modestaeurope.eu/index.php?lan=pl">MODESTA POLSKA</a></p>', 'news-image-545be0c94b4bd.png', 0, 'ONYXX wśród światowej elity', 'ONYXX, wśród, światowej, elity', NULL, '2014-11-06 21:57:44', '2014-11-06 22:18:19', '2014-11-06 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `page`
--

INSERT INTO `page` (`id`, `gallery_id`, `title`, `slug`, `photo`, `content_short_generate`, `content_short`, `content`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`) VALUES
(1, 4, 'Firma', 'firma', NULL, 0, NULL, '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<img alt="" src="/bundles/lscms/kcfinder/upload/images/pion%20onyx.png" style="width: 262px; height: 350px; margin-left: 100px; margin-right: 100px;" /></p>\r\n<p>\r\n	<span style="font-size:14px;">Obecność na lokalnym rynku motoryzacyjnym od 2007 roku, pozwoliła nam nabyć szerokie doświadczenie na wielu płaszczyznach. Zaczynając od sprzedaży samochod&oacute;w używanych, poprzez obsługę powypadkową Klient&oacute;w i pomoc w odzyskiwaniu odszkodowań, pełną kosmetykę samochodową, kończąc na blacharstwie, lakiernictwie i naprawach powypadkowych. Nasz zesp&oacute;ł tworzą ludzie młodzi, kt&oacute;rzy wkładają w naszą markę swoją pasję i wieloletnie doświadczenie. Przez lata wsp&oacute;łpracy nauczyliśmy się słuchać Klient&oacute;w i rozpoznawać ich potrzeby. Dzięki temu możemy bardzo elastycznie dobierać najlepsze dla Państwa rozwiązania. Nie traktujemy naszych Klient&oacute;w szablonowo - indywidualne podejście pozwala na wykonanie każdego zlecenia jak najlepiej. </span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class="title">\r\n	<span style="font-size:18px;">Zaangażowanie</span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">W codziennej pracy cechuje nas przede wszystkim entuzjazm i dbałość o detale. Samochody z naszego studia czy lakierni wyjeżdżają dopracowane w każdym szczeg&oacute;le, bo każdy samoch&oacute;d traktujemy w ten sam spos&oacute;b. Nie istotna jest marka samochodu, status majątkowy właściciela, rodzaj i skala wykonywanej usługi - w swoją pracę wkładamy serce i pełne zaangażowanie. Bardzo satysfakcjonuje nas gdy odbierający samoch&oacute;d Klient przeciera oczy ze zdumienia i okazuje wdzięczność. To sprawia że widzimy w naszej pracy sens i stale dążymy do osiągnięcia perfekcyjnego poziomu w tym co robimy.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class="title">\r\n	<span style="font-size:18px;">Za długo i za drogo?</span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Aby efekt spełnił oczekiwania a każdy detal został dopracowany, wykonywana przez nas praca wymaga czasu, co za tym idzie większych nakład&oacute;w finansowych. To tak jakby por&oacute;wnać hamburgera za 5zł z przydrożnej budy, kt&oacute;rego dostajemy po 3 minutach czekania a klkukrotnie droższego, świeżego burgera stworzonego od podstaw w stylu &quot;slow food&quot;. Oba mają mięso, dodatki i sosy, r&oacute;żni ich jednak skala satysfakcji po zjedzeniu. Tak samo jest z Auto Detailingiem - jak grzyby po deszczu otwierają się nowe firmy, często przekształcone z myjni i garażowego prania tapicerki. Oferują swoje &quot;nowoczesne usługi&quot; za śmiesznie niskie stawki, czas wykonania nawet tych najbardziej skomplikowanych często nie przekracza jednego dnia. Brak doświadczenia i umiejętności, słabe zaplecze techniczne i złe warunki lokalowe często prowadzą do poważnych błęd&oacute;w. To dlatego tak często przyjeżdżają do nas Klienci niezadowoleni z usług innej firmy. Niestety czasem jest za p&oacute;źno - popełnione przez &quot;fachowc&oacute;w&quot; błędy często są bardzo kosztowne w naprawie. W szerszej perspektywie wyb&oacute;r usługi droższej, wykonanej przez firmę o odpowiedniej reputacji, naprawdę się opłaca. Kto nadmiernie oszczędza - płaci podw&oacute;jnie.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class="title">\r\n	<span style="font-size:18px;">Zaplecze</span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Tylko najnowocześniejszy sprzęt i użyte w procesie pielęgnacji lub renowacji produkty, pozwalają wykonać prace na najwyższym poziomie. To dlatego z roku na rok, poszerzamy sw&oacute;j park maszyn o nowy sprzęt, inwestujemy w innowacyjne rozwiązania i korzystamy z najnowszych preparat&oacute;w i kosmetyk&oacute;w dostępnych na rynku. Dzięki temu możemy coraz częściej skr&oacute;cić czas wykonania naprawy lub pielęgnacji, zachowując przy tym tą samą, najwyższą jakość. Korzystamy w naszej pracy ze sprzętu i akcesori&oacute;w wiodących na świecie producent&oacute;w. W naszym słowniku nie istnieje słowo &quot;p&oacute;łśrodek&quot; - marnej jakości produkty zostawiamy amatorom.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class="title">\r\n	<span style="font-size:18px;">Zabezpieczenie Twoich pieniędzy</span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Idee działania naszej firmy możemy opisać posiłkując się znaną i popularną maksymą - &quot;lepiej zapobiegać niż leczyć&quot; - tak prawdziwą nie tylko w dziedzinie medycyny. Nierzadko opiekujemy się samochodami, kt&oacute;rych wartość przekracza cenę mieszkania, a dla właściciela nie są tylko narzędziem pracy. Często to pasja i spełnienie dziecięcych marzeń. Regularna pielęgnacja, opieka od samego początku, pozwolą zachować samoch&oacute;d w nienagannym stanie. Klient może wtedy spać spokojnie, że przez nieodpowiednią pielęgnację jego pojazd nie straci na wartości. To także ogromny plus przy dalszej odsprzedaży pojazdu - zadbany egzemplarz będzie bardzo atrakcyjny dla przyszłych kupujących i nie będzie problemu z jego zbyciem.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>', 0, 'Firma', 'O nas', NULL, '2014-04-02 12:37:09', '2014-11-29 19:44:31'),
(5, NULL, 'Ceny', 'ceny', NULL, 0, NULL, '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="cennik-wnetrze"><img alt="" src="/bundles/lscms/kcfinder/upload/images/CENNIKI%20Wn%C4%99trze.jpg" style="width: 200px; height: 200px;" /></a>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; <a href="cennik-zewnatrz"><img alt="" src="/bundles/lscms/kcfinder/upload/images/CENNIKI%20Zewn%C4%85trz.jpg" style="width: 200px; height: 200px;" /></a></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="lakierowanie"><img alt="" src="/bundles/lscms/kcfinder/upload/images/CENNIKI%20Lakierowanie.jpg" style="width: 200px; height: 200px;" /></a>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; <a href="blacharstwo"><img alt="" src="/bundles/lscms/kcfinder/upload/images/CENNIKI%20Blacharstwo.jpg" style="width: 200px; height: 200px;" /></a></p>', 0, 'Cenniki', 'ceny, cennik', NULL, '2014-11-27 20:36:05', '2014-11-30 11:14:36'),
(6, NULL, 'Interior', 'cennik-wnetrze', NULL, 0, 'Cenniki usług pielęgnacji wnętrza', '<p class="wyroznienie">\r\n	<span style="font-size:26px;"><a href="/bundles/lscms/kcfinder/upload/files/CENNIK%20WN%C4%98TRZE%20Us%C5%82ugi%20pojedyczne1.jpg"><span style="color:#ffd700;">USŁUGI POJEDYNCZE</span></a></span></p>\r\n<p class="wyroznienie">\r\n	<a href="/bundles/lscms/kcfinder/upload/files/CENNIK%20WN%C4%98TRZE%20Pakiety.jpg"><span style="color:#ffd700;"><span style="font-size: 26px;">PAKIETY</span></span></a></p>', 0, 'Interior', 'CENNIK, Usługi, pielęgnacji, wnętrza', NULL, '2014-11-29 08:51:18', '2014-11-30 16:26:59'),
(7, NULL, 'Exterior', 'cennik-zewnatrz', NULL, 0, 'Cennik usług zewnątrz', NULL, 0, 'Exterior', 'Exterior', NULL, '2014-11-30 10:43:11', '2014-11-30 10:46:25'),
(8, NULL, 'Lakierowanie', 'lakierowanie', NULL, 0, '', NULL, 0, 'Lakierowanie', 'Lakierowanie', '', '2014-11-30 11:00:12', NULL),
(9, NULL, 'Blacharstwo', 'blacharstwo', NULL, 0, NULL, NULL, 0, 'Blacharstwo', 'Blacharstwo', NULL, '2014-11-30 11:14:01', '2014-11-30 11:14:06');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8,
  `photo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 NOT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `partners`
--

INSERT INTO `partners` (`id`, `name`, `description`, `photo`, `url`, `arrangement`) VALUES
(1, 'Lemonade', 'Lorem Ipsum dolore sit amentLorem Ipsum dolore sit amentLorem Ipsum dolore sit ament', 'partners-image-54a1c9c9a53a7.png', '#test', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_main` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `dietetyk` tinyint(1) DEFAULT NULL,
  `arrangement` int(11) NOT NULL,
  `photo_list` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo_main` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `person`
--

INSERT INTO `person` (`id`, `firstname`, `lastname`, `content_main`, `content`, `dietetyk`, `arrangement`, `photo_list`, `photo_main`) VALUES
(1, 'Katarzyna', 'Makarska', '<p>\r\n	<strong>Lek.med.</strong>&nbsp;- Lekarz konsultant. Absolwentka dietetyki na Uniwersytecie Medycznym w Białymstoku. Na codzień pracuję w Radzyńskim szpitalu. Pacjenci wypowiadają się o niej ciepło i serdecznie.</p>', '<p>\r\n	<strong><span class="green">Lek.med. Katarzyna Makarska - Lekarz konsultant. Absolwentka dietetyki na Uniwersytecie Medycznym w Białymstoku.</span></strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="Standard">\r\n	<span lang="DE">Na codzień pracuję w Radzyńskim szpitalu. Jak m&oacute;wią jej pacjenci &quot; Pani Kasia jest ciepłą i serdeczną osobą, kt&oacute;ra wkłada serce w to co robi &quot;<o:p></o:p></span></p>\r\n<p class="Standard">\r\n	<span lang="DE">&nbsp;</span></p>\r\n<p class="Standard">\r\n	<span lang="DE">W centrum zajmuje się konsultacją Naszych Pacjęt&oacute;w tak aby diety oraz ćwiczenia były zawsze dobrane w spos&oacute;b bezpieczny.<o:p></o:p></span></p>', 1, 2, 'person-5374b62bc00a7.jpeg', 'person-539e0c236f319.jpeg'),
(2, 'Marcin', 'Makarski', NULL, '<p>\r\n	<span class="green"><strong>Odpowiedzialny za motywację. Przez rok odmienił swoje życie i schudł 117 kg.</strong></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="Standard" style="line-height:13.5pt">\r\n	<span lang="DE">Zna proces odchudzania można powiedzieć od kuchni. W centrum wspiera osoby kt&oacute;re najczęściej są na początku swojej drogi w odchudzaniu.<o:p></o:p></span></p>', 0, 4, 'person-5374b0d3b4e56.jpeg', NULL),
(4, 'Agnieszka', 'Włoszek', NULL, '<p class="Standard">\r\n	<strong><span class="green"><span lang="DE">Instruktor Kulturystyki, absolwentka Akademii Wychowania Fizycznego w Białej Podlaskiej. </span></span></strong></p>\r\n<p class="Standard">\r\n	&nbsp;</p>\r\n<p class="Standard">\r\n	<span lang="DE">Nieustanie poszerza swoją wiedze na szkoleniach. W centrum zajmuje się układaniem trening&oacute;w w taki spos&oacute;b aby były one możliwe do zralizowania nawet dla takich os&oacute;b kt&oacute;re nigdy nie uprawiały żadnych sport&oacute;w. &nbsp;&nbsp;<o:p></o:p></span></p>', 0, 3, 'person-5374b27f814f4.jpeg', 'person-5374b27f916c4.jpeg'),
(5, 'Katarzyna', 'Witczak', '<p>\r\n	<strong>Absolwentka dietetyki na Uniwersytecie Medycznym w Białymstoku.</strong> Studia magisterskie ukończyła z wyr&oacute;żnieniem. Obecnie jest pracownikiem naukowo-dydaktycznym Uniwersytetu w Białymstoku.</p>', '<p>\r\n	<strong><span class="green">Absolwentka dietetyki na Uniwersytecie Medycznym w Białymstoku.</span></strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Studia magisterskie ukończyła z wyr&oacute;żnieniem. Obecnie jest pracownikiem naukowo-dydaktycznym Uniwersytetu w Białymstoku. Posiada zaawansowaną wiedzę i umiejętności w zakresie żywienia człowieka zdrowego i chorego oraz profilaktyki chor&oacute;b dietozależnych.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Jak sama m&oacute;wi najważniejszym celem jej pracy z pacjentem jest ukształtowanie prawidłowych nawyk&oacute;w zywieniowych i modyfikacja jego stylu życia. Swoją wiedzę stale uaktualnia uczestnicząc w konferencjach naukowych i szkoleniach. W pracy kieruje się zasadą - <strong>Odchudzanie powinno być ratunkiem a nie udręką!</strong></p>', 1, 1, 'person-5374b6d0f3e95.jpeg', 'person-5374b6d10f521.jpeg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `person_wizyta`
--

CREATE TABLE `person_wizyta` (
  `id` int(11) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `termin` datetime DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `person_wizyta`
--

INSERT INTO `person_wizyta` (`id`, `person_id`, `firstname`, `lastname`, `phone`, `email`, `termin`, `content`) VALUES
(1, 5, 'Natalia', 'Lipska', '606 816 852', 'natalia.lipska@poczta.fm', '2014-07-17 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `porada`
--

CREATE TABLE `porada` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `page_type` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_short` longtext COLLATE utf8_unicode_ci,
  `content` longtext COLLATE utf8_unicode_ci,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `porada`
--

INSERT INTO `porada` (`id`, `gallery_id`, `category_id`, `project_id`, `page_type`, `title`, `slug`, `content_short`, `content`, `photo`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`) VALUES
(20, NULL, 6, NULL, 0, 'Konferencja - Prezentacja oferty inwestycyjnej Miasta Kraśnik', 'konferencja-prezentacja-oferty-inwestycyjnej-miasta-krasnik', '<p>\r\n	<span style="color: rgb(20, 24, 35); font-family: \'lucida grande\', tahoma, verdana, arial, sans-serif; font-size: 12.8000001907349px; line-height: 18px; text-align: center; background-color: rgb(255, 255, 255);">3 kwietnia 2014 r. uczestniczyłem w imieniu Pana Andrzeja Maja - Starosty Kraśnickiego w konferencji poświęconej prezentacji oferty inwestycyjnej Miasta Kraśnik, kt&oacute;ra odbyła się w Centrum Kultury i Promocji w Kraśniku.</span></p>', '<p>\r\n	W dniu 3 kwietnia 2014 r. &nbsp;uczestniczyłem w imieniu Pana Andrzeja Maja - Starosty Kraśnickiego w konferencji poświęconej prezentacji oferty inwestycyjnej Miasta Kraśnik, kt&oacute;ra odbyła się w Centrum Kultury i Promocji w Kraśniku.&nbsp;</p>\r\n<p>\r\n	Tematem przewodnim spotkania była prezentacja oferty inwestycyjnej Kraśnika. Przewodniczący komisji rozwoju gospodarczego w Radzie Powiatu Kraśnickiego - Piotr Janczarek - zwr&oacute;cił szczeg&oacute;lną uwagę na potrzebę &nbsp;wspierania lokalnych przedsiębiorc&oacute;w. &nbsp;W nadrzędnych tematach gospodarczych należy łączyć siły, gdyż wiele działań będzie realizowanych na etapie Starostwa Powiatowego.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>', 'porada-image-540736a8e5e11.jpeg', 0, 'Konferencja - Prezentacja oferty inwestycyjnej Miasta Kraśnik', 'Konferencja, Prezentacja, oferty, inwestycyjnej, Miasta, Kraśnik, Tematem, przewodnim, spotkania, była', 'Tematem przewodnim spotkania była prezentacja oferty inwestycyjnej Kraśnika.', '2014-09-03 17:41:28', '2014-09-03 17:46:31', '2014-04-03 00:00:00'),
(21, NULL, 6, NULL, 0, 'Konferencja Naturalna i Bezpieczna Żywność', 'konferencja-naturalna-i-bezpieczna-zywnosc', '<p>\r\n	21 marca 2014 r. w Zielonym Dworku w Kraśniku odbyła się konferencja pod nazwą &quot;Naturalna i Bezpieczna Żywność - Europejska Marka Regionu Lubelskiego&quot; zorganizowana przez Uniwersytet Medyczny w Lublinie, Centrum Wdrożeń i Innowacji Uniwersytetu Medycznego oraz Starostwo Powiatowe w Kraśniku.</p>', '<p>\r\n	21 marca 2014 r. w Zielonym Dworku w Kraśniku odbyła się konferencja pod nazwą &quot;Naturalna i Bezpieczna Żywność - Europejska Marka Regionu Lubelskiego&quot; zorganizowana przez Uniwersytet Medyczny w Lublinie, Centrum Wdrożeń i Innowacji Uniwersytetu Medycznego oraz Starostwo Powiatowe w Kraśniku.</p>\r\n<p>\r\n	Miałem przyjemność i satysfakcję uczestniczyć w konferencji w imieniu Pana Krzysztofa Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego.</p>\r\n<p>\r\n	To świetna inicjatywa, wpisująca się doskonale w nowy kierunek rozwoju wojew&oacute;dztwa lubelskiego - oparty na nowoczesnej BIOGOSPODARCE.</p>\r\n<p>\r\n	BIOGOSPODARKA - to sektory, branże i przemysły związane z produkcją mebli, nawoz&oacute;w sztucznych, papieru, energii odnawialnej, zi&oacute;ł, bio-farmaceutyk&oacute;w, bio-produkt&oacute;w, środk&oacute;w ochrony roślin, bio-kosmetyk&oacute;w, paszy dla zwierząt, zagospodarowywaniem odpad&oacute;w i oczywiście - WYSOKIEJ JAKOŚCI, ZDROWEJ ŻYWNOŚCI, ŻYWNOŚCI FUNKCJONALNEJ.<br />\r\n	A nasz region ma olbrzymie zasoby naturalne, ludzkie i tradycje w tym obszarze gospodarczym. Na razie w niewielkim stopniu wykorzystane w nowoczesnej i innowacyjnej formie.</p>\r\n<p>\r\n	Działy bioekonomii - już teraz w woje&oacute;wdztwie lubelskim osiągają roczne przychody rzędu 12 mld zł (w tym ok. 3,5 mld zł w sektorze rolnym, leśnym i rybackim). 1 euro zainwestowane w nowoczesne sektory BIOGOSPODARKI wsparte badaniami i wdrożeniami naukowymi może wygenerować 10 euro wartości dodanej w sektorach biogospodarki.<br />\r\n	A w naszym regionie mamy bardzo wyrażną specjalizację naukową sektora B+R w zakresie nauk związanych z biogospodarką. Są możliwości zaangażowania około 60% potencjału naukowego regionu w proces budowy nowoczesnych sektor&oacute;w biogospodarki w regionie.</p>', 'porada-image-54073ce2e4e40.jpeg', 0, 'Konferencja Naturalna i Bezpieczna Żywność', 'Konferencja, Naturalna, Bezpieczna, Żywność, marca, 2014', '21 marca 2014 r.', '2014-09-03 18:08:02', '2014-09-03 18:49:23', '2014-03-21 00:00:00'),
(22, NULL, 5, NULL, 0, 'Konferencja podsumowująca kolejny etap realizacji projektu RSZZG', 'konferencja-podsumowujaca-kolejny-etap-realizacji-projektu-rszzg', '<p>\r\n	13 grudnia 2013 r. odbyła się konferencja poświęcona podsumowaniu bardzo ważnego etapu realizacji projektu, pt. &quot;Regionalny System Zarządzania Zmianą Gospodarczą&quot;.</p>', '<p>\r\n	13 grudnia 2013 r. odbyła się konferencja poświęcona podsumowaniu bardzo ważnego etapu realizacji projektu, pt. &quot;Regionalny System Zarządzania Zmianą Gospodarczą&quot;.</p>\r\n<p>\r\n	Słowo wstępne w ramach otwarcia konferencji wygłosił Pan Sławomir Sosnowski &ndash; Wicemarszałek Wojew&oacute;dztwa Lubelskiego.</p>\r\n<p>\r\n	Po wystąpieniu Wicemarszałka miałem przyjemność i satysfakcję zaprezentować gł&oacute;wne założenia projektu i jego gł&oacute;wne rezultaty w trakcie prezentacji nt. &bdquo;Realizacja projektu RSZZG w aspekcie przygotowania wojew&oacute;dztwa lubelskiego do wdrażania program&oacute;w operacyjnych nowej perspektywy finansowej UE na lata 2014-2020&rdquo;</p>\r\n<p>\r\n	Założeniem Regionalnego Systemu Zarządzania Zmianą Gospodarczą (RSZZG) jest integracja ludzi, informacji i podmiot&oacute;w zaangażowanych w kreowanie zmian społeczno-gospodarczych w spos&oacute;b umożliwiający dążenie do takiego modelu, w kt&oacute;rym podstawowe znaczenie ma wsp&oacute;łpraca r&oacute;żnorodnych środowisk regionalnych w ramach jednego systemu zarządzania zmianą gospodarczą, a perspektywie w ramach rozbudowy Regionalnego Systemu Innowacji.</p>\r\n<p>\r\n	REGIONALNY SYSTEM ZARZĄDZANIA ZMIANĄ GOSPODARCZĄ &ndash; to nowa jakość w zarządzaniu informacją gospodarczą w regionie.</p>\r\n<p>\r\n	W &bdquo;Założeniach systemu zarządzania rozwojem Polski&rdquo; opracowanych przez Ministerstwo Rozwoju Regionalnego stwierdzono, iż:</p>\r\n<p>\r\n	&bdquo;... Do niedawna sądzono powszechnie, że właściwe określanie cel&oacute;w rozwojowych i zapewnienie możliwości ich realizacji &ndash; jest jedynie funkcją dostępnych środk&oacute;w finansowych.</p>\r\n<p>\r\n	<strong>Utrzymujące się problemy, np. z:</strong></p>\r\n<p>\r\n	- budową autostrad w Polsce,<br />\r\n	- organizacją systemu wsp&oacute;łpracy sfery naukowo-badawczej z przemysłem,<br />\r\n	- alokacji środk&oacute;w UE na poziomie regionalnym, nie pozwalające na pełne wykorzystanie szansy na rozw&oacute;j regionu&oacute;w,</p>\r\n<p>\r\n	jednoznacznie wskazują, że ważnym, obok środk&oacute;w finansowych, a w dłuższej perspektywie &ndash; o wiele bardziej istotnym czynnikiem, decydującym o rozwoju danego regionu, jest funkcjonowanie sprawnego systemu zarządzania w sektorze publicznym.</p>\r\n<p>\r\n	<strong>Przykłady państw i region&oacute;w, kt&oacute;re w ostatnich kilku dekadach osiągnęły sukcesy gospodarcze wskazują, że obok przeprowadzania:</strong></p>\r\n<p>\r\n	- analiz dotyczących wyzwań,<br />\r\n	- budowania scenariuszy rozwojowych,<br />\r\n	- umiejętności wyznaczania cel&oacute;w strategicznych w dłuższych przedziałach czasowych,</p>\r\n<p>\r\n	<strong>istotne znaczenie dla powodzenia strategii rozwoju regionu mają:</strong></p>\r\n<p>\r\n	- umiejętności klasy politycznej,<br />\r\n	- jakość funkcjonowania systemu prawnego,<br />\r\n	- kultura administracyjna zakorzeniona w instytucjach państwa, a przede wszystkim<br />\r\n	- zdolność do bieżącego uczenia się i reagowania na zmiany w otoczeniu.</p>\r\n<p>\r\n	W państwach osiągających sukcesy gospodarcze dobrze funkcjonują systemy zarządzania procesami rozwojowymi!</p>\r\n<p>\r\n	<br />\r\n	Spos&oacute;b i jakość funkcjonowania systemu zarządzania procesami rozwojowymi w sektorze publicznym w całości oraz na poszczeg&oacute;lnych szczeblach zarządzania decyduje w dużej mierze o zdolności do odpowiedniego reagowania regionu (a tak naprawdę klasy politycznej / kadry zarządzającej i instytucji publicznych) na strategiczne wyzwania pojawiające się we wsp&oacute;łczesnym świecie.</p>\r\n<p>\r\n	Wyzwania te mają r&oacute;żny charakter i wynikają z r&oacute;żnorakich czynnik&oacute;w, mechanizm&oacute;w, zjawisk, proces&oacute;w, tendencji i trend&oacute;w, kt&oacute;rych do końca nie znamy &ndash; ale ich wsp&oacute;lną cechą jest jednak to, że mają one w coraz większym stopniu charakter ponadregionalny i ponadkrajowy oraz to, że stają się wsp&oacute;lne dla wielu lub niemal wszystkich mieszkańc&oacute;w Ziemi&rdquo;.</p>\r\n<p>\r\n	<strong>Najważniejszymi czynnikami i mechanizmami zmian gospodarki i rynku pracy w wymiarze regionalnym i globalnym w najbliższych latach, a może dekadach będą takie zjawiska, procesy, tendencje i trendy, jak:</strong></p>\r\n<p>\r\n	- Szybko rosnąca ilość wiedzy i informacji<br />\r\n	- Globalizacja<br />\r\n	- Rozw&oacute;j nowoczesnych technologii<br />\r\n	- Otwarty i wymagający rynek pracy<br />\r\n	- Rosnąca sieciowość i projektowość relacji<br />\r\n	- Postępująca digitalizacja<br />\r\n	- Nowy typ społeczeństwa<br />\r\n	- Oszczędzanie energii i odnawialność jej źr&oacute;deł<br />\r\n	- Dłuższe i lepsze życie<br />\r\n	- Zmiana struktury branż i sektor&oacute;w</p>\r\n<p>\r\n	<strong>Stąd też, w ramach realizacji projektu dochodzi to osiągania takich rezultat&oacute;w jak:</strong></p>\r\n<p>\r\n	- integrowanie wiedzy gospodarczej z najważniejszych źr&oacute;deł w jednym systemie</p>\r\n<p>\r\n	- planowanie i realizowanie badań i analiz na bazie realnych potrzeb informacyjnych<br />\r\n	[badania potrzeb informacyjnych],</p>\r\n<p>\r\n	- tworzony jest jednolity system informatyczny - jako narzędzie służące gromadzeniu, przetwarzaniu i upowszechnianiu informacji gospodarczych [system informatyczny projektu]</p>\r\n<p>\r\n	- na bieżącą dokonuje się monitoringu oraz dostarczania aktualnych informacji nt. zmian zachodzących w gospodarce regionu [system wskaźnik&oacute;w syntetycznych projektu]</p>\r\n<p>\r\n	- ma miejsce wsparcie analityczne procesu monitorowania i aktualizacji Strategii Rozwoju Wojew&oacute;dztwa Lubelskiego,</p>\r\n<p>\r\n	- ma miejsce wsparcie analityczne przygotowywania programu operacyjnego dla wojew&oacute;dztwa lubelskiego w ramach nowego okresu programowania w kontekście przyszłych instrument&oacute;w wsparcia przedsiębiorczości,</p>\r\n<p>\r\n	- tworzona jest baza danych do zasilania zasobami tematycznymi tworzonego Systemu Informacji Przestrzennej Lubelszczyzny.</p>', 'porada-image-5407451a1234e.jpeg', 0, 'Konferencja podsumowująca kolejny etap realizacji projektu RSZZG', 'Konferencja, podsumowująca, kolejny, etap, realizacji, projektu, RSZZG, grudnia, 2013, odbyła', '13 grudnia 2013 r. odbyła się konferencja poświęcona podsumowaniu bardzo ważnego etapu realizacji projektu, pt. "Regionalny System Zarz', '2014-09-03 18:43:05', '2014-09-03 18:48:48', '2013-12-13 00:00:00'),
(23, NULL, 5, NULL, 0, 'VII posiedzenie Rady Programowej Projektu RSZZG', 'vii-posiedzenie-rady-programowej-projektu-rszzg', '<p>\r\n	W dniu 6 grudnia 2013 r. w siedzibie Departamentu Regionalnego Programu Operacyjnego Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie odbyło się VII posiedzenie Rady Programowej projektu systemowego Regionalny System Zarządzania Zmianą Gospodarczą</p>', '<p>\r\n	W dniu 6 grudnia 2013 r. w siedzibie Departamentu Regionalnego Programu Operacyjnego Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie odbyło się VII posiedzenie Rady Programowej projektu systemowego Regionalny System Zarządzania Zmianą Gospodarczą.</p>\r\n<p>\r\n	Miałem przyjemność otworzyć i moderować spotkanie oraz przedstawić informacje o aktualnie realizowanych zadaniach w ramach projektu RSZZG.</p>\r\n<p>\r\n	Podczas posiedzenia zostały przedstawione wyniki badania pogłębionego:</p>\r\n<p>\r\n	- &bdquo;Analiza podmiot&oacute;w oraz powiązań kooperacyjnych w sektorze rolno-spożywczym w kontekście zarządzania regionalnym łańcuchem dostaw żywności&quot; &ndash; dr Maciej Piotrowski, Quality Watch Sp. z o.o.</p>\r\n<p>\r\n	oraz wyniki badania dziedzinowego:</p>\r\n<p>\r\n	- &bdquo;Analiza możliwości rozwoju turystyki zdrowotnej w wojew&oacute;dztwie lubelskim w kontekście zidentyfikowanej inteligentnej specjalizacji regionu w dziedzinie usług medycznych i prozdrowotnych&quot; &ndash; dr Andrzej Tucki</p>', 'porada-image-54074622e88ce.jpeg', 0, 'VII posiedzenie Rady Programowej Projektu RSZZG', 'posiedzenie, Rady, Programowej, Projektu, RSZZG, dniu, grudnia, 2013', 'W dniu 6 grudnia 2013 r.', '2014-09-03 18:47:30', NULL, '2013-12-06 00:00:00'),
(24, NULL, 5, NULL, 0, 'Panel branżowy projektu RSZZG - Turystyka zdrowotna', 'panel-branzowy-projektu-rszzg-turystyka-zdrowotna', '<p>\r\n	4 listopada 2013 r. w Trybunale Koronnym w Lublinie odbył się organizowany przez Departament Gospodarki i Innowacji UMWL panel branżowy badania dziedzinowego pn. &bdquo;Analiza możliwości rozwoju turystyki zdrowotnej w wojew&oacute;dztwie lubelskim w kontekście zidentyfikowanej inteligentnej specjalizacji regionu w dziedzinie usług medycznych i prozdrowotnych&quot; projektu Regionalny System Zarządzania Zmianą Gospodarczą.</p>', '<p>\r\n	4 listopada 2013 r. w Trybunale Koronnym w Lublinie odbył się organizowany przez Departament Gospodarki i Innowacji UMWL panel branżowy badania dziedzinowego pn. &bdquo;Analiza możliwości rozwoju turystyki zdrowotnej w wojew&oacute;dztwie lubelskim w kontekście zidentyfikowanej inteligentnej specjalizacji regionu w dziedzinie usług medycznych i prozdrowotnych&quot; projektu Regionalny System Zarządzania Zmianą Gospodarczą.</p>\r\n<p>\r\n	W spotkaniu uczestniczyli przedstawiciele sektora turystyki, uczelni wyższych, instytucji otoczenia biznesu oraz administracji samorządowej.</p>\r\n<p>\r\n	Miałem przyjemność otworzyć i moderować panel. W swoim wystąpieniu podkreśliłem ważną rolę rozwoju turystyki zdrowotnej w procesie wdrażania Regionalnej Strategii Innowacji. Zidentyfikowany potencjał do rozwoju turystyki zdrowotnej był m.in. jedną z przesłanek do wskazania usług medycznych i prozdrowotnych jako jednej z inteligentnych specjalizacji wojew&oacute;dztwa lubelskiego.</p>', 'porada-image-540747731f9d4.jpeg', 0, 'Panel branżowy projektu RSZZG - Turystyka zdrowotna', 'Panel, branżowy, projektu, RSZZG, Turystyka, zdrowotna, listopada, 2013, Trybunale, Koronnym', '4 listopada 2013 r. w Trybunale Koronnym w Lublinie odbył się organizowany przez Departament Gospodarki i Innowacji UMWL panel branżowy badania dziedzinowego pn.', '2014-09-03 18:53:06', NULL, '2013-11-04 00:00:00'),
(25, NULL, 7, NULL, 0, 'Konferencja "Stypendia naukowe dla doktorantów II"', 'konferencja-stypendia-naukowe-dla-doktorantow-ii', '<p>\r\n	Projekt jest realizowany od 2010 r. w Departamencie Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego.</p>', '<p>\r\n	W dniu 11 grudnia 2013 r. odbyła się w Sali Błękitnej Lubelskiego Urzędu Wojew&oacute;dzkiego w Lublinie konferencja podsumowującą projekt &bdquo;Stypendia naukowe dla doktorant&oacute;w II&quot; w ramach Programu Operacyjnego Kapitał Ludzki.</p>\r\n<p>\r\n	Projekt jest realizowany od 2010 r. w Departamencie Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego.</p>\r\n<p>\r\n	Podczas konferencji Krzysztof Grabczuk - wicemarszałek Wojew&oacute;dztwa Lubelskiego wręczył dyplomy 94 doktorantom, kt&oacute;rzy otrzymali stypendia w ramach III i IV naboru wniosk&oacute;w odpowiednio w 2012 r. i w 2013 r.</p>\r\n<p>\r\n	Miałem przyjemność i satysfakcję zaprezentować gł&oacute;wne założenia i rezultaty projektu.</p>\r\n<p>\r\n	Następnie sześciu doktorant&oacute;w zaprezentowało prezentacje wynik&oacute;w badań naukowych swoich prac doktorskich. Cztery prezentacje doktorat&oacute;w zostały wzbogacone o wystąpienie przedsiębiorcy/jednostki z sektora B+R, z kt&oacute;rymi doktoranci nawiązali wsp&oacute;łpracę w zakresie wdrażania wynik&oacute;w badań naukowych.</p>\r\n<p>\r\n	Celem projektu &bdquo;Stypendia naukowe dla doktorant&oacute;w II&quot; jest wsparcie proces&oacute;w innowacyjnych w regionalnej gospodarce poprzez wyasygnowanie stypendi&oacute;w naukowych dla 212 doktorant&oacute;w kształcących się na kierunkach przyczyniających się do wzmocnienia konkurencyjności i rozwoju gospodarczego Lubelszczyzny, uwzględnionych w Regionalnej Strategii Innowacji Wojew&oacute;dztwa Lubelskiego.</p>', 'porada-image-54084bd47ad06.jpeg', 0, 'Konferencja "Stypendia naukowe dla doktorantów II"', 'Konferencja, "Stypendia, naukowe, doktorantów, dniu, grudnia, 2013, odbyła, się, Sali', 'W dniu 11 grudnia 2013 r. odbyła się w Sali Błękitnej Lubelskiego Urzędu Wojew', '2014-09-04 13:24:03', NULL, '2013-12-11 00:00:00'),
(26, NULL, 7, NULL, 0, 'REGIONALNE FORUM GOSPODARCZE W KRAŚNIKU - INNOWACJE I WSPÓŁPRACA', 'regionalne-forum-gospodarcze-w-krasniku-innowacje-i-wspolpraca', '<p>\r\n	Lubelskiego w Lublinie we wsp&oacute;łpracy z przedsiębiorcami z Kraśnickiej Izby Gospodarczej i pracownikami Starostwa Powiatowego w Kraśniku zorganizowali &quot;REGIONALNE FORUM GOSPODARCZE INNOWACJE I WSP&Oacute;ŁPRACA &ndash; NOWY WYMIAR ROZWOJU REGIONU&quot; - z udziałem kilkuset kraśnickich i lubelskich przedsiębiorc&oacute;w i samorządowc&oacute;w, przedstawicieli świata nauki oraz ekonomist&oacute;w.</p>', '<p>\r\n	26 listopada 2013 r. w Kraśniku pracownicy Departamentu Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie we wsp&oacute;łpracy z przedsiębiorcami z Kraśnickiej Izby Gospodarczej i pracownikami Starostwa Powiatowego w Kraśniku zorganizowali &quot;REGIONALNE FORUM GOSPODARCZE INNOWACJE I WSP&Oacute;ŁPRACA &ndash; NOWY WYMIAR ROZWOJU REGIONU&quot; - z udziałem kilkuset kraśnickich i lubelskich przedsiębiorc&oacute;w i samorządowc&oacute;w, przedstawicieli świata nauki oraz ekonomist&oacute;w.</p>\r\n<p>\r\n	Gł&oacute;wnym tematem debaty były szanse na rozw&oacute;j lokalnej przedsiębiorczości w nowej perspektywie finansowej UE na lata 2014 &ndash; 2020 w wojew&oacute;dzwie lubelskim i powiecie kraśnickim.</p>\r\n<p>\r\n	To pierwsze tego typu wydarzenie gospodarcze w historii powiatu krasnickiego i miasta Kraśnik.</p>\r\n<p>\r\n	Forum gospodarcze to doskonała platforma wymiany informacji, nawiązywania kontakt&oacute;w gospodarczych i promocji firm oraz samorząd&oacute;w. I świetnie, że z tych możliwości skorzystało kilkaset os&oacute;b przedstawicieli firm, organizacji okołobiznesowych, świata nauki i samorząd&oacute;w.</p>\r\n<p>\r\n	Z pewnością to wydarzenie powinno być kontynuowane w kolejnych latach i na stałe wpisać się w kalendarz najważniejszych wydarzeń gospodarczych w woje&oacute;wdztwie lubelskim i powiecie kraśnickim.</p>', 'porada-image-54084c8a71c74.jpeg', 0, 'REGIONALNE FORUM GOSPODARCZE W KRAŚNIKU - INNOWACJE I WSPÓŁPRACA', 'REGIONALNE, FORUM, GOSPODARCZE, KRAŚNIKU, INNOWACJE, WSPÓŁPRACA, listopada, 2013', '26 listopada 2013 r.', '2014-09-04 13:27:05', NULL, '2013-11-26 00:00:00'),
(27, NULL, 4, NULL, 0, 'Konferencja naukowa - Dziedzictwo Jana Pawła II', 'konferencja-naukowa-dziedzictwo-jana-pawla-ii', '<p>\r\n	8 kwietnia 2014 r. z olbrzymią radością i satysfakcją wziąłem udział w imieniu Marszałka Wojew&oacute;dztwa Lubelskiego - Pana Krzysztofa Hetmana w konferencji naukowej poświęconej dziedzictwu Jana Pawła II zorganizowanej przez Katolicki Uniwersytet Lubelski, Starostwo Powiatowe w Kraśniku i Powiatowe Centrum Pomocy Rodzinie pn.&quot;Dziedzictwo Jana Pawła II. Przesłanie dla Polski&quot;.</p>', '<p>\r\n	8 kwietnia 2014 r. z olbrzymią radością i satysfakcją wziąłem udział w imieniu Marszałka Wojew&oacute;dztwa Lubelskiego - Pana Krzysztofa Hetmana w konferencji naukowej poświęconej dziedzictwu Jana Pawła II zorganizowanej przez Katolicki Uniwersytet Lubelski, Starostwo Powiatowe w Kraśniku i Powiatowe Centrum Pomocy Rodzinie pn.&quot;Dziedzictwo Jana Pawła II. Przesłanie dla Polski&quot;.</p>\r\n<p>\r\n	Niezwykle bogaty program konferencji uzmysławiał wszystkim obecnym na konferencji jak wielką spuściznę pozostawił po sobie Papież Polak. Zakres temetyczny dzieł teologicznych oraz prac o wydźwięku społeczno-moralnym, jakie pozostawił Jan Paweł II, jest niezwykle szeroki. Pozwala to czerpać nauki z tego dorobku nie tylko osobom duchowym, ale przede wszystkim osobom świeckim.</p>\r\n<p>\r\n	Pontyfikat Jana Pawła II przypadł na niezwykle ważny i przełomowy okres dla Polski, Europy i całego świata. Nie spos&oacute;b nie zauważyć, że kształtował on ten okres historii w szeg&oacute;lności naszego kraju i Europy. A tematyka konferencji dowodzi, jak szerokie było spektrum zainteresowań wsp&oacute;łczesnością bł. Jana Pawła II. Żaden z istotnych problem&oacute;w, z jakimi zmagał się świat, nie uszedł jego uwadze. Z tym większą troską należy pochylać się nad tą spuścizną, odkrywając w niej coraz nowe przesłania dla przyszłości.</p>\r\n<p>\r\n	Prelegenci, moderatorzy dyskusji panelowych oraz dyskutanci starali się odkrywać przesłania i nauki Jana Pawła II dla obecnych i przyszłych pokoleń w wymiarach: wychowania, roli rodziny, demokracji, ochrony zdrowia i wielu innych.</p>\r\n<p>\r\n	Z pewnością referaty i wnioski z konferencji zaowocują nowymi odkryciami i bogatym plonem dyskusji.</p>', 'porada-image-54084dab3f507.jpeg', 0, 'Konferencja naukowa - Dziedzictwo Jana Pawła II', 'Konferencja, naukowa, Dziedzictwo, Jana, Pawła, kwietnia, 2014', '8 kwietnia 2014 r.', '2014-09-04 13:31:54', NULL, '2014-04-08 00:00:00'),
(28, NULL, 4, NULL, 0, 'Dzień Patrona w Zespole Szkół Nr 3 w Kraśniku', 'dzien-patrona-w-zespole-szkol-nr-3-w-krasniku', '<p>\r\n	4 kwietnia 2014 r. z olbrzymią przyjemnością i satysfakcją uczestniczyłem w imieniu Pana Krzysztofa Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego w uroczystościach Dnia Patrona Zespołu Szk&oacute;ł Nr 3 w Kraśniku Juliusza Słowackiego połączonych ze ślubowaniem pierwszych klas mundurowych.</p>', '<p>\r\n	4 kwietnia 2014 r. z olbrzymią przyjemnością i satysfakcją uczestniczyłem w imieniu Pana Krzysztofa Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego w uroczystościach Dnia Patrona Zespołu Szk&oacute;ł Nr 3 w Kraśniku Juliusza Słowackiego połączonych ze ślubowaniem pierwszych klas mundurowych.</p>', 'porada-image-54084ecb80f7f.jpeg', 0, 'Dzień Patrona w Zespole Szkół Nr 3 w Kraśniku', 'Dzień, Patrona, Zespole, Szkół, Kraśniku, kwietnia, 2014', '4 kwietnia 2014 r.', '2014-09-04 13:36:42', NULL, '2014-04-04 00:00:00'),
(29, NULL, 4, NULL, 0, 'Wojewódzki Turniej Wiedzy o Janie Pawle II w Kraśniku', 'wojewodzki-turniej-wiedzy-o-janie-pawle-ii-w-krasniku', '<p>\r\n	2 kwietnia 2014 r. z olbrzymią radością uczestniczyłem w imieniu Pana Krzysztofa Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego w &quot;Wojew&oacute;dzkim Turnieju Wiedzy o Janie Pawle II - Miejsce dla Każdego&quot; - zorganizowanym już po raz dziewiąty w kraśnickim gimnazjum nr 2.</p>', '<p>\r\n	2 kwietnia 2014 r. z olbrzymią radością uczestniczyłem w imieniu Pana Krzysztofa Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego w &quot;Wojew&oacute;dzkim Turnieju Wiedzy o Janie Pawle II - Miejsce dla Każdego&quot; - zorganizowanym już po raz dziewiąty w kraśnickim gimnazjum nr 2.</p>\r\n<p>\r\n	Do udziału w konkursie zgłosiło się kilkudziesięciu uczni&oacute;w gimnazj&oacute;w i lice&oacute;w m.in. z Kraśnika, Wilkołaza, Polichny i Krasnegostawu.</p>\r\n<p>\r\n	Najlepiej zaprezentowało się gimnazjum z Wilkołaza. Na drugim miejscu znalazło się Publiczne Gimnazjum nr 2 z Kraśnika, a na trzecim gimnazjaliści z Mętowa.</p>\r\n<p>\r\n	Turniejowi wiedzy towarzyszyła wystawa rzeźbiarska Szczepana Ignaczyńskiego pod nazwą &quot;Kapliczki Polskie&quot;.</p>', 'porada-image-54084f5ac27f4.jpeg', 0, 'Wojewódzki Turniej Wiedzy o Janie Pawle II w Kraśniku', 'Wojewódzki, Turniej, Wiedzy, Janie, Pawle, Kraśniku, kwietnia, 2014', '2 kwietnia 2014 r.', '2014-09-04 13:39:06', NULL, '2014-04-02 00:00:00'),
(30, NULL, 3, NULL, 0, 'Otwarcie nowoczesnego bloku operacyjnego w kraśnickim szpitalu', 'otwarcie-nowoczesnego-bloku-operacyjnego-w-krasnickim-szpitalu', '<p>\r\n	Oficjalne otwarcie nowoczesnego bloku operacyjnego odbyło się 25 marca 2014 r.</p>', '<p>\r\n	Oficjalne otwarcie nowoczesnego bloku operacyjnego odbyło się 25 marca 2014 r. W uroczystościach wzięli udział m.in. wiceminister zdrowia Aleksander Sopliński, dyrektor lubelskiego oddziału wojew&oacute;dzkiego NFZ Krzysztof Tuczapski, marszałek wojew&oacute;dztwa lubelskiego Krzysztof Hetman, poseł na sejm RP Jan Łopata, starosta kraśnicki Andrzej Maj oraz przedstawiciele władz samorządowych powiatu kraśnickiego.</p>\r\n<p>\r\n	Na początku grudnia 2012 r. dyrektor SP ZOZ w Kraśniku Marek Kos podpisał w Urzędzie Marszałkowskim umowę na realizację inwestycji kluczowej dla funkcjonowania plac&oacute;wki. Chodzi o przebudowę bloku operacyjnego, na kt&oacute;rą pozyskano unijne dofinansowanie. Całkowita wartość tego projektu to ponad 6 mln zł.</p>', 'porada-image-5408567a8b835.jpeg', 0, 'Otwarcie nowoczesnego bloku operacyjnego w kraśnickim szpitalu', 'Otwarcie, nowoczesnego, bloku, operacyjnego, kraśnickim, szpitalu, Oficjalne, otwarcie, odbyło, się', 'Oficjalne otwarcie nowoczesnego bloku operacyjnego odbyło się 25 marca br. W uroczystościach wzięli udział m.in.', '2014-09-04 14:09:29', '2014-09-04 14:16:06', '2014-03-25 00:00:00'),
(31, NULL, 3, NULL, 0, 'Nowa komisja stała przy Radzie Powiatu Kraśnickiego', 'nowa-komisja-stala-przy-radzie-powiatu-krasnickiego', '<p>\r\n	27 listopada 2013 r. na ostatniej sesji Rady Powiatu Kraśnickiego Radni powołali nową komisję - Komisję Rozwoju Gospodarczego. W jej skład weszli wszyscy radni, kt&oacute;rych kandydatury zostały zgłoszone w trakcie sesji, tj.: Piotr Janczarek, Wiesław Kołczewski, Piotr Nowaczek, Agnieszka Orzeł-Depta, Bożena Kozub, Karol Rej, Wiesław Marzec, Roman Bijak.</p>', '<p>\r\n	27 listopada 2013 r. na ostatniej sesji Rady Powiatu Kraśnickiego Radni powołali nową komisję - Komisję Rozwoju Gospodarczego. W jej skład weszli wszyscy radni, kt&oacute;rych kandydatury zostały zgłoszone w trakcie sesji, tj.: Piotr Janczarek, Wiesław Kołczewski, Piotr Nowaczek, Agnieszka Orzeł-Depta, Bożena Kozub, Karol Rej, Wiesław Marzec, Roman Bijak.</p>\r\n<p>\r\n	Zostałem wybrany na przewodniczącego Komisji. Dziękuję wszystkim Radnym za inicjatywę idącą w kierunku większego zaangażowania w sprawy gospodarcze powiatu oraz zaufanie i powierzenie mi funkcji Przewodniczącego Komisji.</p>\r\n<p>\r\n	Po 2014 roku przed samorządami lokalnymi z terenu wojew&oacute;dztwa lubelskiego otworzą się nowe możliwości i perspektywy rozwoju społeczno-gospodarczego oraz nowe wyzwania i obowiązki.</p>\r\n<p>\r\n	Będą one:</p>\r\n<p>\r\n	1) z jednej strony związane z możliwością włączenia się w realizację ambitnego planu rozwoju gospodarczego Samorządu Wojew&oacute;dztwa Lubelskiego do 2020 r. zapisanego w zaktualizowanej Strategii Rozwoju Wojew&oacute;dztwa Lubelskiego i Regionalnej Strategii Innowacji - opartego na wsparciu rozwoju przedsiębiorczości, wiedzy i nowych technologiach, energetyce niskoemisyjnej (w tym OZE), a także na restrukturyzacji i modernizacji sektora rolnego,</p>\r\n<p>\r\n	2) a z drugiej strony z uruchamianiem nowej perspektywy funduszy unijnych na lata 2014-2020, w ramach kt&oacute;rej będzie możliwość realizacji wielu ważnych projekt&oacute;w prorozwojowych dla powiatu ze wsparciem funduszy UE.</p>\r\n<p>\r\n	Samorządy lokalne: gminne i powiatowe muszą być zatem bardzo dobrze przygotowane programowo i organizacyjnie do realizacji nowych przedsięwzięć i inwestycji w formie nowych typ&oacute;w projekt&oacute;w, z kt&oacute;rych większość powinna być realizowana w partnerstwach/konsorcjach z innymi samorządami, jednostkami naukowymi, badawczo-rozwojowymi i przedsiębiorstwami.</p>\r\n<p>\r\n	Moim zdaniem Komisja Rozwoju Gospodarczego powinna skupiać się przede wszystkim nad inicjowaniem, przygotowywaniem, opiniowaniem i promowaniem spraw, temat&oacute;w, dokument&oacute;w strategicznych i uchwał gospodarczych w takich obszarach rozwoju społeczno-gospodarczego jak:</p>\r\n<p>\r\n	1. Realizacja inwestycji i projekt&oacute;w prorozwojowych:</p>\r\n<p>\r\n	- projekty mające wymiar i charakter projekt&oacute;w zintegrowanych, kluczowych, systemowych, partnerskich, konsorcjalnych, w formule PPP (projekty innowacyjne, edukacyjne i naukowe o dużym zasięgu oddziaływania, B+R, z zakresu rozwoju społeczeństwa cyfrowego, promocji gospodarczej i marketingu gospodarczego, tworzenia i rozwoju stref aktywności gospodarczej i teren&oacute;w inwestycyjnych itp.),<br />\r\n	- projekty generujące dużą wartość dodaną, nowe miejsca pracy,</p>\r\n<p>\r\n	2. Wspieranie rozwoju przedsiębiorczości lokalnej:</p>\r\n<p>\r\n	- prowadzenie przemyślanej i zorganizowanej (systemowej) polityki wobec przedsiębiorc&oacute;w, potencjalnych inwestor&oacute;w, turyst&oacute;w (organizacja starostwa powiatowego i urzęd&oacute;w gmin w oparciu o zasadę kompleksowej obsługi interesanta &ndash; klienta),<br />\r\n	- powoływanie i/lub wspieranie instytucji pobudzających inicjatywy gospodarcze, towarzystwa i izby gospodarcze, instytucje wspierania inicjatyw, agencje rozwoju lokalnego i regionalnego,<br />\r\n	- tworzenie i rozw&oacute;j stref aktywności gospodarczej,<br />\r\n	- budowa i dbałość o wysoki pozom infrastruktury technicznej (drogi, oczyszczanie ściek&oacute;w, zagospodarowanie odpad&oacute;w, kanalizacja, sieci internetowe, itp.),<br />\r\n	- tworzenie funduszy poręczeń, inkubator&oacute;w przedsiębiorczości,<br />\r\n	- wsparcie międzynarodowej ekspansji przedsiębiorstw,<br />\r\n	- zapewnienie r&oacute;żnorodnych źr&oacute;deł finansowania inwestycji,<br />\r\n	- stymulowanie wsp&oacute;łpracy branżowej,<br />\r\n	- wspieranie powstawania i rozwoju lokalnych i regionalnych klastr&oacute;w gospodarczych,<br />\r\n	- ukierunkowywanie systemu edukacyjnego na specjalizacje regionu,<br />\r\n	- inwestowanie w kapitał ludzki,<br />\r\n	- uczestnictwo w budowie struktur regionalnego systemu innowacji,<br />\r\n	- budowanie partnerstw lokalnych,<br />\r\n	- budowanie i promocja marki gospodarczej powiatu/regionu.</p>\r\n<p>\r\n	3. Pozyskiwanie/przyciąganie inwestycji z kapitałem zagranicznym:</p>\r\n<p>\r\n	- w sektorach i branżach nie stanowiących istotnej konkurencji dla podmiot&oacute;w gospodarczych obecnych w powiecie kraśnickim i regionie lubelskim.</p>\r\n<p>\r\n	4. Aktywizacja rynku pracy i instytucji edukacyjnych oraz wsp&oacute;łpraca z jednostkami B+R:</p>\r\n<p>\r\n	- ukierunkowywanie systemu edukacyjnego na rynek pracy i specjalizacje regionu,<br />\r\n	- inwestowanie w kapitał intelektualny,<br />\r\n	- podejmowanie wsp&oacute;łpracy z uczelniami wyższymi, jednostkami naukowymi<br />\r\n	i badawczo-rozwojowymi z terenu wojew&oacute;dztwa lubelskiego,<br />\r\n	- inicjowanie i realizacja projekt&oacute;w systemowych z dofinansowaniem ze środk&oacute;w EFS.</p>\r\n<p>\r\n	5. Planowanie strategiczne:</p>\r\n<p>\r\n	- wsp&oacute;łpraca przy opracowywaniu i opiniowanie projekt&oacute;w nowych dokument&oacute;w strategicznych, planistycznych i programowych na lata 2014-2020.</p>\r\n<p>\r\n	Na pierwszym merytorycznym posiedzeniu Komisji Rozwoju Gospodarczego zaplanowanym na 3 grudnia będziemy dyskutowali nad założeniami Ramowego Planu Pracy na 2014 rok.</p>', 'porada-image-5408593b2170c.jpeg', 0, 'Nowa komisja stała przy Radzie Powiatu Kraśnickiego', 'Nowa, komisja, stała, przy, Radzie, Powiatu, Kraśnickiego, listopada, 2013, ostatniej', '27 listopada 2013 r. na ostatniej sesji Rady Powiatu Kraśnickiego Radni powołali now', '2014-09-04 14:21:14', NULL, '2013-11-27 00:00:00'),
(32, NULL, 1, NULL, 0, 'Konferencja "Biogospodarka Powiatu Biłgorajskiego"', 'konferencja-biogospodarka-powiatu-bilgorajskiego', '<p>\r\n	21 października miałem olbrzymią przyjemość i satysfakcję w imieniu Pana Krzysztofa Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego gościć w powiecie biłgorajskim i wziąć udział w konferencji pt. &quot;Biogospodarka Powiatu Biłgorajskiego w nowej perspektywie finansowej 2014-2020&quot;.</p>', '<p>\r\n	21 października miałem olbrzymią przyjemość i satysfakcję w imieniu Pana Krzysztofa Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego gościć w powiecie biłgorajskim i wziąć udział w konferencji pt. &quot;Biogospodarka Powiatu Biłgorajskiego w nowej perspektywie finansowej 2014-2020&quot;.</p>\r\n<p>\r\n	&nbsp;</p>', 'porada-image-54085d598eb61.jpeg', 0, 'Konferencja "Biogospodarka Powiatu Biłgorajskiego"', 'Konferencja, "Biogospodarka, Powiatu, Biłgorajskiego", października, miałem, olbrzymi', '21 października miałem olbrzymi', '2014-09-04 14:38:48', NULL, '2013-10-21 00:00:00'),
(33, NULL, 1, NULL, 0, 'Innowacyjne Techn. w Inżynierii i Kształtowaniu Środowiska', 'innowacyjne-techn-w-inzynierii-i-ksztaltowaniu-srodowiska', '<p>\r\n	27 czerwca 2013 r. miałem przyjemność i satysfakcję uczestniczyć w imieniu Pana Krzysztof Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego na konferencji naukowo-technicznej - &quot;Innowacyjne Technologie w Inżynierii i Kształtowaniu Środowiska&quot; na Uniwersytecie Przyrodniczym w Lublinie.</p>', '<p>\r\n	27 czerwca 2013 r. miałem przyjemność i satysfakcję uczestniczyć w imieniu Pana Krzysztof Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego na konferencji naukowo-technicznej - &quot;Innowacyjne Technologie w Inżynierii i Kształtowaniu Środowiska&quot; na Uniwersytecie Przyrodniczym w Lublinie.</p>\r\n<p>\r\n	Konferencja odbywała się w dniach 27 - 29 czerwca 2013 r. i była zorganizowana pod honorowym patronatem JM Rektora Uniwersytetu Przyrodniczego w Lublinie oraz Marszałka Wojew&oacute;dztwa Lubelskiego.</p>\r\n<p>\r\n	Celem konferencji było zaprezentowanie innowacyjnych technologii w zakresie gospodarki odpadami, degradacji i rekultywacji gleb, gospodarki wodno &ndash; ściekowej, a także zr&oacute;wnoważonego rozwoju i kształtowania środowiska.</p>\r\n<p>\r\n	Miałem przyjemność i satysfakcję w imieniu Marszałka Wojew&oacute;dztwa Lubelskiego - Krzysztofa Hetmana otworzyć konferencję wraz z JM Rektorem Uniwersytetu Przyrodniczego - prof. dr hab. Marianem Wesołowskim.</p>\r\n<p>\r\n	Następnie wygłosiłem prezentację nt. &bdquo;Działania Samorządu Wojew&oacute;dztwa Lubelskiego w zakresie wspierania rozwoju OZE w regionie. Inteligentne Specjalizacje Regionu&rdquo;.</p>', 'porada-image-54085e0e94d20.jpeg', 0, 'Innowacyjne Techn. w Inżynierii i Kształtowaniu Środowiska', 'Innowacyjne, Techn., Inżynierii, Kształtowaniu, Środowiska, czerwca, 2013', '27 czerwca 2013 r.', '2014-09-04 14:41:49', NULL, '2013-06-27 00:00:00'),
(34, NULL, 1, NULL, 0, 'V Wschodnie Forum Gospodarcze Lub-Invest', 'v-wschodnie-forum-gospodarcze-lub-invest', '<p>\r\n	W dniach 20&ndash;21 czerwca 2013 r. na terenie Międzynarodowych Targ&oacute;w Lubelskich S.A. w Lublinie odbyło się V Wschodnie Forum Gospodarcze Lub &ndash; Invest. W spotkaniu uczestniczyli przedsiębiorcy, samorządowcy i ambasadorzy, kt&oacute;rzy rozmawiali o wsp&oacute;łpracy w ramach obszaru przygranicznego i rozwoju biznesu we wschodniej części Europy.</p>', '<p>\r\n	W dniach 20&ndash;21 czerwca 2013 r. na terenie Międzynarodowych Targ&oacute;w Lubelskich S.A. w Lublinie odbyło się V Wschodnie Forum Gospodarcze Lub &ndash; Invest. W spotkaniu uczestniczyli przedsiębiorcy, samorządowcy i ambasadorzy, kt&oacute;rzy rozmawiali o wsp&oacute;łpracy w ramach obszaru przygranicznego i rozwoju biznesu we wschodniej części Europy.</p>\r\n<p>\r\n	W ramach programu Forum jednym z blok&oacute;w tematycznych był panel - &bdquo;Możliwości rozwoju fotowoltaiki w wojew&oacute;dztwie lubelskim.&rdquo;</p>\r\n<p>\r\n	W programie wzięli udział przedstawiciele ministerstwa gospodarki, nauki, biznesu i samorząd&oacute;w.</p>\r\n<p>\r\n	Spotkanie było doskonałą okazją do przekazania informacji i wymiany pogląd&oacute;w w zakresie perspektywy rozwoju energetyki odnawialnej a gł&oacute;wnie fotowoltaiki.</p>\r\n<p>\r\n	Informacje TV KRAŚNIK:&nbsp;<a href="http://www.ktv.com.pl/glowna/?f=2076#skip_content">http://www.ktv.com.pl/glowna/?f=2076#skip_content</a></p>', 'porada-image-54085f29cbbfa.jpeg', 0, 'V Wschodnie Forum Gospodarcze Lub-Invest', 'Wschodnie, Forum, Gospodarcze, Lub-Invest, dniach, 20&ndash;21, czerwca, 2013, terenie, Międzynarodowych', 'W dniach 20&ndash;21 czerwca 2013 r. na terenie Międzynarodowych Targ', '2014-09-04 14:46:33', '2014-09-04 14:53:25', '2013-06-21 00:00:00'),
(35, NULL, 2, NULL, 0, 'Wykluczenie cyfrowe I - Spotkanie z partnerami projektu', 'wykluczenie-cyfrowe-i-spotkanie-z-partnerami-projektu', '<p>\r\n	17 grudnia 2013 r. uczestniczyłem w spotkaniu z partnerami Projektu &quot;Przeciwdziałanie wykluczeniu cyfrowemu w wojew&oacute;dztwie lubelskim (edycja I)&quot;.</p>', '<p>\r\n	17 grudnia 2013 r. uczestniczyłem w spotkaniu z partnerami Projektu &quot;Przeciwdziałanie wykluczeniu cyfrowemu w wojew&oacute;dztwie lubelskim (edycja I)&quot;.</p>\r\n<p>\r\n	Gł&oacute;wnym celem spotkania było roczne podsumowanie funkcjonowania sprzętu komputerowego i Internetu u Beneficjent&oacute;w Ostatecznych.</p>\r\n<p>\r\n	W spotkaniu wzięli udział przedstawiciele firm: z ramienia firmy Talex S.A. prezentację przedstawił Pan Piotr Jaszczak - Dyrektor ds. Usług Wsparcia i Projekt&oacute;w IT, Firmę ENECO reprezentował Pan Artur Komorowski. Z ramienia Lidera projektu prezentację przedstawił Pan Krzysztof Ławnik - Koordynator projektu. Zostały om&oacute;wione takie zagadnienia jak: inwentaryzacja sprzętu przeprowadzona na koniec roku, zwiększenie liczby Beneficjent&oacute;w Ostatecznych, awarie zgłaszane do Talex-u i Nordisk-u i związane z tym problemy, transfery danych. Przedstawiono r&oacute;wnież najbliższe działania w Projekcie, kt&oacute;rymi będą: monitoring i ankietyzacja Beneficjent&oacute;w Ostatecznych, przetarg na wyłonienie wykonawc&oacute;w na dostawę zestaw&oacute;w komputerowych, usługę dostępu do Internetu i szkolenie, weryfikacja jednostek podległych, dystrybucja materiał&oacute;w promocyjnych przekazanych przez Władzę Wdrażającą Programy Europejskie.</p>\r\n<p>\r\n	<br />\r\n	Projekt &quot;Przeciwdziałanie wykluczeniu cyfrowemu w wojew&oacute;dztwie lubelskim&quot; jest realizowany dzięki wsp&oacute;łfinansowaniu przez Unię Europejską w ramach Europejskiego Funduszu Rozwoju Regionalnego realizowany w ramach Programu Operacyjnego Innowacyjna Gospodarka na lata 2007-2013, Oś Priorytetowa 8 Społeczeństwo Informacyjne - zwiększanie Innowacyjności Gospodarki, Działanie 8.3 Przeciwdziałanie wykluczeniu cyfrowemu - eInclusion.</p>\r\n<p>\r\n	Całkowita wartość projektu: 18 445 445,00 PLN</p>\r\n<p>\r\n	Wartość dofinansowania: 18 445 445,00 PLN</p>\r\n<p>\r\n	Projekt jest realizowany od 2010 roku w ramach konsorcjum Wojew&oacute;dztwa Lubelskiego, 47 Gmin i dw&oacute;ch Powiat&oacute;w wojew&oacute;dztwa lubelskiego.</p>\r\n<p>\r\n	Gł&oacute;wnym celem projektu jest zapewnienie bezpłatnego dostępu do Internetu oraz niezbędnych urządzeń i oprogramowania dla 2795 gospodarstw domowych z terenu 47 Gmin i 2 Powiat&oacute;w wojew&oacute;dztwa lubelskiego, zagrożonych wykluczeniem z aktywnego uczestnictwa w społeczeństwie informacyjnym, ze względu na trudną sytuację materialną lub niepełnosprawność. Dodatkowo w sprzęt komputerowy i Internet zostaną wyposażone instytucje podległe JST (306 zestaw&oacute;w komputerowych i 81 podłączeń do Internetu).</p>\r\n<p>\r\n	Udział w Projekcie jest bezpłatny. Koszty uczestnictwa w Projekcie pokrywane są z Europejskiego Funduszu Rozwoju Regionalnego (85%) i Budżetu Państwa (15%).</p>\r\n<p>\r\n	Zasięg/Partnerzy Projektu:</p>\r\n<p>\r\n	Gmina Adam&oacute;w, Gmina i Miasto Annopol, Gmina Borki, Gmina Cyc&oacute;w, Gmina Czemierniki, Gmina Dębowa Kłoda, Gmina Dubienka, Gmina Goraj, Gmina Hańsk, Gmina Hrubiesz&oacute;w, Gmina Jeziorzany, Gmina Kodeń, Gmina Konopnica, Gmina Konstantyn&oacute;w, Gmina i Miasto Krasnobr&oacute;d, Gmina Krzywda, Gmina Kur&oacute;w, Gmina Leśna Podlaska, Miasto Lublin, Gmina Łabunie, Gmina Łomazy, Gmina Miączyn, Miasto Międzyrzec Podlaski, Gmina Międzyrzec Podlaski, Gmina Milan&oacute;w, Gmina Mircze, Gmina Niedrzwica Duża, Gmina Piszczac, Gmina Podedw&oacute;rze, Miasto Puławy, Gmina Radzyń Podlaski, Miasto Rejowiec Fabryczny, Gmina Rejowiec Fabryczny, Gmina Sawin, Gmina Siedliszcze, Gmina Sosnowica, Gmina Spiczyn, Gmina Stanin, Gmina Stary Brus, Gmina Telatyn, Gmina Trzydnik Duży, Gmina Tuczna, Gmina Wierzbica, Miasto Włodawa, Gmina Wola Uhruska, Gmina W&oacute;lka, Gmina Żyrzyn, Powiat Puławski, Powiat Świdnicki.</p>\r\n<p>\r\n	Projekt będzie realizowany do 2015 roku.</p>', 'porada-image-54086175720ed.jpeg', 0, 'Wykluczenie cyfrowe I - Spotkanie z partnerami projektu', 'Wykluczenie, cyfrowe, Spotkanie, partnerami, projektu, grudnia, 2013, uczestniczyłem, spotkaniu, Projektu', '17 grudnia 2013 r. uczestniczyłem w spotkaniu z partnerami Projektu "Przeciwdziałanie wykluczeniu cyfrowemu w wojew', '2014-09-04 14:56:20', NULL, '2013-12-17 00:00:00'),
(36, NULL, 2, NULL, 0, '2908 km sieci szerokopasmowej na terenie województwa lubelskiego', '2908-km-sieci-szerokopasmowej-na-terenie-wojewodztwa-lubelskiego', '<p>\r\n	<span style="color: rgb(20, 24, 35); font-family: \'lucida grande\', tahoma, verdana, arial, sans-serif; font-size: 13.3333339691162px; line-height: 18px; background-color: rgb(255, 255, 255);">22 maja 2013 r. miałem przyjemność i satysfakcję uczestniczyć w uroczystości podpisania umowy na realizację projektu &quot;Sieć Szerokopasmowa Polski Wschodniej - wojew&oacute;dztwo lubelskie&quot;, realizowanego w Departamencie Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie.&nbsp;</span><br style="color: rgb(20, 24, 35); font-family: \'lucida grande\', tahoma, verdana, arial, sans-serif; font-size: 13.3333339691162px; line-height: 18px; background-color: rgb(255, 255, 255);" />\r\n	&nbsp;</p>', '<p>\r\n	22 maja 2013 r. miałem przyjemność i satysfakcję uczestniczyć w uroczystości podpisania umowy na realizację projektu &quot;Sieć Szerokopasmowa Polski Wschodniej - wojew&oacute;dztwo lubelskie&quot;, realizowanego w Departamencie Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie.</p>\r\n<p>\r\n	W podpisaniu umowy w Lubelskim Parku Naukowo-Technologicznym S.A. wiął udział marszałek wojew&oacute;dztwa lubelskiego Krzysztof Hetman i wicemarszałek Sławomir Sosnowski, kt&oacute;rzy podpisali umowę na realizację projektu &quot;Sieć Szerokopasmowa Polski Wschodniej - wojew&oacute;dztwo lubelskie&quot;, z wykonawcą firmą Aeronaval de Construcciones e Instalaciones z Hiszpanii.</p>\r\n<p>\r\n	Jest to projekt o znaczeniu cywilizacyjnym dla naszego regionu. W historii samorządu wojew&oacute;dztwa nie było jeszcze inwestycji realizowanej na tak dużą skalę. W wojew&oacute;dztwie lubelskim powstanie 2 908 km sieci oraz 312 węzł&oacute;w do dalszej dystrybucji Internetu. Sieć szerokopasmowa będzie przebiegała przez 198 jednostek samorządu terytorialnego.</p>\r\n<p>\r\n	Dzięki wybudowanej sieci Internetowej w zasięgu sieci szerokopasmowej znajdzie się do 95 proc. gospodarstw domowych wojew&oacute;dztwa lubelskiego. W ramach projektu, w zakresie praktycznego wykorzystania Internetu oraz technik cyfrowych, przeszkolonych zostanie ponad 2 tysiące os&oacute;b.</p>\r\n<p>\r\n	Wartość całego projektu to ponad 385 mln złotych, dofinansowanie w wysokości prwie 267 mln złotych będzie pochodziło ze środk&oacute;w Programu Operacyjnego Rozw&oacute;j Polski Wschodniej 2007-2013. Planowany termin zakończenia budowy sieci to 31 lipca 2015 r.</p>\r\n<p>\r\n	Projekt jest realizowany w Departamencie Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie.</p>\r\n<p>\r\n	KORZYŚCI DLA REGIONU:</p>\r\n<p>\r\n	1. Poprawa poziomu życia mieszkańc&oacute;w i aktywizacja społeczności lokalnych:</p>\r\n<p>\r\n	- usprawnienie i obniżenie koszt&oacute;w elektronicznej komunikacji,<br />\r\n	- przeciwdziałanie wykluczeniu cyfrowemu,<br />\r\n	- zwiększenie poziomu wiedzy i kompetencji mieszkańc&oacute;w,<br />\r\n	- ułatwienie mieszkańcom regionu załatwiania spraw administracyjnych, i innych formalności oraz zaspokojenie potrzeb informacyjnych, analitycznych i edukacyjnych za pomocą technologii informatycznych (budowa społeczeństwa informacyjnego / cyfrowego),<br />\r\n	- wzrost liczby nowych inwestycji z zakresu IT w regionie.</p>\r\n<p>\r\n	2. Aktywizacja zawodowa i proces zmian na rynku pracy:</p>\r\n<p>\r\n	- uelastycznienie rynku pracy,<br />\r\n	- wzrost poziomu og&oacute;lnego wykształcenia ludności dający podstawę do budowy gospodarki opartej na wiedzy,<br />\r\n	- rozw&oacute;j kształcenia ustawicznego dający szansę na wzrost poziomu kwalifikacji<br />\r\n	i umiejętności zasob&oacute;w ludzkich w regionie,<br />\r\n	- powstawanie nowych dziedzin działalności gospodarczej, opartych na nowoczesnych technologiach informatycznych i telekomunikacyjnych,<br />\r\n	- rozw&oacute;j inwestycji i usług w obszarze &bdquo;ostatniej mili&rdquo;,<br />\r\n	- ułatwienie aktywności os&oacute;b niepełnosprawnych oraz wzrost perspektyw w ich dostępie do pracy (m.in. poprzez wzrost możliwości świadczenia telepracy).</p>\r\n<p>\r\n	3. Przyśpieszenie proces&oacute;w gospodarczych:</p>\r\n<p>\r\n	- poprawa wizerunku regionu,<br />\r\n	- likwidacja barier technologicznych, wzrost możliwości wdrażania najnowszych rozwiązań technologicznych,<br />\r\n	- oddolne otwarcie na nowe rynki zbytu i usługi,<br />\r\n	- rozw&oacute;j kontakt&oacute;w międzynarodowych firm z regionu i wzrost ich świadomości w zakresie konkurowania w oparciu o wiedzę i nowe technologie.</p>\r\n<p>\r\n	4. Proces zmian na rynku usług teleinformatycznych:</p>\r\n<p>\r\n	- spadek cen usług dostępu do Internetu,<br />\r\n	- demonopolizacja rynku usług komunikacyjnych.</p>\r\n<p>\r\n	5. Podniesienie poziomu edukacji.</p>\r\n<p>\r\n	6. Zr&oacute;wnanie szans w dostępie do informacji.</p>\r\n<p>\r\n	7. Usprawnienie jakości pracy samorząd&oacute;w lokalnych.</p>\r\n<p>\r\n	8. Rozw&oacute;j infrastruktury i usług społeczeństwa informacyjnego:</p>\r\n<p>\r\n	e-nauka<br />\r\n	e-praca<br />\r\n	e-zdrowie<br />\r\n	e-biznes<br />\r\n	e-handel<br />\r\n	e-urząd</p>', 'porada-image-54086261012dc.jpeg', 0, '2908 km sieci szerokopasmowej na terenie województwa lubelskiego', '2908, sieci, szerokopasmowej, terenie, województwa, lubelskiego, maja, 2013', '22 maja 2013 r.', '2014-09-04 15:00:16', NULL, '2013-05-22 00:00:00'),
(37, NULL, 2, NULL, 0, 'Badanie fokusowe dot. Kluczowych Technologii Wspomagających', 'badanie-fokusowe-dot-kluczowych-technologii-wspomagajacych', '<p>\r\n	W dniu 5 kwietnia 2013 r. w siedzibie Departamentu Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie odbyło się spotkanie fokusowe będące jednym z element&oacute;w realizowanego w ramach projektu RSZZG badania dziedzinowego pt.: &bdquo;Ocena innowacyjności i konkurencyjności regionalnej gospodarki z punktu widzenia stosowania i rozwoju Kluczowych Technologii Wspomagających&quot;.</p>', '<p>\r\n	W dniu 5 kwietnia 2013 r. w siedzibie Departamentu Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie odbyło się spotkanie fokusowe będące jednym z element&oacute;w realizowanego w ramach projektu RSZZG badania dziedzinowego pt.: &bdquo;Ocena innowacyjności i konkurencyjności regionalnej gospodarki z punktu widzenia stosowania i rozwoju Kluczowych Technologii Wspomagających&quot;.</p>\r\n<p>\r\n	Celem badania jest uzyskanie pogłębionej wiedzy nt. stopnia zaangażowania jednostek naukowych Lubelszczyzny w proces tworzenia i rozwoju technologii KET, a także poziomu wykorzystywania tych technologii przez przemysł i gospodarkę regionu.</p>\r\n<p>\r\n	Miałem przyjemność otworzyć i moderować spotkanie.</p>', 'porada-image-5408631595696.jpeg', 0, 'Badanie fokusowe dot. Kluczowych Technologii Wspomagających', 'Badanie, fokusowe, dot., Kluczowych, Technologii, Wspomagających, dniu, kwietnia, 2013', 'W dniu 5 kwietnia 2013 r.', '2014-09-04 15:03:17', NULL, '2013-04-05 00:00:00'),
(38, NULL, 10, NULL, 0, 'TEST 1', 'test-1', '<p>\r\n	TEST 1</p>', NULL, 'porada-image-5408d74852ea3.jpeg', 0, 'TEST 1', 'TEST', '', '2014-09-04 23:19:03', NULL, '2014-09-04 00:00:00'),
(39, NULL, 11, NULL, 0, 'TEST 2', 'test-2', '<p>\r\n	TEST 2</p>', NULL, 'porada-image-5408d77427a88.jpeg', 0, 'TEST 2', 'TEST', '', '2014-09-04 23:19:47', NULL, '2014-09-04 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `porada_category`
--

CREATE TABLE `porada_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arrangement` int(11) NOT NULL,
  `color` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `menu_bg_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `porada_category`
--

INSERT INTO `porada_category` (`id`, `name`, `slug`, `arrangement`, `color`, `photo`, `description`, `menu_bg_img`) VALUES
(1, 'Energetyka', 'energetyka', 3, 'ff7a0c', 'category-image-53fe119f595c2.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu wykorzystania potencjału regionalnych zasob&oacute;w energetycznych i rozwoju alternatywnych źr&oacute;deł energii.</p>', 'category-thumb-53fe5f0fa501f.jpeg'),
(2, 'Cyfryzacja', 'cyfryzacja', 1, '1188c9', 'category-image-5401a8f0dc35a.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu zwiększenia dostępności technologii informacyjno-komunikacyjnych i rozwoju społeczeństwa cyfrowego.</p>', 'category-thumb-53fe5c76c51e4.jpeg'),
(3, 'Inwestycje', 'inwestycje', 5, '69b3ca', 'category-image-5404a2d450d79.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu i projekt&oacute;w prorozwojowych oraz zwiększania atrakcyjności inwestycyjnej.</p>', 'category-thumb-53fe5f24020fc.jpeg'),
(4, 'Nauka', 'nauka', 6, '7cbb38', 'category-image-53fe30e5e9ef5.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu inwestowania w edukację, naukę, kapitał ludzki oraz ustawiczne kształcenie.</p>', 'category-thumb-53fe5f2c4d22c.jpeg'),
(5, 'Planowanie strategiczne', 'planowanie-strategiczne', 2, 'ffb324', 'category-image-53fe26031fc71.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu trafnego określania cel&oacute;w rozwojowych i skutecznego zarządzania procesami rozwojowymi w gospodarce.</p>', 'category-thumb-53fe5f05a7589.jpeg'),
(6, 'Przedsiębiorczość', 'przedsiebiorczosc', 7, 'ee4627', 'category-image-53fe31971f949.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu podnoszenia konkurencyjności i innowacyjności firm, pozyskiwania inwestycji zagranicznych oraz tworzenia nowych miejsc pracy.</p>', 'category-thumb-53fe5f34d8a81.jpeg'),
(7, 'Innowacje', 'innowacje', 9, 'a82a5f', 'category-image-53fe3226e015d.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu wzmacniania rozwoju sektora badawczo-rozwojowego oraz wykorzystania wynik&oacute;w prac badawczych w gospodarce.</p>', 'category-thumb-53fe5f3d3d7d2.jpeg'),
(10, 'Współpraca i partnerstwa gospodarcze', 'wspolpraca-i-partnerstwa-gospodarcze', 8, '831b7a', 'category-image-5409e9faba877.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu budowy sieci wsp&oacute;łpracy branżowej i klastrowej oraz partnerstw gospodarczych.</p>', 'category-thumb-54085956b0b0c.jpeg'),
(11, 'Strategia spójności', 'strategia-spojnosci', 4, 'd1b5aa', 'category-image-5404c7d1d1abe.jpeg', '<p>\r\n	testowy opis</p>', 'category-thumb-5408596656881.jpeg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pricing_item`
--

CREATE TABLE `pricing_item` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `promo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  `promo_price` decimal(6,2) DEFAULT NULL,
  `karnet` tinyint(1) DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `pricing_item`
--

INSERT INTO `pricing_item` (`id`, `name`, `description`, `promo_description`, `price`, `promo_price`, `karnet`, `arrangement`) VALUES
(1, 'Open', 'nieograniczony wstęp do centrum przez okres 31 dni', 'Promocja obowiązuje tylko w czerwcu', 99.00, 75.00, 1, 1),
(2, 'Bezpieczne Solarium - Słoneczna Łączka', '1 minuta', NULL, 1.00, NULL, 0, 10),
(3, 'Sauna (1-2 osoby)', '1 godzina', NULL, 25.00, NULL, 0, 7),
(4, 'Sauna (3-4 osoby)', '1 godzina', NULL, 35.00, NULL, 0, 8),
(5, 'Kompleksowe Odchudzanie', 'Karnet Open, dwie konsultacje motywacyjno dietetyczne, analiza składu ciała', NULL, 199.00, NULL, 1, 3),
(7, 'Morning Fit', 'wstęp na siłownię przez okres 31 dni w godzinach porannych (codziennie do godziny czternastej)', NULL, 70.00, NULL, 1, 2),
(8, 'Dwie konsultacje dietetyczno-motywacyjne', 'Plan żywieniowy na miesiąc + analiza składu ciała', NULL, 120.00, NULL, 0, 9),
(9, 'Jednorazowe wejście na siłownie', NULL, NULL, 15.00, NULL, 0, 12),
(10, 'Open na okres 3 miesiące', 'nieograniczony wstęp do centrum przez okres 93 dni', NULL, 240.00, NULL, 0, 4),
(11, 'Karnet 10 wejść', NULL, NULL, 99.00, NULL, 0, 6),
(12, 'Karnet 5 wejść', NULL, NULL, 60.00, NULL, 0, 5),
(13, 'Analiza składu Ciała wraz z omówieniem', NULL, NULL, 20.00, NULL, 0, 11),
(14, 'Pakiet Antycelulitowy', '(dla osób posiadających karnet wstępu cena: 135 zł)', NULL, 165.00, NULL, 0, 13),
(15, 'Karnet Vibro', NULL, NULL, 50.00, NULL, 0, 14),
(16, 'Jednorazowe Wejście Vibro', NULL, NULL, 8.00, NULL, 0, 15),
(18, 'Karnet solarium 15 sesji', '300 min', NULL, 220.00, 110.00, 0, 16),
(19, 'Karnet solarium 10 sesji', '200 minut', NULL, 160.00, 80.00, 0, 17),
(20, 'Karnet solarium 5 sesji', '100 min', NULL, 85.00, 42.50, 0, 18),
(21, 'Karnet solarium 3 sesje', '60 min', NULL, 50.00, 25.00, 0, 19);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_polish_ci,
  `photo` varchar(255) NOT NULL,
  `active` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `refs_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `project`
--

INSERT INTO `project` (`id`, `title`, `content`, `photo`, `active`, `gallery_id`, `slug`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`, `refs_id`) VALUES
(8, 'Porsche Panamera GTS', '<p>\r\n	Nowiutkie Porsche Panamera (800km przebiegu) zostało poddane zabiegom z pakietu Protect New Car.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Wyznajemy zasadę że lepiej zapobiegać niż leczyć a więc Porsche naszego stałego Klienta trafiło do nas praktycznie prosto z salonu na kompleksowe zabezpieczenie.</p>\r\n<p>\r\n	W pierwszej fazie Prosiak dostał pełne mycie SPA wraz z dekontaminacją. Następnie wykonaliśmy delikatną korektę lakieru aby usunąć mikrorysy i perfekcyjnie przygotować powierzchnię pod zabezpieczenie. Finalnie samoch&oacute;d został zabezpieczony powłoką kwarcową Echelon Zen-Xero Basic.</p>', 'porada-image-5436d8e6886b3.jpeg', 0, 7, 'porsche-panamera-gts', 0, 'Zabezpieczenie nowego lakieru', 'powłoka kwarcowa, zabezpieczenie nowego auta,', NULL, '2014-10-09 20:44:21', NULL, '2013-12-11 00:00:00', NULL),
(9, 'Dodge RAM', '<p>\r\n	Ogromny Dodge RAM trafił do nas na pełną kosmetykę wraz z renowacją i wymianą uszkodzonych element&oacute;w. Samoch&oacute;d od lat użytkowany intensywnie, r&oacute;wnież w ciężkim terenie, służący właścicielowi przede wszystkim do pracy. Jednak jeśli coś może pracować brudne to może i pracować czyste :) dlatego Dodge trafił do nas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Zakres wykonanych zabieg&oacute;w był ogromny i obejmował:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		pełne mycie SPA z dekontaminacją chemiczną i glinkowaniem,</li>\r\n	<li>\r\n		pełną korektę lakieru,</li>\r\n	<li>\r\n		kompleksowe czyszczenie i zabezpieczenie wnętrza,</li>\r\n	<li>\r\n		obszycie kierownicy sk&oacute;rą,</li>\r\n	<li>\r\n		wykonanie nowych orurowań progowych ze stali nierdzewnej (stare były przegnite),</li>\r\n	<li>\r\n		polerowanie pozostalych orurowań,</li>\r\n	<li>\r\n		renowację kloszy reflektor&oacute;w,</li>\r\n	<li>\r\n		odświeżenie felg chromowanych,</li>\r\n	<li>\r\n		renowację lub wymianę element&oacute;w dekoracyjnych,</li>\r\n	<li>\r\n		pełne zabezpieczenie lakieru powłoką ceramiczną,</li>\r\n</ul>', 'porada-image-5436dd69e14f0.jpeg', 0, 8, 'dodge-ram', 0, 'Pełna kosmetyka lakieru i wnętrza', 'polerowanie lakieru, renowacja metalu, polerowanie stali nierdzewnej', NULL, '2014-10-09 21:09:29', NULL, '2013-09-09 00:00:00', NULL),
(10, 'Lexus LS600H', '<p>\r\n	Zawitał do nas 5-letni Lexus w możliwie najbardziej luksusowym wydaniu LS600H. Samochód mimo 5 lat użytkowania nie ma żadnych uszkodzeń wewnątrz. Skóra jest w idealnym stanie, podobnie jak pozostałe elementy wnętrza. Trochę inaczej sytuacja wyglądała z nadwoziem. Pięć lat bez zabezpieczenia odcisnęło swoje piętno na powierzchni lakieru. Sporo rys, w tym kilkanaście bardzo głębokich, zmatowienia, wgniotki i odpryski.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Samochód przeszedł pełny proces odnowy SPA na który składało się:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		pełne mycie SPA z dekontaminacją,</li>\r\n	<li>\r\n		usunięcie wgnieceń z błotnika przedniego metodą bez lakierowania,</li>\r\n	<li>\r\n		pełna korekta lakieru z wycinaniem papierem grubych zarysowań,</li>\r\n	<li>\r\n		przygotowanie lakieru pod zabezpieczenie,</li>\r\n	<li>\r\n		zabezpieczenie lakieru powłoką ceramiczną,</li>\r\n	<li>\r\n		zabezpieczenie felg powłoka ceramiczną,</li>\r\n	<li>\r\n		pełny pakiet pielęgnacji wnętrza z zabezpieczeniem,</li>\r\n</ul>', 'porada-image-54441c371eb05.jpeg', 0, 9, 'lexus-ls600h', 0, NULL, NULL, NULL, '2014-10-19 22:16:54', '2014-12-10 22:20:51', '2014-10-19 00:00:00', NULL),
(11, 'Chevrolet Corvette C1', '<p>\r\n	Piękna, klasyczna Corvetta trafiła do nas na dopieszczenie po pełnej renowacji wykonanej przez ekipę Game Over Cycles. Chevrolet trafił do nas już kompletny a my zajęliśmy się doprowadzeniem go do stanu perfekcyjnego.<br />\r\n	<br />\r\n	Podczas 2 tygodniowej pracy wykonaliśmy:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		pełne mycie SPA z dekontaminacją,</li>\r\n	<li>\r\n		wygładzenie struktury lakieru papierami ściernymi,</li>\r\n	<li>\r\n		pełna korekta lakieru,</li>\r\n	<li>\r\n		pełna pielęgnacja wnętrza pojazdu wraz z zabezpieczeniem tapicerki sk&oacute;rzanej,</li>\r\n	<li>\r\n		zabezpieczenie lakieru i element&oacute;w zewnętrznych powłoką ceramiczną,</li>\r\n	<li>\r\n		czyszczenie i pielęgnacja komory silnika,</li>\r\n	<li>\r\n		polerowanie i zabezpieczenie ozdobnych dekli felg,</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>', 'porada-image-5455f7fd89424.jpeg', 0, 10, 'chevrolet-corvette-c1', 0, NULL, NULL, NULL, '2014-11-02 10:23:08', NULL, '2013-08-26 00:00:00', NULL),
(12, 'VW Touareg', '<p>\r\n	Pewnego dnia trafił do nas pewien Volkswagen. Na pierwszy rzut oka robota jak każda inna - jednak nie w tym wypadku. Wnętrze samochodu por&oacute;wnać można było do ogromnej popielniczki. Setki wypalonych w środku papieros&oacute;w odbiło piętno na wyglądzie i zapachu wnętrza. Właściciel obiecał poprawę i ograniczenie palenia w środku a my obiecaliśmy że wnętrze wr&oacute;ci do pierwotnego stanu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Zabraliśmy się więc ostro za pracę i przez 2 dni intensywnych zabieg&oacute;w wykonaliśmy:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		pranie ekstrakcyjne wykładziny podłogowej i bagażnika,</li>\r\n	<li>\r\n		gruntowne czyszczenie i konserwacja tapicerki sk&oacute;rzanej,</li>\r\n	<li>\r\n		dokładne wyczyszczenie deski, plastik&oacute;w i zakamark&oacute;w,</li>\r\n	<li>\r\n		ręczne czyszczenie podsufitki,</li>\r\n	<li>\r\n		czyszczenie uszczelek,</li>\r\n	<li>\r\n		mycie szyb,</li>\r\n	<li>\r\n		usunięcie brzydkich zapach&oacute;w z wnętrza,</li>\r\n	<li>\r\n		konserwacja deski i plastik&oacute;w,</li>\r\n</ul>', 'porada-image-5457e1edab006.jpeg', 0, 11, 'vw-touareg', 0, NULL, NULL, NULL, '2014-11-03 21:13:33', NULL, '2014-01-20 00:00:00', NULL),
(14, 'Toyota Avensis III', '<p>\r\n	Tym razem zadbaliśmy o piękną i świetnie wyposażoną Toyotę Avensis. Samoch&oacute;d w nienajgorszym stanie ale nigdy wcześniej odpowiednio nie pielęgnowany, dlatego też na lakierze pełno zarysowań, we wnętrzu plastiki już szare no i świecąca się tapicerka sk&oacute;rzana. Toyotca brakowało blasku a właściciel postawił przed nami zadanie doprowadzenia jej do stanu salonowego.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Pracy było sporo a przez tydzień czasu wykonaliśmy:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		pełne mycie SPA z dekontaminacją,</li>\r\n	<li>\r\n		korektę lakieru,</li>\r\n	<li>\r\n		lakierowanie maski i zderzaka,</li>\r\n	<li>\r\n		zabezpieczenie lakieru powłoką ceramiczną,</li>\r\n	<li>\r\n		pełen pakiet czyszczenia wnętrza wraz z zabezpieczeniem,</li>\r\n</ul>', 'porada-image-545d0a4b29a17.jpeg', 0, 15, 'toyota-avensis-iii', 0, NULL, NULL, NULL, '2014-11-07 19:07:06', NULL, '2014-09-17 00:00:00', NULL),
(15, 'Honda Accord S-type', '<p>\r\n	Na lokalny zlot samochod&oacute;w marki Honda przygotowaliśmy pięknego Accorda S-type. Był on ozdobą naszego stoiska firmowego na zlocie.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Przygotowania Hondy objęły:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		mycie SPA z dekontaminacją,</li>\r\n	<li>\r\n		korekta lakieru,</li>\r\n	<li>\r\n		usunięcie morki z połowy maski dla pokazania efektu przed i po,</li>\r\n	<li>\r\n		dressing opon i plastik&oacute;w zewnętrznych,</li>\r\n	<li>\r\n		zabezpieczenie lakieru wysokiej klasy woskiem konkursowym,</li>\r\n</ul>', 'porada-image-545d13f3f2410.jpeg', 0, 16, 'honda-accord-s-type', 0, NULL, NULL, NULL, '2014-11-07 19:48:19', NULL, '2014-08-15 00:00:00', NULL),
(16, 'BMW 635csi', '<p>\r\n	Piękne, klasyczne BMW 635csi z 1985 roku trafiło do nas na kompleksowy pakiet pielęgnacji i zabezpieczenia. Samoch&oacute;d niedawno przeszedł pełną renowację i wymagał dopieszczenia, skupienia na detalach i zabezpieczenia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Prawie 2 tygodnie ciągłej pracy i mn&oacute;stwo wykonanych zabieg&oacute;w, m.in:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		pełne mycie SPA z dekontaminacją</li>\r\n	<li>\r\n		pełna korekta lakieru wraz z częściowym wyr&oacute;wnaniem struktury</li>\r\n	<li>\r\n		czyszczenie komory silnika i jego element&oacute;w,</li>\r\n	<li>\r\n		odświeżenie felg i chromowanych rant&oacute;w i zabezpieczenie powłoką ceramiczną,</li>\r\n	<li>\r\n		zabezpieczenie lakieru powłoka kwarcową,</li>\r\n	<li>\r\n		sesja zdjęciowa,</li>\r\n</ul>', 'porada-image-545d22ea83e9b.jpeg', 0, 17, 'bmw-635csi', 0, NULL, NULL, NULL, '2014-11-07 20:51:08', NULL, '2014-03-15 00:00:00', NULL),
(17, 'Mercedes-Benz E-klasse W211', '<p>\r\n	Nasz stały Klient podwi&oacute;zł nam swoją E-klase abyśmy wygładzili strukturę lakieru i przygotowali samoch&oacute;d do sprzedaży. Lakier został wygładzony za pomocą papier&oacute;w ściernych, następnie wypolerowany i zabezpieczony. Odświeżyliśmy r&oacute;wnież wnętrza a zaprzyjaźniony fotograf wykonał sesję sprzedażową.</p>', 'porada-image-545d2e24743e1.jpeg', 0, 18, 'mercedes-benz-e-klasse-w211', 0, NULL, NULL, NULL, '2014-11-07 21:40:04', NULL, '2013-07-30 00:00:00', NULL),
(19, 'Skoda Octavia Scout', '<p>\r\n	&quot;Terenowa&quot; Octavia trafiła do nas na pakiet przygotowania do sprzedaży wraz z sesją zdjęciową.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Autko bardzo zadbane, wymagało dopieszczenia wnętrza i lekkiej korekty lakieru. Nasz fotograf wykonał kilka zdjęć po całym procesie pielęgnacji.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Życzymy właścicielowi szybkiej sprzedaży a nowemu nabywcy zadowolenia z bardzo zadbanej Octavii.</p>', 'porada-image-545dc0917ccb4.jpeg', 0, 20, 'skoda-octavia-scout', 0, NULL, NULL, NULL, '2014-11-08 08:04:49', NULL, '2014-07-10 00:00:00', NULL),
(20, 'Audi A4 B7', '<p>\r\n	Opr&oacute;cz typowej kosmetyki czy lakiernictwa, zajmujemy się r&oacute;wnież stylizacją pojazd&oacute;w. Właśnie na taka usługę trafiło do nas Audi, k&oacute;trego właściciel nie miał zbyt sp&oacute;jnej wizji na sw&oacute;j samoch&oacute;d.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Wykonaliśmy szereg zabieg&oacute;w i modyfikacji aby nadać końcowo całości subtelny i atrakcyjny wygląd.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		pełna korekta lakieru wraz z wygładzeniem jego struktury,</li>\r\n	<li>\r\n		naprawy lakiernicze kilku element&oacute;w,</li>\r\n	<li>\r\n		adaptacja zderzaka z S-line wraz z przer&oacute;bką mocowania intercoolera i rur,</li>\r\n	<li>\r\n		wymiana końc&oacute;wek tłumik&oacute;w na Ulter Black Line,</li>\r\n	<li>\r\n		lakierowanie chromowanych element&oacute;w ozdobnych, oznaczeń, dokładki zderzaka tylnego i reling&oacute;w na czarno,</li>\r\n	<li>\r\n		zabezpieczenie końcowe samochodu woskiem,</li>\r\n	<li>\r\n		dob&oacute;r alufelg 19&#39;</li>\r\n</ul>', 'porada-image-545dc61cdf86e.jpeg', 0, 21, 'audi-a4-b7', 0, NULL, NULL, NULL, '2014-11-08 08:28:28', NULL, '2014-10-10 00:00:00', NULL),
(21, 'Honda Accord VIII', '<p>\r\n	Honda Accord &oacute;smej generacji to piekny samoch&oacute;d. A piękny samoch&oacute;d dobrze gdy jest dopieszczony i regularnie pielęgnowany. Właśnie na pakiet kompleksowego odświeżenia trafił do nas czarny japończyk.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Wykonaliśmy kilka zabieg&oacute;w aby doprowadzić go do stanu perfekcyjnego, m.in.:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		pełne mycie SPA z dekontaminacją chemiczną i glinkowaniem,</li>\r\n	<li>\r\n		korekta lakieru,</li>\r\n	<li>\r\n		czyszczenie tapicerki sk&oacute;rzanej z impregnacją,</li>\r\n	<li>\r\n		pielęgnacja wnętrza pojazdu,</li>\r\n	<li>\r\n		zabezpieczenie woskiem syntetycznym,</li>\r\n</ul>', 'porada-image-545f3562dc689.jpeg', 0, 22, 'honda-accord-viii', 0, NULL, NULL, NULL, '2014-11-09 10:35:30', NULL, '2014-09-08 00:00:00', NULL),
(22, 'Alfa Romeo 147 silver', '<p>\r\n	Srebrna Alfa Romeo 147 trafiła do naszego studia na stylizację i kompleksową pielęgnację. Samoch&oacute;d miał właścicielowi słuzyć następne lata jednak końcowo koncepcja się zmieniła i po szeregu zabieg&oacute;w upiększających został wystawiony do sprzedaży.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Przy samochodzie wykonaliśmy:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		montaż sprężyn obniżających zawieszenie H&amp;R,</li>\r\n	<li>\r\n		oklejenie dachu folią,</li>\r\n	<li>\r\n		lakierowanie felg lakierem dobranym do koloru folii na dachu,</li>\r\n	<li>\r\n		montaż czarnych przednich reflektor&oacute;w TYC,</li>\r\n	<li>\r\n		montaż ozdobnej końc&oacute;wki tłumika,</li>\r\n	<li>\r\n		obszycie kierownicy sk&oacute;rą licową gładką i perforowaną wraz z ozdobnym przeszyciem,</li>\r\n	<li>\r\n		oklejenie rączek drzwi sk&oacute;rą wraz z przeszyciem,</li>\r\n	<li>\r\n		korekta lakieru wraz z zabezpieczeniem,</li>\r\n	<li>\r\n		pełen pakiet pielegnacji wnętrza,</li>\r\n	<li>\r\n		sesja zdjęciowa,</li>\r\n</ul>', 'porada-image-545f39ac236b5.jpeg', 0, 23, 'alfa-romeo-147-silver', 0, NULL, NULL, NULL, '2014-11-09 10:53:47', NULL, '2014-07-16 00:00:00', NULL),
(23, 'Toyota Avensis II', '<p>\r\n	Krwisto czerwona Toyota przyjechała do nas na kompleksową pielęgnację. Samoch&oacute;d był użytkowany od lat przez tego samego właściciela. Niestety pojawiły się drobne mankamenty kt&oacute;re usunęliśmy i przywr&oacute;ciliśmy Avensis blask.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Zakres usług:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		korekta lakieru,</li>\r\n	<li>\r\n		oklejenie dachu czarną folią,</li>\r\n	<li>\r\n		lakierowanie obu zderzak&oacute;w,</li>\r\n	<li>\r\n		pielęgnacja wnętrza,</li>\r\n</ul>', 'porada-image-545f3ddfe8a60.jpeg', 0, 24, 'toyota-avensis-ii', 0, NULL, NULL, NULL, '2014-11-09 11:11:43', NULL, '2014-01-27 00:00:00', NULL),
(24, 'VW Passat B5 FL', '<p>\r\n	Właściciel tego VW jest bardzo wymagający i wyczulony na punkcie swojego pojazdu. Samoch&oacute;d jest w stanie nienagannym, bardzo zadbany, czysty i świetnie się prezentuje. Wisienką na torcie byłby doprowadzony do świetnego stanu lakier i tym się właśnie zajęliśmy.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Wykonaliśmy kilkuetapową korektę lakieru oraz poprawki lakiernicze. Końcowo lakier dostał zabezpieczenie w postaci wosku syntetycznego. Między czasie wykonaliśmy r&oacute;wnież korektę kloszt reflektor&oacute;w kt&oacute;re przez lata trochę zż&oacute;łkły. Efekt końcowy bardzo zadowala. Piękny Passat jest jeszcze ładniejszy a właściciel jeszcze bardziej zadowolony.</p>', 'porada-image-545f41ab01b5a.jpeg', 0, 25, 'vw-passat-b5-fl', 0, NULL, NULL, NULL, '2014-11-09 11:27:54', NULL, '2014-10-09 00:00:00', NULL),
(25, 'BMW X6', '<p>\r\n	Prosto z salonu Klient przyjechał swoim nowiutkim X6. Mimo, iz samoch&oacute;d zupełnie nowy to jednak na lakierze sporo rys, z niewłaściwego obchodzenia się z samochodem w transporcie i na salonie.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Oczywiście wszystkie defekty zostały zniwelowane a lakier i felgi dostały zabezpieczenie w postaci powłoki kwarcowej.</p>', 'porada-image-545f47a8ddf64.jpeg', 0, 26, 'bmw-x6', 0, NULL, NULL, NULL, '2014-11-09 11:53:28', NULL, '2014-03-10 00:00:00', NULL),
(26, 'BMW 645 & Mercedes_SL65', '<p>\r\n	Na życzenie naszych stałych Klient&oacute;w wykonujemy r&oacute;wnież mobilne usługi auto detailingu. Tym razem zajęlismy się dopieszczeniem dw&oacute;ch sportowych samochod&oacute;w BMW i Mercedes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Oba samochody w stanie praktycznie wzorowym. Nasza praca polegała na myciu SPA i dopracowaniu detali we wnętrzu pojazd&oacute;w. Końcowo zabezpieczyliśmy lakiery woskiem dostarczonym nam przez Klienta.</p>', 'porada-image-545f4ad3e14f1.jpeg', 0, 27, 'bmw-645-mercedes-sl65', 0, NULL, NULL, NULL, '2014-11-09 12:06:59', NULL, '2013-08-28 00:00:00', NULL),
(27, 'Chevrolet Bel Air', '<p>\r\n	Kolejny ciekawy projekt od ekipy Game Over. Kultowy Chevrolet Bel Air w pełni odrestaurowany trafił do nas na końcową pielęgnację i zabezpieczenie przed powrotem do Niemiec do swojego właściciela.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Wykonaliśmy m.in.:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		pełną korektę lakieru,</li>\r\n	<li>\r\n		mycie podwozia i element&oacute;w zawieszenia,</li>\r\n	<li>\r\n		polerowanie elekent&oacute;w lakierowanych wewnątrz samochodu,</li>\r\n	<li>\r\n		impregnację dachu cabrio,</li>\r\n	<li>\r\n		zabezpieczenie lakieru powłoka ceramiczną,</li>\r\n	<li>\r\n		pielęgnację wnętrza samochodu.</li>\r\n</ul>', 'porada-image-545f588b6102c.jpeg', 0, 28, 'chevrolet-bel-air', 0, NULL, NULL, NULL, '2014-11-09 13:05:30', NULL, '2014-01-20 00:00:00', NULL),
(28, 'Volvo S60', '<p>\r\n	Tym razem zaopiekowaliśmy się Volvo S60 a dokładniej jego fotelami przednimi. Ktoś kiedyś pr&oacute;bował coś poprawić w wyglądzie jasnych element&oacute;w sk&oacute;rzanych i zrobił to na tyle niedbale że efekt był okropny. Plamy, smugi, przebarwienia, miejsca niedomalowane, źle dobrany kolor, zesztywniałe od farby nici. Po prostu tragedia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Pracy było sporo:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		usunęliśmy całkowicie starą farbę ze sk&oacute;ry i nici,</li>\r\n	<li>\r\n		położylismy specjalny grunt izolujący,</li>\r\n	<li>\r\n		dokładnie wyczyściliśmy ciemne części tapicerki kt&oacute;re poprzedni &quot;spec&quot; zafarbował,</li>\r\n	<li>\r\n		położyliśmy nowy lakier na jasne elementy, dobrany do reszty tapicerki</li>\r\n	<li>\r\n		całość została zabezpieczona specjalnym bezbarwnym lakierem utrwalającym</li>\r\n</ul>', 'porada-image-545f5b9ce7c86.jpeg', 0, 29, 'volvo-s60', 0, 'Volvo S60', 'Volvo, razem, zaopiekowaliśmy, się, dokładniej, jego, fotelami, przednimi., Ktoś, kiedyś', 'Tym razem zaopiekowaliśmy się Volvo S60 a dokładniej jego fotelami przednimi. Ktoś kiedyś pr', '2014-11-09 13:18:36', '2014-11-13 22:54:49', '2014-10-16 00:00:00', NULL),
(29, 'Alfa Romeo 159', '<p>\r\n	Jeden z trudniejszych projekt&oacute;w jaki nam się trafił. Czarna Alfa 159 z niesamowicie porysowanym lakierem trafiła do naszego SPA po pomoc. Nie mieliśmy dużo czasu bo właściciel był przejazdem w Rzeszowie ale udało się sprostać wyzwaniu. Lakier dostał nowe życie i samoch&oacute;d nabrał blasku a właściciel po odbiorze ledwo go poznał.</p>', 'porada-image-545fefe2eea77.jpeg', 0, 30, 'alfa-romeo-159', 0, 'Alfa Romeo 159', 'Alfa, Romeo, Jeden, trudniejszych, projekt', 'Jeden z trudniejszych projekt', '2014-11-09 23:51:14', '2014-11-13 22:54:27', '2013-12-30 00:00:00', NULL),
(30, 'Mercedes-Benz S-klasse W220', '<p>\r\n	Wykonaliśmy naprawę 2 foteli sk&oacute;rzanych do Mercedesa klasy S. Sk&oacute;ra była miejscami poprzecierana, pojawiły się pęknięcia i utrata koloru.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Naprawa polegała na:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		dokładnym wyczyszczeniu sk&oacute;ry,</li>\r\n	<li>\r\n		zeszlifowaniu powierzchni naprawianej papierami ściernymi,</li>\r\n	<li>\r\n		uzupełnieniu miejsc popękanych szpachlą do sk&oacute;ry,</li>\r\n	<li>\r\n		zagruntowaniu miejsc naprawianych,</li>\r\n	<li>\r\n		położeniu nowego lakieru,</li>\r\n	<li>\r\n		zabezpieczeniu całości sk&oacute;ry lakierem bezbarwnym</li>\r\n</ul>', 'porada-image-545ff47e02c19.jpeg', 0, 31, 'mercedes-benz-s-klasse-w220', 0, 'Mercedes-Benz S-klasse W220', 'Mercedes-Benz, S-klasse, W220, Wykonaliśmy, naprawę, foteli, skórzanych, Mercedesa, klasy, Skóra', 'Wykonaliśmy naprawę 2 foteli skórzanych do Mercedesa klasy S. Skóra była miejscami poprzecierana, pojawiły się pęknięcia i utrata koloru.', '2014-11-10 00:10:53', '2014-11-13 22:36:02', '2013-12-22 00:00:00', NULL),
(32, 'Infiniti FX50', '<p>\r\n	Każdy szczegół w takim wnętrzu jest istotny. Akurat w tym pięknym Infiniti, szczegółem wymagającym dopracowania była kierownica i gałka zmiany biegów, które przez kilka lat użytkowania straciły swój oryginalny wygląd. Właściciel przywiózł nam swój samochód we właściwym momencie, gdy skóra na tych elementach straciła jedynie kolor i pojawiły się przebarwienia. W takim momencie można przywrócić bezproblemowo niemal idealny stan.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Naprawa polegała na:</p>\r\n<ul>\r\n	<li>\r\n		dokładnym czyszczeniu skóry,</li>\r\n	<li>\r\n		przeszflifowaniu drobym papierem ściernym,</li>\r\n	<li>\r\n		położeniu podkładu izolujacego i przyczepnościowego,</li>\r\n	<li>\r\n		polakierowaniu skóry indywidualnie dobraną farbą,</li>\r\n	<li>\r\n		zabezpieczeniu lakierem utrwalającym,</li>\r\n</ul>', 'porada-image-54969a9b855fb.jpeg', 0, 44, 'infiniti-fx50', 0, 'Infiniti FX50', 'Infiniti, FX50', NULL, '2014-12-21 11:02:03', '2014-12-21 11:07:45', '2014-12-21 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `promo`
--

CREATE TABLE `promo` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `photo` varchar(255) NOT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `promo`
--

INSERT INTO `promo` (`id`, `title`, `content`, `photo`, `arrangement`) VALUES
(1, '- 10% przy rezerwacji usługi z przedpłatą', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;"><span style="font-family: Arial,Helvetica,sans; line-height: 14px; text-align: justify;">Przyjedź do nas, zadzwoń lub napisz i zarez</span>erwuj dowolną usługę z naszej oferty. Dokonaj przedpłaty got&oacute;wką lub przelewem a otrzymasz <strong>10% rabatu</strong> na zarezerwowaną usługę oraz gwarancję terminu. </span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Więcej informacji mailowo lub telefonicznie.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">Rabat łączy się z innymi promocjami. Maksymalny poziom rabatowania nie może przekroczyć 20%.</span></p>', 'promo-image-547b00891f167.jpeg', 0),
(2, 'Zadbaj o doskonałą widoczność w zimie ! 100 zamiast 150 !', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Usługa polerowania kloszy reflektor&oacute;w przednich wraz z zabezpieczeniem powłoką ceramiczną teraz tylko <span style="font-size:18px;"><strong>100,00 zł </strong></span>zamiast 150,00 zł.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">Promocja trwa do końca stycznia. </span>Promocja nie łączy się z innymi.</p>', 'promo-image-547b012d949c0.jpeg', 0),
(3, '15% wartości usługi osoby poleconej', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Jeśli polecisz nam kolejnego Klienta a ten skorzysta z naszych usług, to Ty dostajesz do wykorzystania u nas 15% wartości wykupionej przez tą osobę usługi. Przykładowo, poleciłeś swojemu bratu naszą firmę. Brat do nas przyjechał, skorzystał z polerowania lakieru za 1 000 pln to Ty otrzymujesz do wykorzystania pulę 150 pln na usługi z naszej oferty. </span></p>\r\n<p>\r\n	<span style="font-size:14px;">Możesz wykorzystać tą pulę częściowo, możesz zaczekać aż uzbiera się większa kwota kt&oacute;ra pozwoli na jakiś większy pakiet lub dopłacić do droższej usługi resztę. </span></p>\r\n<p>\r\n	<span style="font-size:14px;">Wyb&oacute;r należy do Ciebie.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">Promocja nie łączy się z innymi rabatami. Pieniądze otrzymywane za polecenia są wirtualne i mogą zostać wykorzystane jedynie w naszej firmie na usługi z naszej oferty.</span></p>', 'promo-image-547b016232f6b.jpeg', 0),
(4, '- 10% dla Alfaholików', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Dla wszystkich posiadaczy samochod&oacute;w marki Alfa Romeo mamy 10% rabatu na wszystkie usługi z naszej oferty, Warunkiem otrzymania rabatu jest posiadanie klubowych (Alfaholicy) ramek tablic rejestracyjnych.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	Rabat łączy się z innymi promocjami (bez promocji kwotowych, tylko rabatowe). Maksymalny poziom rabatowania nie może przekroczyć 20%.</p>', 'promo-image-547b01be30bf1.jpeg', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `refs`
--

CREATE TABLE `refs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_short` longtext COLLATE utf8_unicode_ci,
  `content` longtext COLLATE utf8_unicode_ci,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `services`
--

INSERT INTO `services` (`id`, `gallery_id`, `category_id`, `subcategory_id`, `title`, `slug`, `content_short`, `content`, `photo`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`, `price`, `time`) VALUES
(1, 32, 6, NULL, 'Podstawowe mycie SPA', 'podstawowe-mycie-spa', '<p>\r\n	<span style="font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Najprostsza, a zarazem bardzo ważna czynność, niezbędna w procesie regularnej pielęgnacji pojazdu. Pozwala utrzymać samoch&oacute;d w czystości, </span>przy regularnym stosowaniu zapobiegając wżerom z resztek owad&oacute;w, ptasich odchod&oacute;w czy negatywnego wpływu soli drogowej.</p>', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Podstawowe mycie SPA jest całkowicie bezpieczne dla lakieru i może być wykonywane w każdej chwili. Jest szczeg&oacute;lnie polecane dla Klient&oacute;w kt&oacute;rzy zdecydowali się zabezpieczyć sw&oacute;j samoch&oacute;d woskiem, sealantem lub powłoką kwarcowa/ceramiczną jako regularna pielęgnacja zabezpieczonego lakieru. Dzięki użyciu w procesie mycia piany aktywnej i szamponu o neutralnym PH, mamy pewność że nie osłabiamy wosku ani powłoki. Ponadto użycie Quick Detailera po myciu nadaje głębszy blask i wzmacnia ochronę lakieru.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;"><span style="font-family: Arial,Helvetica,sans; line-height: 14px; text-align: justify;">W skład usługi podstawowego mycia SPA wchodzi:</span></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">spłukanie pojazdu z luźnych zabrudzeń,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">mycie felg i nadkoli,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">pokrycie pojazdu bezpieczną pianą aktywną,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">dokładne spłukanie,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">mycie rękawicą i szamponem,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">wycieranie do sucha miękkimi ręcznikami,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">wydmuchanie resztek wody ze szczelin,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">przetarcie wnęk drzwiowych,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">nabłyszczenie, usunięcie smug oraz zabezpieczenie lakieru za pomocą Quick Detailera,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">dressing opon i plastik&oacute;w zewnętrznych,</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">Wszystkie usługi wykonujemy po uprzedniej rezerwacji telefonicznej lub osobistym um&oacute;wieniu się!</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>', 'services-image-545ff7d67075b.jpeg', 1, 'to change', 'to change', 'to change', '2014-09-19 00:09:01', NULL, '2014-09-19 00:00:00', '60-80 zł', '1-2 h'),
(2, 33, 6, NULL, 'Pełne mycie SPA z dekontaminacją', 'pelne-mycie-spa-z-dekontaminacja', '<p>\r\n	Usługa polecana w szczeg&oacute;lności dla mocno zabrudzonych. Stosowana okresowo przed nałożeniem wosku jako element regularnej pielęgnacji oraz każdorazowo przed korektą lakieru jako element perfekcyjnego przygotowania lakieru przed renowacją.</p>', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Usługa ta r&oacute;żni się od wersji podstawowej stosowanymi preparatami i zakresem działania. Wykonujemy ją przede wszystkim wtedy gdy lakier na samochodzie jest zaniedbany, był przez dłuższy okres czasu nie myty lub myty nieodpowiednio i posiada zabrudzenia drogowe, owady, ptasie odchody, żywicę i soki z drzew, itp. - zabrudzenia kt&oacute;rych nie usuniemy w trakcie podstawowego mycia SPA. Zastosowane preparaty są agresywne dla wosk&oacute;w i mogą je osłabiać ale dzięki swoim intensywnym właściwościom pozwalają perfekcyjnie oczyścić lakier. Pełne mycie stosujemy zawsze przed dalszą renowacją (korektą lakieru) lub zabezpieczeniem nowego samochodu powłoką ceramiczną aby dokładnie odtłuścić lakier i usunąć z niego wszelkie zanieczyszczenia.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">W skład usługi wchodzi:</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">spłukanie pojazdu z luźnych zabrudzeń,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">mycie felg i nadkoli,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">dekontaminacja chemiczna (usunięcie smoły, zywicy, owad&oacute;w, opiłk&oacute;w metalicznych)</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">pokrycie pojazdu bezpieczną pianą aktywną,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">dokładne spłukanie,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">mycie rękawicą i szamponem,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">glinkowanie lakieru glinką dobraną do typu lakieru,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">ponowne mycie rekawicą i szamponem,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">wycieranie do sucha miękkimi ręcznikami,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">wydmuchanie resztek wody ze szczelin,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">przetarcie wnęk drzwiowych,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">nabłyszczenie, usunięcie smug oraz zabezpieczenie lakieru za pomocą Quick Detailera,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">dressing opon i plastik&oacute;w zewnętrznych,</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">Wszystkie usługi wykonujemy po uprzedniej rezerwacji telefonicznej lub osobistym um&oacute;wieniu się!</span></p>', 'services-image-54612b8a64431.jpeg', 1, 'to change', 'to change', 'to change', '2014-11-10 22:18:01', NULL, '2014-11-10 00:00:00', '150-200 zł', '3-5 h'),
(3, 34, 6, NULL, 'Protect NEW Car', 'protect-new-car', '<p>\r\n	Pakiet wykonywany na nowych samochodach, kt&oacute;rych lakier nie wymaga korekty. Jest to nasz flagowy zestaw zabieg&oacute;w pozwalający perfekcyjnie oczyścić, przygotować i zabezpieczyć powłoką kwarcową/ceramiczną nowy samoch&oacute;d od razu po wyjeździe z salonu.</p>', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Usługi z pakietu Protect NEW Car dedykowane są pojazdom nowym, nie wymagającym gruntownej korekty lakieru. Dzięki ochronie od pierwszego dnia po zakupie, Tw&oacute;j samoch&oacute;d będzie o wiele mniej narażony na działanie czynnik&oacute;w środowiska takich jak: ptasie odchody, owady, żywica z drzew, s&oacute;ld drogowa, kwaśne deszcze, promienie UV, itp. Dzięki zastosowaniu w procesie zabezpieczenia najnowocześniejszych powłok ceramicznych/kwarcowych uzyskujemy na powierzchni zabezpieczanej grubą, wytrzymałą powłokę, kt&oacute;ra maksymalnie chroni powierzchnię przed powstawaniem mikrozarysowań, przed korozją i utlenianiem lakieru a przede wszystkim ułatwia regularną pielęgnację pojazdu, dzięki właściwościom hydrofobowym.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Zabezpieczyć powłoką można każdą powierzchnię - lakier, metal, szyby, plastik a nawet powierzchnie gumowe. Dzięki szerokiej ofercie dostępnych zabezpieczeń każdy znajdzie coś dla siebie, na miarę oczekiwanego efektu i możliwości finansowych.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Pakiet Protect NEW Car podzieliliśmy na kilka rodzaj&oacute;w, r&oacute;żniących się od siebie zakresem wykonawanych czynności.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;"><strong>Exterior Protect Light</strong></span></p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">pełne mycie SPA z dekontaminacją</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">wygładzenie lakieru pastą finish, usunięcie niewielkich pojedynczych rysek,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">maszynowe przygotowanie lakieru cleanerem/dedykowanym primerem,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie powierzchni lakierowanych powłoką kwarcową/ceramiczną/nanotytanową,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">dressing tworzyw sztucznych i opon,</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;"><strong>Exterior Protect Standard</strong></span></p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">pełne mycie SPA z dekontaminacją</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">wygładzenie lakieru pastą finish, usunięcie niewielkich pojedynczych rysek,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">maszynowe przygotowanie lakieru cleanerem/dedykowanym primerem,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie powierzchni lakierowanych powłoką kwarcową/ceramiczną/nanotytanową,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie szyby czołowej i przednich bocznych powłoką hydrofobową (niewidzialną wycieraczką),</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie felg powłoką ceramiczną,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">dressing tworzyw sztucznych i opon,</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;"><strong>Exterior Protect Full</strong></span></p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">pełne mycie SPA z dekontaminacją</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">wygładzenie lakieru pastą finish, usunięcie niewielkich pojedynczych rysek,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">maszynowe przygotowanie lakieru cleanerem/dedykowanym primerem,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie powierzchni lakierowanych powłoką kwarcową/ceramiczną/nanotytanową,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie wszystkich szyb powłoką hydrofobową (niewidzialną wycieraczką)</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie felg powłoką ceramiczną,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie tworzyw sztucznych i opon powłoką hydrofobową,</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;"><strong>Interior Protect Light</strong></span></p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">dokładne odkurzenie wnętrza</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">impregnacja foteli przednich materiałowych lub sk&oacute;rzanych nanopowłoką,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">impregnacja dywanik&oacute;w przednich materiałowych nanopowłoka,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">impregnacja kierownicy nanopowłoką,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie tworzyw dressingiem z filtrem UV,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie uszczelek dressingiem,</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;"><strong>Interior Protect Standard</strong></span></p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">dokładne odkurzenie wnętrza</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">impregnacja foteli przednich i kanapy nanopowłoką,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">impregnacja dywanik&oacute;w materiałowych nanopowłoka,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie kierownicy nanopowłoką,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie tworzyw sztucznych nanopowłoką,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie uszczelek dressingiem,</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;"><strong>Interior Protect Full</strong></span></p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">dokładne odkurzenie wnętrza</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">impregnacja foteli przednich i kanapy nanopowłoką,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">impregnacja dywanik&oacute;w materiałowych nanopowłoka,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">impregnacja wykładziny podłogowej nanopowłoką,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie kierownicy nanopowłoką,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie tworzyw sztucznych nanopowłoką,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">zabezpieczenie uszczelek nanopowłoką,</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Pakiety usług zewnątrz i wewnątrz można dowolnie łączyć. Istnieje oczywiście możliwość modyfikacji usług w danym pakiecie wg życzeń Klienta. Powyższe pakiety są jedynie propozycją wynikającą z naszych obserwacji i doświadczeń.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">Na cenę usługi ma wpływ wiele czynnik&oacute;w, m.in. wielkość i rodzaj samochodu, wybrany rodzaj powłoki, wybrany pakiet. Dokładne informacje dotyczące ceny udzielamy po indywidualnej rozmowie lub mailowo.</span></p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">Usługę Protect NEW Car wykonujemy w naszym studiu lub jeszcze w salonie przed odbiorem samochodu. Druga opcja wymaga um&oacute;wienia się przynajmniej 3 tyg wcześniej.</span></p>', 'services-image-54628c7f20bf4.jpeg', 1, 'to change', 'to change', 'to change', '2014-11-11 23:23:58', NULL, '2014-11-11 00:00:00', '900-5000 zł', '2-3 dni'),
(4, 35, 6, NULL, 'Naprawa reflektorów', 'naprawa-reflektorow', '<p>\r\n	<span class="red">Częstym problemem występującym przy samochodach używanych są klosze reflektor&oacute;w. Przez lata pokrywy reflektor&oacute;w ż&oacute;łkną, lakier zabezpieczający schodzi płatami lub pęka. Korekta reflektor&oacute;w przywr&oacute;ci im fabryczny blask i poprawi jakość świecenia lampy.</span></p>', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Przez lata użytkowania elementy samochodu ulegają zużyciu i trzeba je wymieniać. W przypadku reflektor&oacute;w wymiana nie jest konieczna gdyż można przywr&oacute;cić im fabryczny wygląd wykonując korektę kloszy. Najczęstszym problemem jest ż&oacute;łknięcie powłoki zabezpieczającej klosz, wynikające z wpływu słońca, kiepskiej jakości chemii, wpływu czynnik&oacute;w zewnętrznych. Do tego dochodzą r&oacute;wnież rysy kt&oacute;re powstają z nieumiejętnego mycia lub odśnieżania reflektor&oacute;w.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Korekta kloszy dzieli się na 2 rodzaje:</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">lekka, gdy powłoka zabezpieczająca jest możliwa do uratowania poprzez polerowanie,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">pełna, gdy należy całkowicie ściągnąć starą powłokę aby przywr&oacute;cić lampie fabryczny wygląd,</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">W pierwszym przypadku wykonujemy delikatną korektę powłoki zabezpieczającej za pomoca materiał&oacute;w ściernych a następnie polerujemy maszynowo aż do uzyskania nienagannego wyglądu.</span></p>\r\n<p>\r\n	<span style="font-size:14px;">W drugim przypadku usuwamy całkowicie starą powłokę zostawiając gołe tworzywo, kt&oacute;re następnie polerujemy do osiągnięcia pełnej przejrzystości i zabezpieczamy powłoką ceramiczną lud dedykowanym lakierem bezbarwnym do lamp.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">O ile w pierwszym przypadku zabezpieczenie jest dodatkiem, wpływającym oczywiście na trwałość efektu, o tyle w drugim jest to konieczność. Nie zabezpieczona lampa będzie bardzo szybko ż&oacute;łkła i będzie bardzo podatna na wszelkie uszkodzenia.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;"><strong>Częstym problemem jest r&oacute;wnież parowanie lampy</strong>. Zazwyczaj jest to efekt zatkanego odpowietrzenia ale zdarza się r&oacute;wnież że samoch&oacute;d miał kolizje i lampa była naprawiana lub jest po prostu rozszczelniona. Na życzenie Klienta demontujemy parujący reflektor, diagnozujemy przyczynę i usuwamy jeśli to możliwe. Najczęściej wystarczy wysuszenie lampy i poprawa odpowietrzenia. Jeśli wina leży w szczelności to suszymy reflektor i uszczelniamy specjalnym płynnym silikonem.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">Korektę wykonujemy jedynie na reflektorach wykonanych z tworzywa. </span></p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">Nie gwarantujemy usunięcia wszystkich defekt&oacute;w (rys, odprysk&oacute;w) gdyż niekt&oacute;re mogą być za głębokie.</span></p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">Cena za uszczelnianie reflektor&oacute;w podawana jest zawsze indywidualnie.</span></p>', 'services-image-54704a3115e67.jpeg', 1, 'to change', 'to change', 'to change', '2014-11-22 09:32:48', NULL, '2014-11-22 00:00:00', '150-250 pln', '4h'),
(5, 36, 6, NULL, 'Korekta lakieru', 'korekta-lakieru', '<p>\r\n	Najważniejsza i najbardziej czasochłonna usługa w procesie auto detailingu, pozwalająca wyciągnąć blask i piękno nawet z bardzo zniszczonego lakieru. Koniec z matowym i porysowanym lakierem na kt&oacute;rym czas odcisnął swoje piętno.</p>', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">W całym, szeroko pojętym Auto Detailingu, proces korekty lakieru jest najważniejszy i przynosi najbardziej widoczne efekty. Zniszczone felgi, zż&oacute;łknięte reflektory czy zaśniedziałe listwy ozdobne to tylko szczeg&oacute;ły, kt&oacute;re wpływają oczywiście na wygląd całościowy samochodu ale nie one są najważniejsze. Nawet najdroższe felgi, nowiutkie reflektory czy ozdobne dodatki nie podniosą oceny wizualnej jeśli lakier samochodu będzie w kiepskim stanie.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Z upływającymi latami użytkowania, lakiery samochodowe tracą swoje piekno i blask. Wpływ na to ma wiele czynnik&oacute;w - przede wszystkim promienie UV, niewłaściwa chemia do mycia, używanie szczotek w procesie mycia auta lub mycie na myjniach automatycznych. Dużą rolę odgrywają r&oacute;wnież czynniki środowiskowe - s&oacute;l drogowa, ptasie odchody, owady czy żywica z drzew. Najczęstszymi objawami nieprawidłowego stanu lakieru są rysy, zmatowienia, wżery, smugi, odbarwienia.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Skomplikowany, kilkuetapowy proces polerowania lakieru pozwala usunąć większość defekt&oacute;w z&nbsp; lakieru. Tak przygotowana powierzchnia staje sie wolna od wad, pięknie się błyszczy i nabiera blasku. Wygładzenie lakieru oraz jego zabezpieczenie wpływa znacząco na łatwość p&oacute;źniejszej pielęgnacji.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Bardzo istotne jest, aby korektę lakieru przeprowadzała osoba mają o tym pojęcie i doświadczenie. Niewłaściwe przeprowadzenie procesu polerowania może nieść za sobą nieodwracalne lub trudne do usunięcia skutki, m.in. przetarcie lakieru bezbarwnego, przegrzanie powierzchni lakieru, głębokie swirle i hologramy powstałe podczas nieumięjętnego polerowania. Tylko doświadczony specjalista jest w stanie wykonać korektę bezpiecznie i bez obaw o uszkodzenie lakieru.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Korektę lakieru dzielimy na 4 podstawowe rodzaje:</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;"><strong>KOREKTA LEKKA</strong> - 2 etapowy proces wygładzenia lakieru. Usuwamy 50-70 % zarysowań i defekt&oacute;w. (500-600 zl)</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;"><strong>KOREKTA STANDARDOWA</strong> - 3 etapowy proces pozwalający usunąć 80-90% defekt&oacute;w. (900-1000 zł)</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;"><strong>KOREKTA PEŁNA</strong> - 3 i więcej etapowy proces, w kt&oacute;rym usuwamy każde możliwe do usunięcia uszkodzenie. W tym procesie używamy r&oacute;wnież papier&oacute;w ściernych do punktowego wycinanai głębokich uszkodzeń. (1200-1400 zł)</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;"><strong>KOREKTA PEŁNA Z USUNIĘCIEM MORKI</strong> - kilkuetapowy proces polerowania lakieru i wygładzenia jego struktury za pomocą materiał&oacute;w ściernych. Najbardziej zaawansowany rodzaj korekty w ofercie pozwalający osiągnąć efekt lustra na lakierze. (1500-2000 zł)</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">Na cenę usługi korekty lakieru ma wpływ stopień zniszczenia lakieru, wielkość samochodu i oczekiwany efekt.</span></p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">W cenę usługi zawsze wliczone jest pełne mycie SPA z dekontaminacją oraz końcowe dwuetapowa <span style="font-size:14px;">(cleaner maszynowo + zabezpieczenie)</span> ochrona lakieru woskiem lub sealantem.</span></p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">NIE DAJ SIĘ ZWIEŚĆ WĄTPLIWEJ KLASY &quot;FACHOWCOM&quot;. Przeprowadzenie właściwej korekty w 4h nie jest możliwe. Profesjonalna usługa kosztuje a cena zależy w dużej mierze od czasu jej trwania, użytych materiał&oacute;w, wiedzy i doświadczenia specjalisty.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>', 'services-image-5471b4557e0aa.jpeg', 1, 'to change', 'to change', 'to change', '2014-11-23 11:17:56', NULL, '2014-11-23 00:00:00', '500-2000 PLN', '8-30 h'),
(6, 37, 6, NULL, 'Czyszczenie i konserwacja skóry', 'czyszczenie-i-konserwacja-skory', '<p>\r\n	Kolejny istotny zabieg, pozwalający zadbać o najważniejszy element wyposażenia samochodu. Piękna sk&oacute;ra w samochodzie to komfort podr&oacute;żowania i wysokie walory estetyczne, a żeby zawsze była piękna to konieczne są regularne zabiegi czyszczenia i konserwacji.</p>', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">W obecnych czasach tapicerka sk&oacute;rzana w samochodzie to już nie luksus zarezerwowany tylko dla wybranych. Coraz więcej samochod&oacute;w jest wyposażonych w sk&oacute;rzane obicia ze wzgledu na łatwość w pielęgnacji, podniesienie wartości samochodu oraz satysfakcji Klienta. Tylko zadbana tapicerka da właścicielowi radość z użytkowania.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Podstawą nienagannego wyglądu tapicerki sk&oacute;rzanej jest jej właściwa pielęgnacja. Właściwe czyszczenie i zabezpieczenie sk&oacute;ry to nie mycie ludwikiem i nałożenie oliwki dla dzieci. Najważniejsze jest użycie właściwych środk&oacute;w, przeznaczonych do pielęgnacji sk&oacute;ry. Tylko użycie profesjonalnych kosmetyk&oacute;w da nam gwarancję że przy czyszczeniu nie uszkodzimy sk&oacute;ry a zabezpieczenie osiągnie wymierne efekty.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Usługa pielegnacji sk&oacute;ry przeprowadzana jest zawsze w 2 etapach. Najpierw dokładnie czyścimy tapicerkę przy pomocy cleanera i szczotki ze specjalnym, bezpiecznym dla sk&oacute;ry włosiem. Używany w procesie czyszczenia środek jest w 100% naturalny i posiada wszystkie atesty, r&oacute;wnież bezpieczeństwa dla sk&oacute;ry ludzkiej. Dopiero po dokładnym oczyszczeniu sk&oacute;ry przystępujemy do głębokiej impregnacji, do kt&oacute;rej używamy specjalny impregnat. Krem nawilża sk&oacute;rę i odbudowuje jej warstwę ochronną, kt&oacute;ra ściera się podczas użytkowania. Zabezpieczona tapicerka łatwiej się czyści a brud i plamy nie wnikają w jej strukturę.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Usługi pielęgnacji sk&oacute;ry dzielimy na 2 podstawowe rodzaje:</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">Gruntowne czyszczenie a następnie zabezpieczenie tapicerki, gdy sk&oacute;ra przez dłuższy okres czasu nie była czyszczona.</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">Odświeżenie tapicerki i uzupełnienie warstwy ochronnej, w celu regularnej pielęgnacji.</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">Do pielęgnacji tapicerki używamy produkt&oacute;w LCK, rekomendowanych m.in. przez Porsche, VW, AUDI, BMW.</span></p>', 'services-image-5471cd839fa81.jpeg', 1, 'to change', 'to change', 'to change', '2014-11-23 13:05:23', NULL, '2014-11-23 00:00:00', '50-400 PLN', '2-5h'),
(7, 38, 6, NULL, 'Naprawa skóry', 'naprawa-skory', '<p>\r\n	Jeśli pojawi się problem ze sk&oacute;rą na fotelu czy kierownicy, wcale nie oznacza to że sk&oacute;rę trzeba wymienić. Dzięki odpowiedniej technologii jesteśmy w stanie naprawić uszkodzenia takie jak: utrata koloru, przetarcia, przecięcia, wypalenia i inne.</p>', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Często Klienci nieświadomi tego że tapicerkę sk&oacute;rzaną można naprawić, decydują się pochopnie na wymianę uszkodzonych element&oacute;w. W ONYXX jesteśmy w stanie pokonać większość problem&oacute;w powstałych na powierzchni sk&oacute;ry w trakcie użytkowania.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Najczęstszym problemem są przetarcia i utrata koloru, kt&oacute;re występują zazwyczaj na boczkach fotela kierowcy, kierownicy, gałce zmiany bieg&oacute;w. Rzadko lub nieodpowiednio pielęgnowana sk&oacute;ra, wsiadanie i wysiadanie kilka razy dziennie powodują nadmierne i przyspieszone zużycie sk&oacute;ry w miejscach najbardziej narażonych. Do tego dochodzą pęknięcia wysuszonej powierzchni sk&oacute;ry. Wszystkie te problemy jesteśmy w stanie usunąć za pomocą specjalnie opracowanych wypełniaczy, szpachli, podkład&oacute;w i lakier&oacute;w dedykowanych do każdego rodzaju tapicerki sk&oacute;rzanej. Dzięki cały czas ulepszanej technologii, naprawy są trwałe i estetyczne, a odnowiona i zabezpieczona sk&oacute;ra posłuży kolejne lata.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Radzimy sobie świetnie także z bardzo skomplikowanymi defektami - przecięciami sk&oacute;ry na wylot, wypalonymi dziurami po papierosie, startym całkowicie licem sk&oacute;ry. Jesteśmy w stanie odbudować strukturę oryginalnej sk&oacute;ry w miejscu naprawianym. Jest to bardzo pomocne przy drogich lub rzadkich modelach samochod&oacute;w, gdzie często wymiana nie wchodzi w grę.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">PRZYKŁADOWE CENY:</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">Naprawa czarnej kierownicy - 100 zł</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">Naprawa kierownicy w innym kolorze - 130 zł</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">Lakierowanie siedziska i oparcia fotela wraz z uzupełnieniem ubytk&oacute;w - 600 zł</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">Naprawa przetartego boczka fotela - 100-200 zł</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">Każda naprawa tapicerki wyceniana jest indywidualnie po uprzednich oględzinach.</span></p>', 'services-image-5472515b4361a.jpeg', 1, 'to change', 'to change', 'to change', '2014-11-23 22:27:54', NULL, '2014-11-23 00:00:00', 'od 100 PLN', 'od 2h'),
(8, 39, 6, NULL, 'Detailing wnętrza', 'detailing-wnetrza', '<p>\r\n	Pielęgnacja pojazdu wewnątrz to kolejny bardzo ważny składnik procesu Auto Detailingu. Czyste i zadbane wnętrze pozwala na podr&oacute;ż w komfortowych warunkach. Proponujemy zatem szereg czynności mających na celu doprowadzenie wnętrza do stanu idealnego.</p>', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Nasza praca to przede wszystkim skupienie na detalach i dopracowanie szczeg&oacute;ł&oacute;w. Największe pole do popisu to wnętrze pojazdu, kt&oacute;re powinno być r&oacute;wnie zadbane co lakier. Od lat specjalizujemy się w pielęgnacji i dopieszczaniu samochod&oacute;w i przeszły przez nasze ręce setki samochod&oacute;w. Zauważyliśmy przez ten okres czasu jedną rzecz, kt&oacute;ra się powtarza - Klienci za rzadko lub za p&oacute;źno decydują się na pielęgnacje wnętrza pojazdu. Tapicerkę przecież można wyprać w każdym momencie, sk&oacute;rę można wyczyścić i czy deskę przetrzeć jak się zabrudzi. Niestety brak regularnej pielęgnacji często prowadzi do nieodwracalnych lub ciężki do naprawy problem&oacute;w. Przetarta sk&oacute;ra na siedzeniach bo nie była regularnie konserwowana, tapicerka tak mocno zabrudzona że zafarbowała i nie wr&oacute;ci do swojego koloru, brud i kurz w systemach nawiewu, kt&oacute;ry możnaby usunąć chyba tylko poprzez ściągnięcie deski rozdzielczej, podłoga i bagażnik usmarowane do tego stopnia że nawet po czyszczeniu zostają trwałe odbarwienia. To są problemy estetyczne wynikające z zaniedbań ale to Państwa zdrowie cierpi na tym najbardziej. Jazda w brudnym, zakurzonych wnętrzu, pełnym roztoczy czy bakterii nie należy do przyjemnych a przede wszystkich ogromnie wpływa na nasze samopoczucie.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Przygotowaliśmy dla Państwa szereg zabieg&oacute;w pozwalających przywr&oacute;cić idealną czystość we wnetrzu samochodu a także usługi odświeżenia, pozwalające na regularną pielęgnację. Usługi podzielone są na pakiety aby każdy m&oacute;gł wybrać usługę odpowiednią dla jego samochodu i portfela. Oczywiście Klient może sam złożyć własny pakiet pielęgnacji, kt&oacute;ry będzie mu najbardziej odpowiadał. Wszystkie niezbędne informację juz wkr&oacute;tce będą zawarte w nowym cenniku.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Czynności kt&oacute;re wykonujemy w procesie pielęgnacji wnętrz:</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">pranie ekstrakcyjne tapicerki materiałowej wraz z suszeniem (tapicerka jest prana aż do osiągnięcia pełnej czystości i usunięcia plam)</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">czyszczenie i konserwacja tapicerki sk&oacute;rzanej lub wykonanej z alcantary,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">gruntowne czyszczenie deski rozdzielczej i element&oacute;w plastikowych wnętrza,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">czyszczenie ekstrakcyjne wykładziny podłogowej i bagażnika,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">czyszczenie i konserwacja uszczelek,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">pielęgnacja element&oacute;w metalowych i drewnianych we wnetrzu,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">czyszczenie zakamark&oacute;w - szyn foteli, profili pod wykładzinami, schowk&oacute;w, przesłonek, itp.</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">czyszczenie dywanik&oacute;w materiałowych i gumowych,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">impregnacja i zabezpieczenie tapicerki, foteli, plastik&oacute;w, sk&oacute;ry, alcantary</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">demontaż i montaż foteli, wykładziny, boczk&oacute;w, element&oacute;w wnętrza w przypadku bardzo mocnego zabrudzenia wnętrza,</span></li>\r\n</ul>', 'services-image-5473833f3e79a.jpeg', 1, 'to change', 'to change', 'to change', '2014-11-24 20:13:02', NULL, '2014-11-24 00:00:00', '40-1500 PLN', '2-25 h'),
(9, 40, 6, NULL, 'Zabezpieczenie lakieru', 'zabezpieczenie-lakieru', '<p>\r\n	Aby lakier zawsze był pięknie błyszczący a mycie proste i przyjemne, oferujemy Państwu szereg możliwości zabezpieczenia lakieru. Od najprostszego woskowania, poprzez powłoki syntetyczne aż do topowych na świecie powłok ceramicznych, kwarcowych lub nanotytanowych.</p>', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Aby utrzymać lakier w nienagannej kondycji przez lata, ważne jest jego odpowiednie zabezpieczenie. Oferowane przez nas woski, sealanty, powłoki kwarcowe, ceramiczne lub tytanowe zapewniają perfekcyjną ochronę oraz ułatwiają regularną pielęgnację. Nie ważne czy samochód jest nowy, czy używany - każdy zasługuje na solidne zabezpieczenie.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Dzieki zróżnicowaniu cen i typów oferowanych zabezpieczeń, każdy Klient może dobrać ochronę odpowiednią dla swojego samochodu, sposobu użytkowania, oczekiwanego efektu a przede wszystkim dostępnego budżetu. Bazujemy na produktach najlepszych na rynku, uznanych na świecie producentów - nie oferujemy marketowych wosków czy powłok kiepskiej jakości.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class="title">\r\n	<span style="font-size:14px;"><strong>CLEANER</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Podstawą właściwego zabezpieczenia jest odpowiednie przygotowanie lakieru. Cleaner to preparat który za pomocą składników chemicznych lub cząstek mikrościernych perfekcyjnie oczyszcza lakier. Usuwa pozostałości past polerskich, tłuszcze i silikony, przygotowując powierzchnię pod nałożenie wosku lub powłoki. Niektóre, dzięki właściwościom ściernym, usuwają również niewielkie defekty z powierzchni lakieru (ryski, plamy po wodzie, małe wżery). Cleaner jest podstawą przygotowania powierzchni i uzupełnieniem mycia z dekontaminacją.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class="title">\r\n	<span style="font-size:14px;"><strong>GLAZE (Politura)</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Preparat zazwyczaj w formie mleczka, nanoszony ręcznie lub maszynowo. Jego zadaniem jest wzmocnienie blasku lakieru, maksymalne nabłyszczenie i zamaskowanie mikro defektów. Glaze nie posiada właściwości ściernych a jedynie wypełniacze a wiec efekt wizualny nie jest trwały. Świetnie nadaje się jako szybkie przygotowanie lakieru pod kątem wizualny, po cleanerze a przed woskiem.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class="title">\r\n	<span style="font-size:14px;"><strong>WOSK NATURALNY</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Podstawowa, najstarsza i najbardziej popularna (ze względu na ogólną dostępność i niski koszt) forma zabezpieczenia lakieru. Współczesne woski dobrej klasy, oparte są na carnaubie, czyli najtwardszym znanym obecnie wosku naturalnym. Najważniejszą cechą wosków jest stworzenie na lakierze bariery ochronnej, która ma zabezpieczyć lakier przed wnikaniem zabrudzeń i znacznie ułatwić bieżącą pielęgnację, dzięki tworzeniu efektu odpychania wody (kropelkowanie). Woski naturalne nadają również charakterystyczną głębie i efekt &quot;mokrego&quot; lakieru. </span></p>\r\n<p>\r\n	<span style="font-size:14px;">Woski, jak każde inne zabezpieczenie, nakłada sie na powierzchnię czystą i odtłuszczoną, a najlepiej przygotowaną za pomocą cleanera. Możemy nałożyć jedną warstwę wosku, możemy również nałożyć kilka w odpowiednich odstępach czasowych lub użyć wosku naturalnego jako warstwy ostatniej po zabezpieczeniu woskiem syntetycznym (dla bardzo mocnej ochrony).</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Cechy:</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">hydrofobowość lakieru (odpychanie wody)</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">ułatwiona pielęgnacja</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">głęboki blask i efekt mokrego lakieru</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">trwałość 4-10 tygodni</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">bardzo słaba odporność na mocną chemię</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class="title">\r\n	<span style="font-size:14px;"><strong>WOSK SYNTETYCZNY</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Drugi bardzo popularny typ wosków, oparty na syntetycznych polimerach. Woski syntetyczne posiadają mocniejsze właściwości ochronne niż woski naturalne i polecane są w szczególności w okresie jesienno-zimowym. Są bardziej odporne na czynniki zewnętrzne, w tym na mocną chemię. Sealanty równiez można warstwować, co wpływa na mocniejszą ochronę.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Cechy:</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">świetna hydrofobowość lakieru</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">mocno ułatwiona pielęgnacja</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">dobre efekty wizualne lakieru</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">trwałość 3-5 miesięcy</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">niezła odporność na chemię</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class="title">\r\n	<span style="font-size:14px;"><strong>WOSK HYBRYDOWY</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Woski łączące w sobie zalety wosków naturalnych i syntetycznych. Wzmocniona ochrona dzięki zastosowaniu składników polimerowych oraz świetny wygląd dzieki zawartości carnauby. Hybrydy nie są jednak aż tak wytrzymałe jak woski syntetyczne. </span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class="title">\r\n	<span style="font-size:14px;"><strong>AIO (All in one)</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Produkty łączący w sobie zalety cleanera, politury i wosku. Ma za zadanie w jednym etapie oczyścić powierzchnię, nadać blask i zamaskować małe defekty oraz zabezpieczyć powierzchnię woskiem. AIO pozwala skrócić znacząco czas pielęgnacji lakieru ale nigdy nie będzie tak efektywny w zabezpieczeniu i oczyszczaniu jak każdy z jego składników osobno.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class="title">\r\n	<span style="font-size:14px;"><strong>POWŁOKI</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Powłoki dzielimy ze względu na trwałość, oferowane właściwości zabezpieczające, efekty wizualne i sposób odprowadzania wody. Najważniejszą cechą powłok jest ich grubość na lakierze, co za tym idzie bardzo mocne zabezpieczenie przed wpływem czynników zewnętrznych - soli, ptasich odchodów, żywicy, owadów, promieni UV, twardej wody. Powłoka wzmacnia również odporność na powstawanie mikro zarysowań. Inną ważną cechą powłoki jest odpychanie wody, tzw. hydrofobowość, co za tym idzie łatwość w pielęgnacji dzięki nie przyleganiu brudu. Niektóre powłoki posiadają właściwości zabezpieczające nawet przed bardzo agresywną chemią lub lakierami (anty-graffiti).</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Trzeba jednak zwrócić uwagę że lakier zabezpieczony powłoką nie będzie czysty i piękny jeśli nie będzie regularnie myty. Pielęgnacja w zasadzie ogranicza się tylko do mycia, które należy wykonywać często aby nie dopuścić do sytuacji w której brud przykryje powłokę. Lakieru zabezpieczonego nie trzeba woskować lub pielęgnować w specjalny sposób. Wystarczy bezpieczne mycie przy użyciu właściwych kosmetyków, ewentualnie przetarcie lakieru dedykowanym Quick Detailerem (środek do usuwania smug, plam z wody, nadający polysk i wzmacniający ochronę).</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Lakier przed nałożeniem powłoki ochronnej powinien być perfekcyjnie przygotowany. Zalecane, w przypadku lakierów kilkuletnich, jest wykonanie najpierw korekty lakieru aby zniwelować i usunąć wszelkie defekty z lakieru. Powłoka wydobywa blask i głębie z każdego lakieru ale działa też jak soczewka - jeśli na lakierze będą defekty to po nałożeniu powłoki staną się jeszcze bardziej widoczne. </span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Cechy:</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">najmocniejsza i najgrubsza warstwa ochronna,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">wzmacnia zabezpieczenie przed wpływem czynników zewnętrznych (sól, ptasie odchody, żywice, korozja, promienie UV)</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">ogranicza powstawanie mikro zarysowań,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">świetna lub całkowita odporność na chemię,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">niesamowita głębia i blask nieosiagalna dla większości wosków,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">świetne właściwości hydrofobowe,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">trwałość 1-10 lat</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;"><strong>Podział cenowy ze względu na trwałość:</strong></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">1 rok / 900-1100 PLN</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">2-3 lata / 1600-1800 PLN</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">3-10* lat / 2500-3500 PLN</span></li>\r\n</ul>\r\n<p>\r\n	<span style="font-size:14px;">* w przypadku regularnej pielęgnacji i okresowego uzupełniania powłoki</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;"><strong>Cena usługi obejmuje:</strong></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">pełne mycie SPA z dekontaminacją,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">wygładzenie lakieru i usunięcie mikro rysek za pomocą pasty wykańczającej,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">przygotowanie lakieru przed nałożeniem powłoki za pomocą cleanera bądź dedykowanego primera,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">aplikacja powłoki,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">wygrzewanie powłoki promiennikiem IR w celu pelnego utwardzenia,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">dressing elementów zewnątrznych (plastików, opon)</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:14px;">Cena nie obejmuje korekty lakieru.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class="title">\r\n	<strong><span style="font-size:14px;">QUICK DETAILER</span></strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Uzupełnieniem regularnej pielęgnacji i mycia, szczególnie między woskowaniami, jest preparat quick detailer. Jest to preparat oparty na składnikach wosków syntetycznych lub naturalnych i ma podobne działanie jak produkty AIO, z tym że występuje w postaci płynnej. Usuwa z powierzchni lakieru drobne zabrudzenia i smugi, nadaje blask i wzmacnia ochronę nałożonego wosku lub powłoki. Stosujemy go po każdym myciu SPA jako produkt kończący, nadający finalny wygląd lakieru.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style="text-align: center;">\r\n	<span style="font-size:14px;"><span style="font-size:12px;">Samochód zabezpieczony powłoką ceramiczną:</span></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style="text-align: center;">\r\n	<span style="font-size:14px;"><iframe allowfullscreen="" frameborder="0" height="268" src="//www.youtube.com/embed/M4yIrE2tX2w?rel=0" width="477"></iframe></span></p>\r\n<p style="text-align: center;">\r\n	&nbsp;</p>\r\n<p style="text-align: center;">\r\n	<iframe frameborder="0" height="268" scrolling="no" src="//www.youtube.com/embed/ZykXCY34Dng?rel=0" width="477"></iframe></p>\r\n<p style="text-align: center;">\r\n	&nbsp;</p>', 'services-image-548099bc5152e.jpeg', 1, 'to change', 'to change', 'to change', '2014-12-04 18:28:27', NULL, '2014-12-04 00:00:00', '150-5000', '4-25h'),
(10, NULL, 6, NULL, 'Zabezpieczenie szyb nanopowłoką', 'zabezpieczenie-szyb-nanopowloka', '<p>\r\n	Nowoczesna powłoka szyby mająca na celu odpychanie wody, tym samym zapewnienie lepszej widoczności podczas jazdy. Stosowanie nanopowłoki ochronnej szyb wydłuża żywotność wycieraczek, kt&oacute;rych po prostu mniej używamy.</p>', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Aby zapewnić lepszą widoczność podczas jazdy w deszczu, producenci nanopowłok opracowali produkty do zabezpieczenia szyb. Gł&oacute;wnym ich zadaniem jest odpychanie wody z powierzchni zabezpieczonej, co już przy relatywnie niewielkiej prędkości (od 40km/h) pozwala na samo osuszanie się szyby i zdecydowanie poprawia widoczność podczas deszczu. Powierzchnia szyby staje się bardziej śliska i gładka co r&oacute;wnież wpływ na wolniejsze zużycie pi&oacute;r wycieraczek. Niekt&oacute;re powłoki zabezpieczają r&oacute;wnież przed powstawaniem mikro zarysowań na powierzchni szyby.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;"><strong>W ramach usługi wykonujemy:</strong></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:14px;">dokładnie mycie i odtłuszczenie szyby,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">przygotowanie primerem,</span></li>\r\n	<li>\r\n		<span style="font-size:14px;">nałożenie nanopowłoki</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>', 'services-image-5480c6cd6c0a3.jpeg', 1, 'to change', 'to change', 'to change', '2014-12-04 21:40:45', NULL, '2014-12-04 00:00:00', '80-350 PLN', '3-8h'),
(11, 41, 6, NULL, 'Zabezpieczenie felg', 'zabezpieczenie-felg', '<p>\r\n	Zabieg przeznaczony dla osób, które chcą aby ich felgi jak najdłużej pozostały piękne a ich mycie to była czysta przyjemność. Koniec z usmarowanymi felgami na które trzeba wylać litry chemii aby doprowadzić je do ładu.</p>', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Felgi to jeden z najbardziej narażonych na zabrudzenia element samochodu. Brud z drogi, smoła, opiłki metalu z klocków - z tym na codzień musimy sobie radzić. O ile użycie mocnej chemii zazwyczaj rozwiązuje problem, to nie każdy kilka razy w tygodniu ma czas aby jeździć na myjnię z zestawem kosmetyków i szorować felgi.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Aby ułatwić regularną pielęgnację i ograniczyć matowienie i nadmierne zniszczenie felg, proponujemy usługę zabezpieczenia kół woskiem syntetycznym lub powłoką ceramiczną. Różnica między tymi dwoma rodzajami ochrony to przede wszystkim trwałość i cena. W pierwszej, tańszej opcji ochrona ogranicza sie w zasadzie do zabezpieczenia przed nadmiernym brudzeniem się kół a także ma ułatwić ich pielęgnację. Wosk syntetyczny zazwyczaj starcza na kilka myć i nie jest to zbyt trwałe zabezpieczenie. Zupełnie inaczej ma się sytuacja przy zabezpieczeniu powłoką. Tu warstwa jest już gruba, powłoka ma odporność na wysokie temperatury i jest odporna na chemię. Powłoka na felgach utrzymuje sie zazwyczaj od kilku do kilkunastu miesięcy.</span></p>', 'services-image-5482af9538a8f.jpeg', 1, 'to change', 'to change', 'to change', '2014-12-06 08:26:12', NULL, '2014-12-06 00:00:00', '80-600 PLN', '8-15h'),
(12, NULL, 6, NULL, 'Zabezpieczenie dachu cabrio', 'zabezpieczenie-dachu-cabrio', '<p>\r\n	Aby dach cabrio był piękny przez lata należy o niego zadbać i co jakiś czas go wyczyścić i zaimpregnować. Tylko wtedy będziemy mieć pewność że materiał będzie wodoodporny a w środku nie rozwinie się grzyb.</p>', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Materiał dachu cabrio już w czasie produkcji poddawany jest impregnacji w celu nadania właściwości odpychających wodę, ograniczających powstawanie grzybów i pleśni a także odporności na zabrudzenia. Fabryczna impregnacja nie jest jednak zbyt trwała i już po kilku latach wierzchni materiał dachu traci swoja wodoodporność. Niezabezpieczony materiał jest bardzo podatny na wszelkiego rodzaju przetarcia, nie tyle mechaniczne co spowodowane brudem, który pod wpływem wody działa jak materiał ścierny, niszcząc strukturę, włókna i szwy.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:14px;">Aby zapobiec nadmiernemu i przedwczesnemu zużyciu poszycia dachu, niezbędna jest regularna pielęgnacja. Podstawą jest dokładne odkurzenie dachu a następnie wyczyszczenie odpowiednimi kosmetykami. Dopiero tak przygotowany dach można poddać impregnacji. Tutaj również mamy 2 sposoby zabezpieczenia - preparaty syntetyczne oraz nano powłoki. Te pierwsze są tańsze ale też mnie trwałe i w zasadzie po każdym gruntownym czyszczeniu dachu trzeba je nakładać od nowa. Nano powłoki są o wiele bardziej trwale i mocniejsze w zabezpieczeniu. Są również bardziej odporne na chemię dzięki czemu nawet po uzyciu mocniejszej chemii w trakcie mycia, nie usuniemy powłoki.</span></p>', 'services-image-5483090d13e25.jpeg', 1, 'to change', 'to change', 'to change', '2014-12-06 14:47:56', NULL, '2014-12-06 00:00:00', '300-500 PLN', '6-12h'),
(13, NULL, 10, NULL, 'Sprzedaj bez wkładu', 'sprzedaj-bez-wkladu', '<p>\r\n	Unikalna propozycja dla osób chcących sprzedać swój pojazd. Wykonamy wszystkie potrzebne usługi: kosmetykę, lakierowanie, mechanikę, sesję zdjęciową, ogłoszenie, sprzedaż - BEZ POBRANIA OPŁAT! Nasze wynagrodzenie rozliczymy dopiero po sprzedaży.</p>', '<p class="wyroznienie">\r\n	<span style="font-size:22px;">Szczegóły wkrótce!</span></p>', 'services-image-54841fd62b6e1.jpeg', 1, 'to change', 'to change', 'to change', '2014-12-07 10:37:25', NULL, '2014-12-07 00:00:00', 'od 250 PLN', 'od 2h'),
(14, 43, 10, NULL, 'Sesje zdjęciowe', 'sesje-zdjeciowe', '<p>\r\n	Nasz fotograf wykona dla Państwa sesję przedsprzedażową pojazdu. Przygotujemy wcześniej samochód przed zdjęciami, zadbamy o odpowiednie &quot;tło&quot; i końcową obróbkę zdjęć.</p>', NULL, 'services-image-548429ed28dac.jpeg', 1, 'to change', 'to change', 'to change', '2014-12-07 11:20:28', NULL, '2014-12-07 00:00:00', 'od 150 PLN', 'od 1h');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `services_category`
--

CREATE TABLE `services_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `arrangement` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Zrzut danych tabeli `services_category`
--

INSERT INTO `services_category` (`id`, `title`, `photo`, `slug`, `arrangement`) VALUES
(6, 'Auto SPA & Smart Repair', 'category-image-54396e49698b1.jpeg', 'auto-spa-smart-repair', NULL),
(7, 'Blacharstwo / Lakiernictwo', 'category-image-54396e8340933.jpeg', 'blacharstwo-lakiernictwo', 1),
(10, 'Obsługa przedsprzedażowa', 'category-image-54396f9bdc27a.jpeg', 'obsluga-przedsprzedazowa', 5),
(11, 'Renowacja i stylizacja', 'category-image-5484255839f13.jpeg', 'renowacja-i-stylizacja', 3),
(12, 'Obsługa powypadkowa', 'category-image-545bd03456894.jpeg', 'obsluga-powypadkowa', 2),
(13, 'Aktualne oferty', 'category-image-548421142def0.jpeg', 'aktualne-oferty', 6);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `services_subcategory`
--

CREATE TABLE `services_subcategory` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `category_id` int(11) NOT NULL,
  `arrangement` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Zrzut danych tabeli `services_subcategory`
--

INSERT INTO `services_subcategory` (`id`, `title`, `category_id`, `arrangement`) VALUES
(1, 'Testowa podkategoria', 6, 0),
(2, 'Testowa podkategoria 2', 6, 1),
(4, 'Testowa Podkategoria 3', 6, 2),
(5, 'test 5', 6, NULL),
(6, 'Test obsługa przedsprzedaż sub cat', 10, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `setting`
--

INSERT INTO `setting` (`id`, `label`, `description`, `value`) VALUES
(1, 'seo_title', 'Treść domyślnego tagu Title', 'ONYXX Automobile Center / auto detailing, polerowanie lakieru, lakiernictwo Rzeszów'),
(2, 'seo_description', 'Treść domyślnego metatagu Description', 'Onyxx'),
(3, 'seo_keywords', 'Treść domyślnego metatagu Keywords', ''),
(4, 'limit_news', 'Ilość aktualności wyświetlanych na jednej stronie listowania', '4'),
(19, 'email_to_kontakt', 'Adres e-mail na który mają być wysyłane wiadomości z formularza kontaktowego', 'biuro@onyxx.pl'),
(20, 'added_texts_regulamin', 'Polityka cookies', '<p>\r\n	<b>Regulamin dostępny już wkr&oacute;tce.</b></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<b>Przepraszamy za niedogodności.</b></p>'),
(24, 'facebookAppId', 'ID Aplikacji facebook', '1468529833382947'),
(25, 'gps_latitude', 'Szerokość Georaficzna GPS', '50.038744'),
(26, 'gps_longitude', 'Długość Geograficzna GPS', '22.055856'),
(27, 'facebookFanpage', 'adres fanpage na facebooku', 'http://www.facebook.com/OnyxxDetailing'),
(28, 'shopSite', 'Adres sklepu link SG', 'https://automobilecenter.shoparena.pl'),
(29, 'phone_number', 'Telefon kontaktowy', '+48 733 666 667'),
(30, 'company_address', 'Adres firmy', '35-301 Rzeszów, ul. Lwowska 112'),
(31, 'wirtualny_spacer_url', 'Adres odnośnika do wirtualnego spaceru', 'http://lemonadestudio.pl'),
(32, 'file_sklep', 'grafika w polu sklep ns SG [276x288]', 'settings-image-546cc742ee01a.jpeg'),
(34, 'file_fbImage', 'Zdjęcie wyświetlane w czasie share na FB[300x300]', 'settings-image-546cfb29e0223.jpeg'),
(35, 'googlePage', 'adres strony google plus', 'https://plus.google.com/107287725220699763510/about'),
(36, 'file_kontakt1', 'grafika 1 w kontakcie [271x166]', 'settings-image-549ebc06169b1.jpeg'),
(37, 'file_kontakt2', 'grafika 2 w kontakcie [271x166]', 'settings-image-549ebc234bfe0.jpeg'),
(38, 'file_kontakt3', 'grafika 3 w kontakcie [271x166]', 'settings-image-549ebd55ce7e5.jpeg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo_width` int(11) NOT NULL,
  `photo_height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `slider`
--

INSERT INTO `slider` (`id`, `label`, `photo_width`, `photo_height`) VALUES
(1, 'Slider-SG', 980, 432);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider_photo`
--

CREATE TABLE `slider_photo` (
  `id` int(11) NOT NULL,
  `slider_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `link_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `routeParameters` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `onclick` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `slider_photo`
--

INSERT INTO `slider_photo` (`id`, `slider_id`, `photo`, `title`, `content`, `link_name`, `type`, `route`, `routeParameters`, `url`, `onclick`, `arrangement`) VALUES
(5, 1, 'slider-image-54088fce3c3f0.jpeg', 'Przedsiębiorczość / 2009', '<p>\r\n	Podnoszenie konkurencyjności i innowacyjności firm, pozyskiwanie inwestycji zagranicznych, tworzenie nowych miejsc pracy.</p>', 'Przedsiębiorczość', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/przedsiebiorczosc', NULL, 2),
(6, 1, 'slider-image-54088fdd9d9d3.jpeg', 'Nauka / 2010', '<p>\r\n	Inwestowanie w edukację, naukę, kapitał ludzki oraz ustawiczne kształcenie.</p>', 'Nauka', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/nauka', NULL, 3),
(7, 1, 'slider-image-54088ffe9ad88.jpeg', 'Inwestycje / 2011', '<p>\r\n	Realizacja inwestycji i projekt&oacute;w prorozwojowych oraz zwiększanie atrakcyjności inwestycyjnej.</p>', 'Inwestycje', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/inwestycje', NULL, 4),
(8, 1, 'slider-image-54089009a0e30.jpeg', 'Energetyka / 2012', '<p>\r\n	Wykorzystanie potencjału regionalnych zasob&oacute;w energetycznych i rozw&oacute;j alternatywnych źr&oacute;deł energii.</p>', 'Energetyka', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/energetyka', NULL, 5),
(9, 1, 'slider-image-54089014bf9bc.jpeg', 'Planowanie strategiczne / 2013', '<p>\r\n	Trafne określanie cel&oacute;w rozwojowych i skuteczne zarządzanie procesami rozwojowymi w gospodarce.</p>', 'Planowanie strategiczne', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/planowanie-strategiczne', NULL, 6),
(10, 1, 'slider-image-5408902431bcd.jpeg', 'Cyfryzacja / 2014', '<p>\r\n	Zwiększanie dostępności technologii informacyjno-komunikacyjnych i rozw&oacute;j społeczeństwa cyfrowego.</p>', 'Cyfryzacja', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/cyfryzacja', NULL, 7),
(11, 1, 'slider-image-54088f961a409.jpeg', 'Innowacje / 2008', '<p>\r\n	Wzmacnianie sektora badawczo-rozwojowego oraz wykorzystywanie wynik&oacute;w prac badawczych w gospodarce.&nbsp;</p>', 'Innowacje', 'url', 'lscms_dietetyk', NULL, '/moja-dzialalnosc/kategoria/innowacje', NULL, 1),
(14, 1, 'slider-image-540890a90bb43.jpeg', 'Sieci współpracy i partnerstwa gospodarcze', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu budowy sieci wsp&oacute;łpracy branżowej i klastrowej oraz partnerstw gospodarczych</p>', 'Sieci współpracy i partnerstwa gospodarcze', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/wspolpraca-i-partnerstwa-gospodarcze', NULL, 8),
(15, 1, 'slider-image-540890b6f2108.jpeg', 'Strategia spójności', '<p>\r\n	Opis testowy</p>', 'Strategia spójności', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/strategia-spojnosci', NULL, 9);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `trainings`
--

CREATE TABLE `trainings` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_short` longtext COLLATE utf8_unicode_ci,
  `content` longtext COLLATE utf8_unicode_ci,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `trainings`
--

INSERT INTO `trainings` (`id`, `title`, `slug`, `content_short`, `content`, `photo`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`) VALUES
(1, 'Auto Detailing - pielęgnacja', 'auto-detailing-pielegnacja', '<p>\r\n	<span style="font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Podstawowe szkolenie dla pasjonat&oacute;w lub przyszłych detailer&oacute;w</span>, pozwalające posiąść podstawowe umiejętności, niezbędne do właściwego przeprowadzenia proces&oacute;w pielegnacji pojazd&oacute;w.</p>', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:12px;"><span style="font-family: Arial,Helvetica,sans; line-height: 14px; text-align: justify;">Szkolenie z Auto Detailingu pozwoli nabyć umiejętności niezbędne do prawidłowego wykonywania czynności związanych z pielegnacją pojazd&oacute;w. Szkolenie &quot;Auto Detailing - pielęgnacja&quot; to przede wszystkim skupienie na procesie mycia, dekontaminacji i zabezpieczenia pojazdu zar&oacute;wno wewnątrz jak i na zewnątrz. </span></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:12px;"><span style="font-family: Arial,Helvetica,sans; line-height: 14px; text-align: justify;">Szkolenie trwa 2 dni po 8h</span> i obejmuje następujące zagadnienia:</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:12px;"><strong>DZIEŃ 1</strong></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:12px;">Zasady bezpiecznego mycia samochod&oacute;w. Przygotowanie samochodu do pełnego auto detailingu z naciskiem na minimalizację ryzyka uszkodzenia powierzchni lakieru. Rozpoznanie błęd&oacute;w powstających w trakcie niewłaściwej pielęgnacji samochodu</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Pielęgnacja k&oacute;ł i nadkoli (mycie i zabezpieczenie)</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Czyszczenie komory silnika</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Dekontaminacja lakieru. Właściwe zastosowanie glinki do oczyszczania lakieru oraz produkt&oacute;w do usuwania smoły, owad&oacute;w i opiłk&oacute;w metalicznych</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Zabezpieczenie lakieru &ndash; woski syntetyczne i naturalne, powłoki kwarcowe/ceramiczne</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Zajecia praktyczne</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:12px;"><strong>DZIEŃ 2</strong></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:12px;">Detailing wnętrza wraz z praniem tapicerki materiałowej oraz czyszczeniem i zabezpieczeniem tapicerki sk&oacute;rzanej</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Dressing i zabezpieczenie zewnętrznych element&oacute;w pojazdu oraz komory silnika</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Impregnacja i zabezpieczenie element&oacute;w wnętrza i tapicerki</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Właściwa regularna pielęgnacja pojazdu zabezpieczonego</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Ocena niedoskonałości i wad powłoki lakierniczej w celu dalszego określenia metod stosowanych w celu ich zniwelowania</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Zajęcia praktyczne z w/w zakresu</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Prezentacja produkt&oacute;w stosowanych w studiach autodetailingu</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Egzamin teoretyczny (10 pytań) i praktyczny (3 losowo wybrane czynności z zakresu szkolenia)</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:12px;">Cena szkolenia: 1 200,00 zł netto</span></p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:12px;">Po ukończeniu szkolenia i zaliczeniu egzaminu osoba otrzymuje certyfikat, tytuł &quot;Car Valeter&quot; oraz prawo do rabat&oacute;w hurtowych na cały asortyment (chemia, kosmetyki, akcesoria) dostępny w ofercie naszej firmy.</span></p>', 'training-image-546a4ceaed637.jpeg', 1, 'to change', 'to change', 'to change', '2014-09-20 00:28:41', NULL, '2014-09-20 00:00:00'),
(2, 'Auto Detailing - renowacja', 'auto-detailing-renowacja', '<p>\r\n	Zaawansowany pakiet szkoleń, kładący nacisk na renowację element&oacute;w pojazdu. Szkolenie przeznaczone jest przede wszystkim dla os&oacute;b pragnących związać swą karierę z branżą Auto Detailingu.</p>', '<p>\r\n	<span style="font-size:12px;"><span style="font-family: Arial,Helvetica,sans; line-height: 14px; text-align: justify;">Szkolenie rozszerzone z Auto Detailingu pozwoli nabyć umiejętności niezbędne do prawidłowego wykonywania czynności związanych z renowacją pojazd&oacute;w. Szkolenie &quot;Auto Detailing - renowacja&quot; to wiedza teoretyczna i umiejętności praktyczne dotyczące renowacji lakieru, reflektor&oacute;w, metalu</span>, itp.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:12px;"><span style="font-family: Arial,Helvetica,sans; line-height: 14px; text-align: justify;">Szkolenie trwa 3 dni po 8h</span> i obejmuje następujące zagadnienia:</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:12px;"><strong>DZIEŃ 1</strong></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:12px;">Inspekcja powłoki lakierniczej</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Pomiar grubości lakieru. Sposoby szacowania grubości poszczeg&oacute;lnych warstw.</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Podstawy teoretyczne polerowania maszynowego.</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Wet-sanding - bezpieczne użycie papieru ściernego w usuwaniu defekt&oacute;w lakieru.</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:12px;"><strong>DZIEŃ 2</strong></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:12px;">Korekta lakieru - zajęcia praktyczne</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-size:12px;"><strong>DZIEŃ 3</strong></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:12px;">Polerowanie kloszy reflektor&oacute;w</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Techniki polerowania metalu</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Zajęcia praktyczne</span></li>\r\n	<li>\r\n		<span style="font-size:12px;">Egzamin teoretyczny (20 pytań w tym 10 ze szkolenia &quot;Auto Detailing - pielęgnacja&quot; jeśli osoba go nie odbyła wcześniej) oraz praktyczny (3 losowo wybrane czynności + 3 ze szkolenia podstawowego)</span></li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:12px;">Cena szkolenia: 2 500,00 zł netto</span></p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:12px;">Po ukończeniu szkolenia i zaliczeniu egzaminu osoba otrzymuje certyfikat, tytuł &quot;Auto Detailer&quot; oraz prawo do rabat&oacute;w hurtowych na cały asortyment (chemia, kosmetyki, akcesoria) dostępny w ofercie naszej firmy. Dodatkowo proponujemy zniżkę na szkolenia Smart Repair w wysokości 15%.</span></p>\r\n<p class="wyroznienie">\r\n	<span style="font-size:12px;">Skorzystanie z dw&oacute;ch lub więcej szkoleń jednorazowo upoważnia do 20% zniżki.</span></p>', 'training-image-546a573eeaa91.jpeg', 1, 'to change', 'to change', 'to change', '2014-11-17 21:14:54', NULL, '2014-11-17 00:00:00');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `acl_classes`
--
ALTER TABLE `acl_classes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`);

--
-- Indexes for table `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  ADD KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  ADD KEY `IDX_46C8B806EA000B10` (`class_id`),
  ADD KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_46C8B806DF9183C9` (`security_identity_id`);

--
-- Indexes for table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  ADD KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`);

--
-- Indexes for table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  ADD KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_825DE299C671CEA1` (`ancestor_id`);

--
-- Indexes for table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`);

--
-- Indexes for table `fos_group`
--
ALTER TABLE `fos_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_4B019DDB5E237E06` (`name`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`);

--
-- Indexes for table `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `IDX_B3C77447A76ED395` (`user_id`),
  ADD KEY `IDX_B3C77447FE54D947` (`group_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F02A543B4E7AF8F` (`gallery_id`);

--
-- Indexes for table `karnet_zamowienie`
--
ALTER TABLE `karnet_zamowienie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_77D360B841F8D70E` (`karnet_id`);

--
-- Indexes for table `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D754D550727ACA70` (`parent_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1DD399504E7AF8F` (`gallery_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_140AB6204E7AF8F` (`gallery_id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person_wizyta`
--
ALTER TABLE `person_wizyta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8126474B217BBB47` (`person_id`);

--
-- Indexes for table `porada`
--
ALTER TABLE `porada`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_12857A934E7AF8F` (`gallery_id`),
  ADD KEY `IDX_12857A9312469DE2` (`category_id`);

--
-- Indexes for table `porada_category`
--
ALTER TABLE `porada_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_7234C9C75E237E06` (`name`);

--
-- Indexes for table `pricing_item`
--
ALTER TABLE `pricing_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promo`
--
ALTER TABLE `promo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refs`
--
ALTER TABLE `refs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_12857A934E7AF8F` (`gallery_id`),
  ADD KEY `IDX_12857A9312469DE2` (`category_id`);

--
-- Indexes for table `services_category`
--
ALTER TABLE `services_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services_subcategory`
--
ALTER TABLE `services_subcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9F74B898EA750E8` (`label`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_CFC71007EA750E8` (`label`);

--
-- Indexes for table `slider_photo`
--
ALTER TABLE `slider_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9203C87C2CCC9638` (`slider_id`);

--
-- Indexes for table `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `acl_classes`
--
ALTER TABLE `acl_classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `acl_entries`
--
ALTER TABLE `acl_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `fos_group`
--
ALTER TABLE `fos_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=940;
--
-- AUTO_INCREMENT dla tabeli `karnet_zamowienie`
--
ALTER TABLE `karnet_zamowienie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT dla tabeli `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT dla tabeli `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT dla tabeli `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `person_wizyta`
--
ALTER TABLE `person_wizyta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `porada`
--
ALTER TABLE `porada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT dla tabeli `porada_category`
--
ALTER TABLE `porada_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT dla tabeli `pricing_item`
--
ALTER TABLE `pricing_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT dla tabeli `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT dla tabeli `promo`
--
ALTER TABLE `promo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `refs`
--
ALTER TABLE `refs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT dla tabeli `services_category`
--
ALTER TABLE `services_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT dla tabeli `services_subcategory`
--
ALTER TABLE `services_subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT dla tabeli `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `slider_photo`
--
ALTER TABLE `slider_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT dla tabeli `trainings`
--
ALTER TABLE `trainings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Ograniczenia dla tabeli `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_group` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD CONSTRAINT `FK_F02A543B4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `karnet_zamowienie`
--
ALTER TABLE `karnet_zamowienie`
  ADD CONSTRAINT `FK_77D360B841F8D70E` FOREIGN KEY (`karnet_id`) REFERENCES `pricing_item` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_D754D550727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `menu_item` (`id`);

--
-- Ograniczenia dla tabeli `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `FK_1DD399504E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `FK_140AB6204E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `person_wizyta`
--
ALTER TABLE `person_wizyta`
  ADD CONSTRAINT `FK_8126474B217BBB47` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `porada`
--
ALTER TABLE `porada`
  ADD CONSTRAINT `FK_12857A9312469DE2` FOREIGN KEY (`category_id`) REFERENCES `porada_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_12857A934E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `slider_photo`
--
ALTER TABLE `slider_photo`
  ADD CONSTRAINT `FK_9203C87C2CCC9638` FOREIGN KEY (`slider_id`) REFERENCES `slider` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
