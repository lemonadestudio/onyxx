
$(function() {

    fckconfig_common_config = {
        skin: 'BootstrapCK-Skin',
        contentsCss: ['/bundles/frontend/css/fonts/fonts.css', '/bundles/lscms/css/editor.css?ref2='+Math.random()],
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}},
            {name: 'Nagłówek', element: 'h2', attributes: {'class': 'title'}},
            {name: 'Cytat', element: 'p', attributes: {'class': 'wyroznienie'}}


        ],
        filebrowserBrowseUrl: kcfinderBrowseUrl + '?type=files',
        filebrowserImageBrowseUrl: kcfinderBrowseUrl + '?type=images',
        filebrowserFlashBrowseUrl: kcfinderBrowseUrl + '?type=flash',
        filebrowserUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=flash'
    };

    fckconfig_slider_config = {
        skin: 'BootstrapCK-SkinDark',
        contentsCss: ['/bundles/lscms/css/editor-slider.css'],
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}}
        ],
        filebrowserBrowseUrl: kcfinderBrowseUrl + '?type=files',
        filebrowserImageBrowseUrl: kcfinderBrowseUrl + '?type=images',
        filebrowserFlashBrowseUrl: kcfinderBrowseUrl + '?type=flash',
        filebrowserUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=flash'
    };

    fckconfig_basic_config = {
        skin: 'BootstrapCK-Skin',
        contentsCss: ['/bundles/lscms/css/editor.css'],
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}},
            {name: 'Czerwony', element: 'span', attributes: {'class': 'red'}},
            {name: 'Zielony', element: 'span', attributes: {'class': 'green'}}
        ],
        toolbar:
            [
                { name: 'styles', items : [ 'Styles' ] },
                {name: 'basicstyles', items: ['Bold', 'Italic']},
                {name: 'tools', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                {name: 'paragraph', items: ['NumberedList', 'BulletedList', 'Outdent', 'Indent']}
            ],
        removePlugins: 'elementspath',
        resize_enabled: false,
        toolbarCanCollapse: false,
        bodyClass: 'editor'
    };

    fckconfig = jQuery.extend(true, {
        width: '740px',
        height: '400px'
    }, fckconfig_common_config);

    fckconfig_slider = jQuery.extend(true, {
        width: '740px',
        height: '400px'
    }, fckconfig_slider_config);

    fckconfig_basic = jQuery.extend(true, {
        width: '740px',
        height: '400px'
    }, fckconfig_basic_config);

    $('.wysiwyg').ckeditor(fckconfig);
    $('.wysiwyg-basic').ckeditor(fckconfig_basic);
    $('.wysiwyg-slider').ckeditor(fckconfig_slider);
});